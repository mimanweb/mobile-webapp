import http from "../../utils/http";
import {API} from "../../utils/api";
import {COMICS_STATUS} from "../../utils/index/comics/comics";
import _ from "underscore";
import {COMIC_BOOK_IMAGE_STATUS, COMIC_BOOK_STATUS} from "../../utils/index/comics/book";


export const actionType = {
    SET_COMICS_LIST: "comics/SET_COMICS_LIST",
    SET_COMICS_STATE: "comics/SET_COMICS_STATE",
    SET_COMICS_PAGE_STATE: "comics/SET_COMICS_PAGE_STATE",
    SET_COMICS_SEARCH_TEXT_STATE: "comics/SET_COMICS_SEARCH_TEXT_STATE",
    SET_RECENT_READ_COMICS_LIST_STATE: "comics/SET_RECENT_READ_COMICS_LIST_STATE",
}


const initialState = {
    isApproved: false,
    comicsList: [],
    comicsDataId: null,
    comicsId: null,
    comicsNumber: null,
    title: {},
    writers: [],
    artists: [],
    publisher: {},
    genres: [],
    contentDescriptors: [],
    summary: "",
    coverImage: null,
    editions: [],
    publishedEditionsGroupByTranslator: [],
    itemsPerPage: null,
    totalPageCount: null,
    page: null,
    searchText: "",
    recentReadComics: []
}


export default function reducer(state=initialState, action) {
    switch (action.type) {
        case actionType.SET_COMICS_LIST:
        case actionType.SET_COMICS_STATE:
        case actionType.SET_COMICS_PAGE_STATE:
        case actionType.SET_COMICS_SEARCH_TEXT_STATE:
        case actionType.SET_RECENT_READ_COMICS_LIST_STATE:
            return {
                ...state,
                ...action.payload,
            }
        default:
            return state;
    }
};





/**
 * Set comics list state
 * */
export function setComicsListState(comicsList) {
    return {
        type: actionType.SET_COMICS_LIST,
        payload: {
            comicsList: comicsList,
        }
    }
}


/**
 * Set recently read comics list state
 * */
export function setRecentReadComicsListState(comicsList) {
    return {
        type: actionType.SET_RECENT_READ_COMICS_LIST_STATE,
        payload: {
            recentReadComics: comicsList,
        }
    }
}



/**
 * Set comics list and page state
 * */
export function setComicsListPageState(comicsList, itemsPerPage, totalCount, page) {
    const totalPageCount = Math.ceil(totalCount/itemsPerPage);

    return {
        type: actionType.SET_COMICS_PAGE_STATE,
        payload: {
            comicsList: comicsList,
            itemsPerPage: itemsPerPage,
            totalPageCount: totalPageCount,
            page: page
        }
    }
}


/**
 * Set comics search text state
 * */
export function setComicsSearchTextState(text) {
    return {
        type: actionType.SET_COMICS_SEARCH_TEXT_STATE,
        payload: {
            searchText: text
        }
    }
}


/**
 * Set comics state
 * */
export function setComicsState(comicsData) {
    const {_id: comicsDataId, id: comicsId, status, number: comicsNumber, title, writers, artists, publisher, genres, contentDescriptors, summary, coverImage, editions} = comicsData;

    const isApproved = status===COMICS_STATUS.APPROVED;
    let publishedEditionsGroupByTranslator = [];
    let publishedEditions = [];

    if (!_.isEmpty(editions)) {
        publishedEditions = editions.filter(book => book.status===COMIC_BOOK_STATUS.PUBLISH);
        publishedEditions.forEach(book => book.ownerUserId = book.owner.userId);

        publishedEditionsGroupByTranslator = _.groupBy(publishedEditions, "ownerUserId");
    }

    return {
        type: actionType.SET_COMICS_STATE,
        payload: {
            isApproved: isApproved,
            comicsDataId: comicsDataId,
            comicsId: comicsId || null,
            comicsNumber: comicsNumber || null,
            title: title || "",
            writers: writers || [],
            artists: artists || [],
            publisher: publisher || {},
            genres: genres || [],
            contentDescriptors: contentDescriptors || [],
            summary: summary || "",
            coverImage: coverImage || null,
            editions: editions || [],
            publishedEditions: publishedEditions,
            publishedEditionsGroupByTranslator: publishedEditionsGroupByTranslator
        }
    }
}



/**
 * Create comics
 * */
export async function requestCreateComics(title) {
    return await http.post(API.Comics.createComics.url, {title});
}


/**
 * Find comics
 * With comics title
 * */
export async function requestAllComicsMatchTitle(title, pageToSearch) {
    return await http.get(API.Comics.findComicsMatchTitle.url, {title, pageToSearch});
}


/**
 * Find comics
 * Match title index character
 * */
export async function requestAllComicsMatchTitleIndexChar(text, pageToSearch) {
    return await http.get(API.Comics.findComicsMatchTitleIndexChar.url, {text, pageToSearch});
}


/**
 * Find comics writers regex match text
 * */
export async function requestComicsWritersMatchText(textToSearch) {
    return await http.get(API.Comics.findWritersMatchText.url, {textToSearch});
}


/**
 * Add comics writer
 * */
export async function requestAddComicsWriter(comicsDataId, comicsWriterDataId) {
    return await http.post(API.Comics.addComicsWriter.url, {comicsDataId, comicsWriterDataId});
}


/**
 * Add new comics writer
 * */
export async function requestAddNewComicsWriter(comicsDataId, writerName) {
    return await http.post(API.Comics.addNewComicsWriter.url, {comicsDataId, writerName});
}


/**
 * Find comics artists regex match text
 * */
export async function requestComicsArtistsMatchText(textToSearch) {
    return await http.get(API.Comics.findArtistsMatchText.url, {textToSearch});
}


/**
 * Add comics artist
 * */
export async function requestAddComicsArtist(comicsDataId, comicsArtistDataId) {
    return await http.post(API.Comics.addComicsArtist.url, {comicsDataId, comicsArtistDataId});
}


/**
 * Add new comics artist
 * */
export async function requestAddNewComicsArtist(comicsDataId, artistName) {
    return await http.post(API.Comics.addNewComicsArtist.url, {comicsDataId, artistName});
}


/**
 * Find Comics Publishers regex match text
 * */
export async function requestComicsPublishersMatchText(textToSearch) {
    return await http.get(API.Comics.findPublishersMatchText.url, {textToSearch});
}


/**
 * Update comics publisher
 * */
export async function requestUpdateComicsPublisher(comicsDataId, comicsPublisherDataId) {
    return await http.put(API.Comics.updateComicsPublisher.url, {comicsDataId, comicsPublisherDataId});
}


/**
 * Add new comics publisher
 * */
export async function requestAddNewComicsPublisher(comicsDataId, publisherName) {
    return await http.put(API.Comics.updateWithNewComicsPublisher.url, {comicsDataId, publisherName});
}


/**
 * Find all Comics Genres
 * */
export async function requestAllComicsGenres() {
    return await http.get(API.Comics.findAllComicsGenres.url);
}


/**
 * Add Comics Genre
 * */
export async function requestAddComicsGenre(comicsDataId, genreDataId) {
    return await http.post(API.Comics.addComicsGenre.url, {comicsDataId, genreDataId});
}


/**
 * Find all Comics Content Descriptors
 * */
export async function requestAllComicsContentDescriptors() {
    return await http.get(API.Comics.findAllComicsContentDescriptors.url);
}


/**
 * Add Comics Content Descriptor
 * */
export async function requestAddComicsContentDescriptor(comicsDataId, contentDescriptorDataId) {
    return await http.post(API.Comics.addComicsContentDescriptor.url, {comicsDataId, contentDescriptorDataId});
}


/**
 * Update Comics Cover Image
 * */
export async function requestUpdateComicsCoverImage(comicsDataId, imgFile) {
    const formData = new FormData();

    formData.append("comicsDataId", comicsDataId);
    formData.append('image', imgFile);

    return await http.post(API.Comics.updateComicsCoverImage.url, formData);
}


/**
 * Update comics summary
 * */
export async function requestUpdateComicsSummary(comicsDataId, comicsSummary) {
    return await http.put(API.Comics.updateComicsSummary.url, {comicsDataId, comicsSummary});
}


/**
 * Recent read comics latest update info
 * */
export async function requestRecentReadComicsLatestUpdateInfo(recentReadList) {
    return await http.get(API.Comics.recentReadComicsLatestUpdateInfo.url, {recentReadList});
}

