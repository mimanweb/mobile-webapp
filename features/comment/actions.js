import http from "../../utils/http";
import {API} from "../../utils/api";



export const actionType = {
    SET_COMMENT_NUMBER_TO_WRITE: "comment/SET_COMMENT_NUMBER_TO_WRITE",
    SET_COMMENT_NUMBER_TO_DELETE: "comment/SET_COMMENT_NUMBER_TO_DELETE",
    SET_COMMENT_AND_PAGE_STATE: "comment/SET_COMMENT_AND_PAGE_STATE",
    SET_PAGE_COMMENTS_STATE: "comment/SET_PAGE_COMMENTS_STATE",
    SET_BEST_COMMENTS_STATE: "comment/SET_BEST_COMMENTS_STATE",
}


const initialState = {
    commentNumberToWrite: null,
    commentNumberToDelete: null,
    pageComments: [],
    bestComments: [],
    itemsPerPageCount: 0,
    totalPageCount: 0,
    searchedPage: 0,
    totalCommentCount: 0,
    commentToScroll: null,
}


export default function reducer(state=initialState, action) {
    switch (action.type) {
        case actionType.SET_COMMENT_NUMBER_TO_WRITE:
        case actionType.SET_COMMENT_NUMBER_TO_DELETE:
        case actionType.SET_COMMENT_AND_PAGE_STATE:
        case actionType.SET_PAGE_COMMENTS_STATE:
        case actionType.SET_BEST_COMMENTS_STATE:
            return {
                ...state,
                ...action.payload,
            }
        default:
            return state;
    }
};




/**
 * Set post page comment and comment page state
 * */
export function setCommentState(commentData, commentToScroll) {
    const {pageComments, bestComments, itemsPerPageCount, totalPageCount, totalCommentCount, searchedPage} = commentData;
    return {
        type: actionType.SET_COMMENT_AND_PAGE_STATE,
        payload: {
            pageComments: pageComments,
            bestComments: bestComments,
            itemsPerPageCount: itemsPerPageCount,
            totalPageCount: totalPageCount,
            totalCommentCount: totalCommentCount,
            searchedPage: searchedPage,
            commentToScroll: commentToScroll? Number(commentToScroll): null
        }
    }
}



/**
 * Set comment number in writing
 * */
export function setCommentNumberToWrite(commentNumber) {
    return {
        type: actionType.SET_COMMENT_NUMBER_TO_WRITE,
        payload: {
            commentNumberToWrite: commentNumber
        }
    }
}


/**
 * Set comment number to delete
 * */
export function setCommentNumberToDelete(commentNumber) {
    return {
        type: actionType.SET_COMMENT_NUMBER_TO_WRITE,
        payload: {
            commentNumberToDelete: commentNumber
        }
    }
}


/**
 * Set page-comments state
 * */
export function setPageCommentsState(list) {
    return {
        type: actionType.SET_PAGE_COMMENTS_STATE,
        payload: {
            pageComments: list
        }
    }
}


/**
 * Set best-comments state
 * */
export function setBestCommentsState(list) {
    return {
        type: actionType.SET_BEST_COMMENTS_STATE,
        payload: {
            bestComments: list
        }
    }
}



/**
 * Request comment data and specified comment-page
 * */
export async function requestCommentsInSpecificPage(postNumber, pageToSearch) {
    return await http.get(API.Comment.findCommentsInSpecificPage.url, {postNumber, pageToSearch});
}


/**
 * Upload comment
 * */
export async function requestUploadComment(postNumber, category, board, imgFile, youtubeHTML, text, nickname, password) {
    const formData = new FormData();

    formData.append('image', imgFile);
    formData.append('postNumber', postNumber);
    formData.append('category', category);
    formData.append("board", board);
    if (youtubeHTML) formData.append("youtubeHTML", youtubeHTML);
    formData.append("text", text);
    formData.append("nickname", nickname);
    formData.append("password", password);

    return await http.post(API.Comment.uploadComment.url, formData);
}


/**
 * Upload reply-comment
 * */
export async function requestUploadReplyComment(postNumber, commentNumber, category, board, imgFile, youtubeHTML, text, nickname, password) {
    const formData = new FormData();

    formData.append('image', imgFile);
    formData.append('postNumber', postNumber);
    formData.append('commentNumber', commentNumber);
    formData.append('category', category);
    formData.append("board", board);
    if (youtubeHTML) formData.append("youtubeHTML", youtubeHTML);
    formData.append("text", text);
    formData.append("nickname", nickname);
    formData.append("password", password);

    return await http.post(API.Comment.uploadReplyComment.url, formData);
}


/**
 * Update comment upvote record
 * */
export async function requestUpdateCommentUpvote(postNumber, commentNumber) {
    return await http.post(API.Comment.updateCommentUpvote.url, {postNumber, commentNumber});
}


/**
 * User delete comment
 * */
export async function requestDeleteComment(postNumber, commentNumber, password) {
    return await http.delete(API.Comment.userDeleteComment.url, {postNumber, commentNumber, password});
}