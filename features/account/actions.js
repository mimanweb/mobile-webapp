import {API} from "../../utils/api";
import http from "../../utils/http";


export async function requestChangePassword(userId, passwordResetToken, newPassword) {
    const params = {
        id: userId,
        token: passwordResetToken,
        password: newPassword
    }

    const res = await http.put(API.User.changePassword.url, params);

    if (res.error) {
        let msg = "";
        switch (res.status) {
            case 404:
                msg = "해당 아이디로 가입된 계정이 없습니다.";
                break;
            case 403:
                msg = "현재 비밀번호 재설정 링크는 만료되었습니다.\n이메일(email)로 다시 발급받아 주세요."
                break;
            case 500:
                msg = "서버 오류가 발생하였습니다.";
                break;
        }

        return {success: false, msg: msg};
    } else {
        return {success: true};
    }
}


export async function requestUpdateUserInfo(password, newPassword, nickname) {
    const res = await http.put(API.User.changeInfo.url, {password: password, newPassword: newPassword, nickname: nickname});

    let msg = "";
    if (res.error) {
        switch (res.status) {
            case 400:
                if (res.code === "000") msg = "닉네임 형식이 잘 못 되었습니다."
                if (res.code === "001") msg = "이번주 닉네임을 이미 변경하였습니다."
                if (res.code === "002") msg = "새 비밀번호 형식이 잘 못 되었습니다."
                break;
            case 403:
                msg = "비밀번호가 틀렸습니다."
                break;
            case 500:
                msg = "서버 오류가 발생하였습니다."
                break;
        }

        return {success: false, msg: msg};
    }
    return {success: true};
}


/**
 * User unregister
 * */
export async function requestUserUnregister(password) {
    return await http.delete(API.User.unregister.url, {password});
}