import http from "../../utils/http";
import {API} from "../../utils/api";


// Action types
export const actionType = {
    REGISTER_REQUEST: "registration/REGISTER_REQUEST"
};


// Initial state
const initialState = {
    id: null,
    password: null,
    email: null,
    nickname: null
};


// Reducer
export default function reducer(state=initialState, action) {
    switch (action.type) {
        case actionType.REGISTER_REQUEST:
            return {
                ...state,
                ...{id: action.id,
                    password: action.password,
                    email: action.email,
                    nickname: action.nickname}
            }
        default:
            return state;
    }
};


// Actions
export async function checkDuplicateRequest(key, value) {
    const res = await http.get(API.User.checkDuplicate.url, {[key]: value});
    return res.data;
}

export function registerRequest(id, password, email, nickname) {
    return {
        type: actionType.REGISTER_REQUEST,
        id: id,
        password: password,
        email: email,
        nickname: nickname
    }
}