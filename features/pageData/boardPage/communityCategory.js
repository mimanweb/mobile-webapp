import http from "../../../utils/http";
import {API} from "../../../utils/api";
import {BOARD_LIST_TYPE} from "../../../utils/index/board";



/**
 * Community Humor page data
 * */
export async function requestCommunityNoticePageData(pageNumber, listType=BOARD_LIST_TYPE.ALL, context) {
    const {status, data, isServerDown} = await http.get(API.PageData.board.community.notice.url, {pageNumber, listType}, undefined, context);

    return {
        status: status,
        isServerDown: isServerDown,
        board: data? data.board: null,
    }
}


/**
 * Community Freeboard page data
 * */
export async function requestCommunityFreeboardPageData(pageNumber, listType=BOARD_LIST_TYPE.ALL, context) {
    const {status, data, isServerDown} = await http.get(API.PageData.board.community.freeboard.url, {pageNumber, listType}, undefined, context);

    return {
        status: status,
        isServerDown: isServerDown,
        board: data? data.board: null,
    }
}


/**
 * Community Humor page data
 * */
export async function requestCommunityHumorPageData(pageNumber, listType=BOARD_LIST_TYPE.ALL, context) {
    const {status, data, isServerDown} = await http.get(API.PageData.board.community.humor.url, {pageNumber, listType}, undefined, context);

    return {
        status: status,
        isServerDown: isServerDown,
        board: data? data.board: null,
    }
}


/**
 * Community Horror page data
 * */
export async function requestCommunityHorrorPageData(pageNumber, listType=BOARD_LIST_TYPE.ALL, context) {
    const {status, data, isServerDown} = await http.get(API.PageData.board.community.horror.url, {pageNumber, listType}, undefined, context);

    return {
        status: status,
        isServerDown: isServerDown,
        board: data? data.board: null,
    }
}


/**
 * Community Food page data
 * */
export async function requestCommunityFoodPageData(pageNumber, listType=BOARD_LIST_TYPE.ALL, context) {
    const {status, data, isServerDown} = await http.get(API.PageData.board.community.food.url, {pageNumber, listType}, undefined, context);

    return {
        status: status,
        isServerDown: isServerDown,
        board: data? data.board: null,
    }
}
