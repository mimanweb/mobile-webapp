import {all, fork, call, delay, put, take, takeLatest} from 'redux-saga/effects'
import {actionTypes as userActionTypes} from "./reducer";
import {loginRequest, loginSuccess, loginFailure, logoutRequest, logoutSuccess, logoutFailure} from './actions'
import {API} from "../../utils/api";
import axios from 'axios';
import router from 'next/router'
import {errBehave} from "../../utils/http";


/**
 * Login request
 * */
function* watchLogin({userId, userPassword, stayLoggedIn, referer}) {
    try {
        const params = {id: userId, password: userPassword, stayLoggedIn: stayLoggedIn};
        yield call(() => {
            return axios.post(
                API.User.login.url,
                {},
                {withCredentials: true, headers: {Authorization: `Bearer ${JSON.stringify(params)}`}})
        });


        const siteRefererRegexPattern = `https?:\/\/(|[\w\.]+)${process.env.WEBSITE_ORIGIN_REFERER}`;
        const isSelfRequested = referer && new RegExp(`^${siteRefererRegexPattern}(\/|$)`).test(referer);
        console.log(referer, isSelfRequested, process.env.WEBSITE_ORIGIN_REFERER, new RegExp(`^${siteRefererRegexPattern}(\/|$)`))
        /**
         * Webapp accessed from itself
         * Route to referer(prev) path
         * */
        if (isSelfRequested) {
            const match = new RegExp(`^${siteRefererRegexPattern}(.*)`).exec(referer);console.log(referer, match[2])
            const refererPath = match[2] || "/";
            console.log(match)
            router.push(refererPath);
        }
        /**
         * Webapp requested without referer
         * Might accessed from other website or from a new tab
         * Go to home
         * */
        else {
            router.push("/");
        }
    } catch (e) {
        yield errBehave(e, 400, "000", ()=>{alert("아이디/비밀번호를 입력해 주십시오.")});
        yield errBehave(e, 403, "000", ()=>{alert("아이디/비밀번호가 옳지 않습니다.")});
        yield errBehave(e, 404, "000", ()=>{alert("아이디/비밀번호가 옳지 않습니다.")});
        yield errBehave(e, 500, null, ()=>{alert("서버 오류가 발생하였습니다.")});
        yield put(loginFailure(e));
        router.reload();
    }
}


/**
 * Logout request
 * */
function* watchLogout() {
    yield call(() => axios.post(API.User.logout.url, {}, {withCredentials: true}));
    router.reload();
}


export default function* rootSaga() {
    yield all([
        takeLatest(userActionTypes.LOGIN_REQUEST, watchLogin),
        takeLatest(userActionTypes.LOGOUT_REQUEST, watchLogout)
    ])
};
