import http from "../../utils/http";


// Action types
import { actionTypes } from './reducer';

// URL
import {API} from "../../utils/api";


export function loginRequest(userId, userPassword, stayLoggedIn, referer) {
    return {
        type: actionTypes.LOGIN_REQUEST,
        userId: userId,
        userPassword: userPassword,
        stayLoggedIn: stayLoggedIn,
        referer: referer
    };
}

export function loginSuccess(payload) {
    return {
        type: actionTypes.LOGIN_SUCCESS,
        payload: payload
    };
}

export function loginFailure(err) {
    return {
        type: actionTypes.LOGIN_FAILURE,
        error: err
    };
}

export function logoutRequest() {
    return {
        type: actionTypes.LOGOUT_REQUEST
    }
}

export function logoutSuccess() {
    return {
        type: actionTypes.LOGOUT_SUCCESS
    }
}

export function logoutFailure() {
    return {
        type: actionTypes.LOGOUT_FAILURE
    }
}


/**
 * Set user unread notification state
 * */
export function setUserUnreadNotificationState(state) {
    return {
        type: actionTypes.SET_NOTIFICATION_STATE,
        payload: {
            hasUnreadNotification: state
        }
    }
}


/**
 * Set user notification list state
 * */
export function setUserNotificationListState(list, totalPageCount, currPage) {
    return {
        type: actionTypes.SET_NOTIFICATION_STATE,
        payload: {
            notificationList: list,
            notificationTotalPageCount: totalPageCount,
            notificationPage: currPage,
        }
    }
}



// Server api requests

/**
 * Request user login verification
 * */
export async function requestLoginVerify(context) {
    return await http.get(API.User.loginVerify.url, undefined, undefined, context);
}


/**
 * Request user notification list
 * */
export async function requestUserNotificationList(pageToSearch, itemsPerPage) {
    return await http.get(API.User.getNotifications.url, {pageToSearch, itemsPerPage});
}


/**
 * Request set user notification data as checked
 * */
export async function requestSetUserNotificationChecked(notificationDataId) {
    return await http.put(API.User.setNotificationChecked.url, {notificationDataId});
}


/**
 * Request set all user notification data as checked
 * */
export async function requestSetAllUserNotificationsChecked() {
    return await http.put(API.User.setAllNotificationsChecked.url);
}

