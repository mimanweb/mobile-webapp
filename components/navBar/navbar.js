import React from "react";
import style from "./navbar.module.scss"
import Link from "next/link";
import {useSelector} from "react-redux";



const LoginPanel = () => {
    return (
        <div className={style["login_panel"]}>
            <Link href={"/account/login"}>
                <a>로그인</a>
            </Link>
        </div>
    )
}


const UserMenuPanel = () => {
    return (
        <div className={style["user_info"]}>
            <section className={style["divider"]}>
                <i/>
            </section>


            <div>
                <Link href={"/account/login"}>
                    <a className={style["notification"]}><div className={style["icon"]}/></a>
                </Link>
            </div>

            <div>
                <Link href={"/account/login"}>
                    <a className={style["user_info"]}><div className={style["icon"]}/></a>
                </Link>
            </div>
        </div>
    )
}



export default function NavBar() {
    const {loggedIn} = useSelector(state => state.user);


    return (
        <nav className={style["container"]}>

            {/** LOGO */}
            <section className={style["logo"]}>
                <div className={style["icon"]}>
                    <Link href={"/"}>
                        <a>MIMANWEB</a>
                    </Link>
                </div>
            </section>


            {/** INFO */}
            <section className={style["info"]}>
                {!loggedIn? (
                    <LoginPanel/>
                ): (
                    <UserMenuPanel/>
                )}
            </section>
        </nav>
    )
};