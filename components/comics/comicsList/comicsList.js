import React, {useRef, useState, forwardRef, useImperativeHandle} from "react";
import {useDispatch, useSelector} from "react-redux";
import style from "./comicsList.module.scss";
import {
    setComicsListState,
    requestAllComicsMatchTitle,
    requestAllComicsMatchTitleIndexChar, setComicsListPageState, setComicsSearchTextState
} from "../../../features/comics/actions";
import Keyboard from "../../../utils/editor/keyboard";
import _ from "underscore";
import {useRouter} from "next/router"
import Link from "next/link";
import HorizontalTextPageNavigator from "../../pageNavigator/pageNavigator";
import {requestComicsTranslatePageData} from "../../../features/pageData/actions";
import {COMMENT_PAGE_PER_INDEX} from "../../../utils/index/comment";
import {COMICS_SEARCH_LIST_PAGE_GROUP_COUNT} from "../../../utils/index/comics/comics";




const AddNewComicsButton = () => {
    return (
        <div className={style["add_new_comics"]}>
            <div>
                <div className={style["icon"]}/>
                <p>볼륨/이슈 등록</p>
            </div>
        </div>
    )
}


const ComicsItem = ({volume, issue, imgUrl, translator}) => {
    return (
        <div className={style["comics_item"]}>

            {/** COMICS IMAGE THUMBNAIL*/}
            <section className={style["thumbnail"]}>
                <img src={imgUrl}/>
            </section>


            {/** COMICS INFO */}
            {/** COMICS TITLE, VOLUME/ISSUE, UPDATE TIME */}
            <section className={style["info"]}>
                <div className={style["ep"]}>
                    <p className={style["volume"]}>볼륨{volume}</p>
                    <p className={style["issue"]}>이슈{issue}</p>
                </div>
            </section>
        </div>
    )
}


const ComicsInfo = ({title, dataOid, linkIndex, dataId, itemToFocus}) => {
    const router = useRouter();

    const userState = useSelector(state => state.user);
    const isRegUser = userState.loggedIn;


    const toWritePage = async () => {
        await router.push({
            pathname: `/write/comics/book`,
            query: {
                id: dataOid
            }
        });
    }



    return (
        <div className={`${style["comics_info"]} ${dataOid===itemToFocus? style["highlight"]: ""}`}>
            <section className={style["title"]}>
                <div className={style["status"]}>
                    <div className={style["icon"]}/>
                    번역가능
                </div>

                <Link href={{
                    pathname: `/comics/book/${linkIndex}/${dataId}`
                }}>
                    <a>
                        <p className={style["kor"]}>{title.korean}</p>
                        <p className={style["origin"]}>{title.origin}</p>
                    </a>
                </Link>

                {isRegUser && (
                    <div className={style["add_new_comics"]}>
                        <div onClick={toWritePage}>
                            <div className={style["icon"]}/>
                            <p>볼륨/이슈 등록</p>
                        </div>
                    </div>
                )}
            </section>

            {/*<section className={style["list"]}>*/}
            {/*    <div>*/}
            {/*        <section className={style["translator"]}>*/}
            {/*            <span>번역 </span>*/}
            {/*            <span className={style["nickname"]} title="bonbon900">봉봉9000</span>*/}
            {/*        </section>*/}
            {/*        <section>*/}
            {/*            <ComicsItem*/}
            {/*                volume={1}*/}
            {/*                issue={1}*/}
            {/*                imgUrl="https://cdn.imagecomics.com/assets/i/releases/550524/the-walking-dead-the-alien-hc_89b71a3968.jpg"/>*/}
            {/*            <ComicsItem*/}
            {/*                volume={1}*/}
            {/*                issue={2}*/}
            {/*                imgUrl="https://cdn.imagecomics.com/assets/i/releases/256276/the-walking-dead-book-15-hc_1a28c5019b.jpg"/>*/}
            {/*            <ComicsItem*/}
            {/*                volume={1}*/}
            {/*                issue={3}*/}
            {/*                imgUrl="https://cdn.imagecomics.com/assets/i/releases/154611/the-walking-dead-vol-30-new-world-order-tp_f19e6ee641.jpg"/>*/}
            {/*            <ComicsItem*/}
            {/*                volume={2}*/}
            {/*                issue={1}*/}
            {/*                imgUrl="https://cdn.imagecomics.com/assets/i/releases/27573/the-walking-dead-vol-29-lines-we-cross-tp_aaafa9f216.jpg"/>*/}
            {/*            <ComicsItem*/}
            {/*                volume={2}*/}
            {/*                issue={1}*/}
            {/*                imgUrl="https://cdn.imagecomics.com/assets/i/releases/27573/the-walking-dead-vol-29-lines-we-cross-tp_aaafa9f216.jpg"/>*/}
            {/*            <ComicsItem*/}
            {/*                volume={2}*/}
            {/*                issue={1}*/}
            {/*                imgUrl="https://cdn.imagecomics.com/assets/i/releases/27573/the-walking-dead-vol-29-lines-we-cross-tp_aaafa9f216.jpg"/>*/}
            {/*            <ComicsItem*/}
            {/*                volume={2}*/}
            {/*                issue={1}*/}
            {/*                imgUrl="https://cdn.imagecomics.com/assets/i/releases/27573/the-walking-dead-vol-29-lines-we-cross-tp_aaafa9f216.jpg"/>*/}
            {/*            <ComicsItem*/}
            {/*                volume={2}*/}
            {/*                issue={1}*/}
            {/*                imgUrl="https://cdn.imagecomics.com/assets/i/releases/27573/the-walking-dead-vol-29-lines-we-cross-tp_aaafa9f216.jpg"/>*/}
            {/*        </section>*/}
            {/*    </div>*/}
            {/*    <div>*/}
            {/*        <section className={style["translator"]}>*/}
            {/*            <span>번역 </span>*/}
            {/*            <span className={style["nickname"]} title="bonbon900">봉봉9000</span>*/}
            {/*        </section>*/}
            {/*        <section>*/}
            {/*            <ComicsItem*/}
            {/*                volume={1}*/}
            {/*                issue={1}*/}
            {/*                imgUrl="https://cdn.imagecomics.com/assets/i/releases/550524/the-walking-dead-the-alien-hc_89b71a3968.jpg"/>*/}
            {/*            <ComicsItem*/}
            {/*                volume={1}*/}
            {/*                issue={2}*/}
            {/*                imgUrl="https://cdn.imagecomics.com/assets/i/releases/256276/the-walking-dead-book-15-hc_1a28c5019b.jpg"/>*/}
            {/*            <ComicsItem*/}
            {/*                volume={1}*/}
            {/*                issue={3}*/}
            {/*                imgUrl="https://cdn.imagecomics.com/assets/i/releases/154611/the-walking-dead-vol-30-new-world-order-tp_f19e6ee641.jpg"/>*/}
            {/*            <ComicsItem*/}
            {/*                volume={2}*/}
            {/*                issue={1}*/}
            {/*                imgUrl="https://cdn.imagecomics.com/assets/i/releases/27573/the-walking-dead-vol-29-lines-we-cross-tp_aaafa9f216.jpg"/>*/}
            {/*            <ComicsItem*/}
            {/*                volume={2}*/}
            {/*                issue={1}*/}
            {/*                imgUrl="https://cdn.imagecomics.com/assets/i/releases/27573/the-walking-dead-vol-29-lines-we-cross-tp_aaafa9f216.jpg"/>*/}
            {/*        </section>*/}
            {/*    </div>*/}
            {/*</section>*/}
        </div>
    )
}




const SearchIndexButton = ({option, searchFunc}) => {
    const comicsState = useSelector(state => state.comics);

    const {searchText} = comicsState;



    return (
        <div className={`${style["search_index_button"]} ${option.textToSearch===searchText? style["selected"]: ""}`} onClick={()=>searchFunc(option.textToSearch)}>{option.buttonText}</div>
    )
}


/**
 * MAIN
 * */
const ComicsList = forwardRef(({}, ref) => {
    const dispatch = useDispatch();

    const [itemToFocus, setItemToFocus] = useState(null);

    const comicsState = useSelector(state => state.comics);

    const {comicsList, itemsPerPage, totalPageCount, page, searchText} = comicsState;


    const searchInputRef = useRef(null);


    const currPageIndex = Math.ceil(page/COMICS_SEARCH_LIST_PAGE_GROUP_COUNT);

    const pageStartOfCurrPageIndex = (currPageIndex-1)*COMICS_SEARCH_LIST_PAGE_GROUP_COUNT;
    const pageEndOfCurrPageIndex = (currPageIndex)*COMICS_SEARCH_LIST_PAGE_GROUP_COUNT;




    /**
     * Search index button click
     * */
    const setComicsSearchIndex = async (text) => {
        dispatch(setComicsSearchTextState(text));

        await searchComicsList(text, 1);
    }

    /**
     * Search result panel page button click
     * Search comics list match title index char
     * */
    const searchComicsList = async (textToSearchTitleIndexChar, pageToSearch) => {

        const {status, data} = await requestAllComicsMatchTitleIndexChar(textToSearchTitleIndexChar, pageToSearch);

        const {comicsList, totalCount, itemsPerPage, searchedPage} = data;

        switch (status) {
            case 200:
                dispatch(setComicsListPageState(comicsList, itemsPerPage, totalCount, searchedPage));
                break;
        }
    }



    const searchButtonClick = async (pageToSearch) => {
        setItemToFocus(null);

        const textToSearch = searchInputRef.current.value;

        if (
            !textToSearch ||
            !/[\S]/g.test(textToSearch)
        ) return;


        const {status, data} = await requestAllComicsMatchTitle(textToSearch, pageToSearch);

        switch (status) {
            case 200:
                const {comicsList, totalCount, itemsPerPage, searchedPage} = data;
                dispatch(setComicsListPageState(comicsList, itemsPerPage, totalCount, searchedPage));
                break;
            case 404:
                dispatch(setComicsListPageState([], 10, 0, 1));
                break;
        }

        dispatch(setComicsSearchTextState(null));
    }

    const onSearchBarKeyDown = async (e) => {
        if (e.keyCode===Keyboard.KEY.ENTER) {
            await searchButtonClick();
        }
    }



    useImperativeHandle(ref, () => ({
        search: async (textToSearch, itemToFocus) => {
            searchInputRef.current.value = textToSearch;
            await searchButtonClick();

            setItemToFocus(itemToFocus);
        }
    }))



    return (
        <div className={style["container"]}>

            <section className={style["title"]}>
                <p className={style["logo"]}>MIMANWEB</p>
                <p className={style["title"]}>코믹스 목록</p>
            </section>

            <section className={style["comics_search"]}>
                <div className={style["search_bar"]}>
                    <div className={style["icon"]}/>
                    <input type="text" spellCheck={false} onKeyDown={onSearchBarKeyDown} ref={searchInputRef}/>
                </div>

                {/** SEARCH BUTTON */}
                <div className={style["search_button"]}>
                    <p onClick={()=>searchButtonClick()}>검색하기</p>
                </div>
            </section>

            <section className={style["search_indexes"]}>
                {[
                    {textToSearch: "", buttonText: "ALL"},
                    {textToSearch: "0123456789", buttonText: "0 - 9"},
                    {textToSearch: "abcdefghijklmnopqrstuvwxyz", buttonText: "A - Z"},
                    // {textToSearch: "ㄱㄲㄴㄷㄸㄹㅁㅂㅃㅅㅆㅇㅈㅉㅊㅋㅌㅍㅎ", buttonText: "ㄱ - ㅎ"},
                    {textToSearch: "ㄱㄲㄴㄷㄸ", buttonText: "ㄱ - ㄷ"},
                    {textToSearch: "ㄹㅁㅂㅃㅅㅆ", buttonText: "ㄹ - ㅅ"},
                    {textToSearch: "ㅇㅈㅉㅊㅋㅌㅍㅎ", buttonText: "ㅇ - ㅎ"},
                ].map((option, key) => {
                    return (
                        <SearchIndexButton key={key} option={option} searchFunc={setComicsSearchIndex}/>
                    )
                })}
            </section>

            <section className={style["comics_list"]}>
                {_.isEmpty(comicsList) && (
                    <div className={style["no_result_message"]}>검색결과가 없습니다.</div>
                )}
                {comicsList.map((comics, idx) => {
                    return <ComicsInfo key={idx} title={comics.title} dataOid={comics._id} dataId={comics.id} linkIndex={comics.linkIndex} itemToFocus={itemToFocus}/>
                })}

            </section>

            <section className={style["page_nav"]}>
                <HorizontalTextPageNavigator
                    isLinkMode={false}
                    currPage={page}
                    totalPageCount={totalPageCount}
                    pagePerPageGroupCount={COMICS_SEARCH_LIST_PAGE_GROUP_COUNT}
                    toFirstPageFunc={searchText!==null? ()=>searchComicsList(searchText, 1): ()=>searchButtonClick(1)}
                    toPrevPageFunc={searchText!==null? ()=>searchComicsList(searchText, pageStartOfCurrPageIndex): ()=>searchButtonClick(pageStartOfCurrPageIndex)}
                    toPageFunc={searchText!==null? (_page)=>searchComicsList(searchText, _page): (_page)=>searchButtonClick(_page)}
                    toNextPageFunc={searchText!==null? ()=>searchComicsList(searchText, pageEndOfCurrPageIndex+1): ()=>searchButtonClick(pageEndOfCurrPageIndex+1)}
                />
            </section>
        </div>
    )
})



export default ComicsList;