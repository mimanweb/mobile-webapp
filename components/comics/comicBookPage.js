import React, {useState, useEffect, useRef} from "react";
import LeftSidebar from "../sideBar/left-sidebar";
import RightSidebar from "../sideBar/right-sidebar";
import style from "./comicBookPage.module.scss";
import ComicsContentWarningPanel from "./contentWarning";
import {useDispatch, useSelector} from "react-redux";
import {COMIC_BOOK_CONTENT_TYPE} from "../../utils/index/comics/book";
import {parseImgId} from "../../utils/image/parse";
import Article from "../post/article";
import CommentPanel from "../post/comment";
import DocHead from "../meta/head";
import Link from "next/link";
import XSmallButton, {XSmallButtonType} from "../button/x-small";
import {requestDeleteComicBookData} from "../../features/comicsBook/actions";
import router from "next/router";
import _ from "underscore";
import {parseRecentComicsListText} from "../../utils/format";
import {updateRecentReadComicsList} from "../../utils/index/comics/comics";



const SinglePage = ({img}) => {
    return (
        <div className={style["single_page"]}>
            <img src={img}/>
        </div>
    )
}


const WidePage = ({img}) => {
    return (
        <div className={style["wide_page"]}>
            <img src={img}/>
        </div>
    )
}


const DoublePage = ({img1, img2}) => {
    return (
        <div className={style["double_page"]}>
            <img src={img1}/>
            <img src={img2}/>
        </div>
    )
}






const ComicsBookPage = () => {
    const dispatch = useDispatch();

    const userState = useSelector(state => state.user);
    const comicsState = useSelector(state => state.comics);
    const comicBookState = useSelector(state => state.comicBook);

    const {title: comicsTitle, comicsNumber, comicsId} = comicsState;
    const {
        comicBookDataId,
        contentType,
        link: comicBookLink,
        linkThumbnail: comicBookLinkThumbnail,
        isEditionTitle, isShortStory, isVolume,
        editionTitle, volume, issue,
        owner: comicBookOwner
    } = comicBookState;


    const comicsPagePath = `/comics/book/${comicsNumber}/${comicsId}`;
    const isContentTypeImage = contentType===COMIC_BOOK_CONTENT_TYPE.IMAGE;
    const isContentTypeLink = contentType===COMIC_BOOK_CONTENT_TYPE.LINK;
    const isMyComicBook = userState.loggedIn && userState.id===comicBookOwner.userId;



    const [dark, setDark] = useState(false);



    const contentWarningSectionRef = useRef(null);
    const rightSideBarRef = useRef(null);




    const editButtonClick = async () => {
        await router.push({
            pathname: "/edit/comics/book",
            query: {
                id: comicBookDataId
            }
        });
    }

    const deleteButtonClick = async () => {
        const {status} = await requestDeleteComicBookData(comicBookDataId);

        switch (status) {
            case 200:
                await router.push(comicsPagePath);
                break;
        }
    }




    /**
     * Update recent read comics list in LocalStorage
     * */
    useEffect(async () => {
        if (comicBookDataId) {
            const listTxt = localStorage.getItem("recentReadComics")

            if (!listTxt) {
                localStorage.setItem("recentReadComics", `${comicsNumber}_${comicBookDataId}`);
            } else {
                const recentComicsInfoList = parseRecentComicsListText(listTxt);


                const newList = [`${comicsNumber}_${comicBookDataId}`];

                recentComicsInfoList.forEach(info => {
                    if (newList.length<7 && info.comicsNumber!==comicsNumber) {
                        newList.push(`${info.comicsNumber}_${info.comicBookDataId}`);
                    }
                })


                localStorage.setItem("recentReadComics", newList.join(","));


                await updateRecentReadComicsList(dispatch)
            }
        }
    }, [comicBookDataId])


    useEffect(() => {
        const _body = document.body;
        const _html = _body.parentNode;

        if (dark) {
            // _body.style.transition = "background-color 2s"
            _body.style.backgroundColor = "transparent";
            _html.style.transition = "background-color 1.6s"
            _html.style.backgroundColor = "#161616";
        } else {
            _body.style.backgroundColor = "transparent";
            _html.style.transition = "background-color 1.2s"
            _html.style.backgroundColor = "";
        }
    }, [dark])



    /**
     * Set scroll event
     * */
    let ticking;

    const onScroll = () => {
        const scrollPos = window.scrollY;
        const scrollBottomPos = window.innerHeight + scrollPos;
        const windowMiddlePos = window.innerHeight/2 + scrollPos;
        const contentWarningSectionTopPos = contentWarningSectionRef.current.offsetTop;
        const contentWarningSectionBottomPos = contentWarningSectionRef.current.offsetHeight + contentWarningSectionRef.current.offsetTop;


        if (!ticking) {
            window.requestAnimationFrame(function() {
                if (windowMiddlePos>contentWarningSectionTopPos) {
                    setDark(true);
                }
                else if (windowMiddlePos<=contentWarningSectionTopPos) {
                    setDark(false);
                }
                ticking = false;
            });

            ticking = true;
        }
    }

    useEffect(() => {
        /**
         * Add scroll event listener
         * */
        if (isContentTypeImage) {
            rightSideBarRef.current.removeStickyFromAd();

            window.addEventListener("scroll", onScroll);
        }


        return () => {
            if (isContentTypeImage) {
                window.removeEventListener("scroll", onScroll);
            }
        }
    }, [])



    return (
        <>
            <DocHead title={`${comicsTitle.korean}${isEditionTitle? ":"+editionTitle: ""}${(!isShortStory && isVolume)? ` vol${volume}`: ""}${!isShortStory? ` issue${issue}`: ""} | 미만웹`}/>
            
            <LeftSidebar/>

            <main className={style["container"]}>
                <section className={style["comics_info"]}>
                    <div className={style["title"]}>
                        <Link href={comicsPagePath}>
                            <a>{comicsTitle.korean}</a>
                        </Link>
                    </div>
                    <div className={style["origin_title"]}>
                        <Link href={comicsPagePath}>
                            <a>{comicsTitle.origin}</a>
                        </Link>
                    </div>

                    <div className={style["sub_title_volume_issue"]}>
                        {isEditionTitle && (
                            <p className={style["sub_title"]}>{editionTitle}</p>
                        )}
                        {!isShortStory && (
                            <p className={style["volume_issue"]}>{isVolume && `vol${volume} `}issue{issue}</p>
                        )}
                    </div>
                </section>

                <section className={style["comics_cover"]}>
                    {isContentTypeLink && (
                        <div className={style['link_comic_book']}>
                            <a className={style["link_url"]} href={comicBookLink} target="_blank">
                                <div className={style["icon"]}/>
                                <p>{comicBookLink}</p>
                            </a>
                            <section>
                                <a className={style["book_link"]} href={comicBookLink} target="_blank">
                                    {comicBookLinkThumbnail? (
                                        <img src={parseImgId(comicBookLinkThumbnail)}/>
                                    ): (
                                        <div className={style["no_thumbnail"]}>
                                            <div>
                                                <p>{comicsTitle.korean}</p>
                                                <p>{comicsTitle.origin}</p>
                                            </div>
                                        </div>
                                    )}

                                    <p>보러가기</p>
                                </a>
                            </section>
                        </div>
                    )}

                    {isContentTypeImage && (
                        <div className={style["image_comic_book"]}>
                            <img src="https://cdn.imagecomics.com/assets/i/releases/605811/moonstruck-vol-3-troubled-waters-tp_f6a7a7ee80.jpg"/>
                        </div>
                    )}
                </section>

                {isContentTypeImage && (
                    <>
                        <section className={style["content_warning"]} ref={contentWarningSectionRef}>
                            <ComicsContentWarningPanel/>
                        </section>

                        <section className={style["comics_content"]}>
                            <SinglePage img="https://i.annihil.us/u/prod/marvel/i/mg/5/90/5f4faf8edc1dc/clean.jpg"/>
                            <SinglePage img="https://i.annihil.us/u/prod/marvel/i/mg/5/90/5f4faf8edc1dc/clean.jpg"/>
                            <SinglePage img="https://cdn.imagecomics.com/assets/i/releases/601640/getting-it-together-3-of-4_6187d9337b.jpg"/>
                            <DoublePage
                                img1="https://cdn.imagecomics.com/assets/i/releases/601640/getting-it-together-3-of-4_6187d9337b.jpg"
                                img2="https://cdn.imagecomics.com/assets/i/releases/601640/getting-it-together-3-of-4_6187d9337b.jpg"/>
                            <DoublePage
                                img1="https://i.annihil.us/u/prod/marvel/i/mg/3/f0/5fa956e149470/clean.jpg"
                                img2="https://i.annihil.us/u/prod/marvel/i/mg/3/90/5f59434317ec1/clean.jpg"/>
                            <WidePage img="https://i.annihil.us/u/prod/marvel/i/mg/5/90/5f4faf8edc1dc/clean.jpg"/>
                            <SinglePage img="https://i.annihil.us/u/prod/marvel/i/mg/5/90/5f4faf8edc1dc/clean.jpg"/>
                        </section>
                    </>
                )}



                {isMyComicBook && (
                    <section className={style["edit_delete_buttons"]}>
                        <XSmallButton buttonType={XSmallButtonType.COMIC_BOOK.EDIT} onClick={editButtonClick}/>
                        <XSmallButton buttonType={XSmallButtonType.COMIC_BOOK.DELETE} onClick={deleteButtonClick}/>
                    </section>
                )}



                <section className={style["post"]}>
                    <div>
                        <section className={style["translator_review"]}>
                            <span>번역자 후기</span>
                        </section>

                        <section className={style["post"]}>
                            <Article hideTitle={true} hideEditMenus={true} hideMetaInfo={true}/>
                        </section>
                    </div>
                </section>

                <section className={style["comment"]}>
                    <CommentPanel/>
                </section>

            </main>

            <RightSidebar ref={rightSideBarRef}/>
        </>
    )
}



export default ComicsBookPage;