import React, {useState, useEffect, forwardRef, useRef, useImperativeHandle} from "react";
import style from "./comicsPage.module.scss";
import LeftSidebar from "../sideBar/left-sidebar";
import RightSidebar from "../sideBar/right-sidebar";
import ComicsEditionList from "./comicsEditionList/comicsEditionList";
import {useSelector} from "react-redux";
import _ from "underscore";
import {
    requestAddNewComicsWriter, requestAddComicsWriter, requestComicsWritersMatchText,
    requestAddNewComicsArtist, requestAddComicsArtist, requestComicsArtistsMatchText,
    requestAddNewComicsPublisher, requestUpdateComicsPublisher, requestComicsPublishersMatchText,
    requestAllComicsGenres, requestAddComicsGenre,
    requestAllComicsContentDescriptors, requestAddComicsContentDescriptor,
    requestUpdateComicsCoverImage,
    requestUpdateComicsSummary
} from "../../features/comics/actions";
import {useRouter} from "next/router";
import Keyboard from "../../utils/editor/keyboard";
import {StretchOpenMessageBoard as ComicsCoverImageInputMessage, StretchOpenBoardType, StretchOpenMessageType} from "../../components/panel/index";
import {dataURLtoFile} from "../../utils/image";
import {parseImgId} from "../../utils/image/parse";
import DocHead from "../meta/head";




const COMICS_INFO_TYPE = {
    WRITER: "WRITER",
    ARTIST: "ARTIST",
    PUBLISHER: "PUBLISHER",
    GENRE: "GENRE",
    CONTENT_DESCRIPTOR: "CONTENT_DESCRIPTOR",
}



/**
 * ITEM ADD MENU PANEL
 * */
const AddMenuPanel = forwardRef(({infoType}, ref) => {
    const router = useRouter();

    const comicsState = useSelector(state => state.comics);

    const {comicsDataId} = comicsState;

    const [isVisible, setIsVisible] = useState(false);
    const [inputMode, setInputMode] = useState(false);
    const [textToAdd, setTextToAdd] = useState("");
    const [textToSearch, setTextToSearch] = useState("");
    const [searchedItemList, setSearchedItemList] = useState([]);




    const findInfoItems = async () => {
        if (
            ((infoType!==COMICS_INFO_TYPE.GENRE) && !textToSearch) &&
            ((infoType!==COMICS_INFO_TYPE.CONTENT_DESCRIPTOR) && !textToSearch)
        ) return;

        let status;
        let list;

        switch (infoType) {
            case COMICS_INFO_TYPE.WRITER:
                const queryWritersRes = await requestComicsWritersMatchText(textToSearch);

                status = queryWritersRes.status;
                list = queryWritersRes.data.writerList;
                break;
            case COMICS_INFO_TYPE.ARTIST:
                const queryArtistsRes = await requestComicsArtistsMatchText(textToSearch);

                status = queryArtistsRes.status;
                list = queryArtistsRes.data.artistList;
                break;
            case COMICS_INFO_TYPE.PUBLISHER:
                const queryPublishersRes = await requestComicsPublishersMatchText(textToSearch);

                status = queryPublishersRes.status;
                list = queryPublishersRes.data.publisherList;
                break;
            case COMICS_INFO_TYPE.GENRE:
                const queryGenreRes = await requestAllComicsGenres();

                status = queryGenreRes.status;
                list = queryGenreRes.data.genreList;
                break;
            case COMICS_INFO_TYPE.CONTENT_DESCRIPTOR:
                const queryContentDescriptorRes = await requestAllComicsContentDescriptors();

                status = queryContentDescriptorRes.status;
                list = queryContentDescriptorRes.data.contentDescriptorList;
                break;
        }

        switch (status) {
            case 200:
                setSearchedItemList(list);
                break;
            case 403:
                router.reload();
                break;
        }
    }


    const updateComicsInfoWithItem = async (item) => {
        let status;

        switch (infoType) {
            case COMICS_INFO_TYPE.WRITER:
                const addWriterRes = await requestAddComicsWriter(comicsDataId, item);

                status = addWriterRes.status;
                break;
            case COMICS_INFO_TYPE.ARTIST:
                const addArtistRes = await requestAddComicsArtist(comicsDataId, item);

                status = addArtistRes.status;
                break;
            case COMICS_INFO_TYPE.PUBLISHER:
                const updatePublisherRes = await requestUpdateComicsPublisher(comicsDataId, item);

                status = updatePublisherRes.status;
                break;
            case COMICS_INFO_TYPE.GENRE:
                const addGenreRes = await requestAddComicsGenre(comicsDataId, item);

                status = addGenreRes.status;
                break;
            case COMICS_INFO_TYPE.CONTENT_DESCRIPTOR:
                const addContentDescriptorRes = await requestAddComicsContentDescriptor(comicsDataId, item);

                status = addContentDescriptorRes.status;
                break;
        }

        switch (status) {
            case 200:
            case 403:
            case 404:
                router.reload();
                break;
        }
    }


    const createNewInfoAndAdd = async () => {
        if (!textToAdd) return;

        let status;

        switch (infoType) {
            case COMICS_INFO_TYPE.WRITER:
                const addNewWriterRes = await requestAddNewComicsWriter(comicsDataId, textToAdd);

                status = addNewWriterRes.status;
                break;
            case COMICS_INFO_TYPE.ARTIST:
                const addNewArtistRes = await requestAddNewComicsArtist(comicsDataId, textToAdd);

                status = addNewArtistRes.status;
                break;
            case COMICS_INFO_TYPE.PUBLISHER:
                const addNewPublisherRes = await requestAddNewComicsPublisher(comicsDataId, textToAdd);

                status = addNewPublisherRes.status;
                break;
        }

        switch (status) {
            case 200:
            case 403:
            case 404:
                router.reload();
                break;
        }
    }



    const onTextToSearchFieldKeyDown = async (e) => {
        if (e.keyCode===Keyboard.KEY.ENTER) {
            await findInfoItems();
        }
    }

    /**
     * Toggle add new mode
     * */
    const toAddNewItemMode = () => {
        setInputMode(true);
    }

    const cancelAddNewMode = () => {
        setInputMode(false);
    }


    useImperativeHandle(ref, () => ({
        enable: () => {
            setIsVisible(true);
        }
    }))


    useEffect(async () => {

        if (!isVisible) return;

        switch (infoType) {
            case COMICS_INFO_TYPE.GENRE:
            case COMICS_INFO_TYPE.CONTENT_DESCRIPTOR:
                await findInfoItems()
                break;
        }
    }, [isVisible])



    const SearchedItem = ({idx, item, size}) => {
        switch (infoType) {
            case COMICS_INFO_TYPE.ARTIST:
            case COMICS_INFO_TYPE.WRITER:
            case COMICS_INFO_TYPE.PUBLISHER:
                return (
                    <div key={idx} className={style["searched_item"]}>
                        <span onClick={()=>updateComicsInfoWithItem(item._id)}>{item.name.origin}</span>
                        {idx!==size-1 && (<span>, </span>)}
                    </div>
                )
            case COMICS_INFO_TYPE.GENRE:
                return (
                    <div key={idx} className={style["searched_item"]}>
                        <span onClick={()=>updateComicsInfoWithItem(item._id)}>{item.name.korean}</span>
                        {idx!==size-1 && (<span>, </span>)}
                    </div>
                )
            case COMICS_INFO_TYPE.CONTENT_DESCRIPTOR:
                return (
                    <div key={idx} className={style["searched_item"]}>
                        <span onClick={()=>updateComicsInfoWithItem(item._id)}>{item.name.korean}</span>
                        {idx!==size-1 && (<span>, </span>)}
                    </div>
                )
        }
    }

    return (
        <div className={`${style["add_menu_panel"]} ${isVisible? style["visible"]: ""}`}>
            {!inputMode? (
                <>
                    <section>
                        <div className={style["close_button"]} onClick={()=>setIsVisible(false)}/>

                        {
                            infoType===COMICS_INFO_TYPE.GENRE? "추가할 장르 선택":
                            infoType===COMICS_INFO_TYPE.CONTENT_DESCRIPTOR? "추가할 태그 선택":
                                (
                                    <>
                                        <div className={style["search_bar"]}>
                                            <div className={style["icon"]}/>
                                            <input type="text" spellCheck={false} onInput={e => setTextToSearch(e.target.value)} onKeyDown={onTextToSearchFieldKeyDown}/>
                                        </div>

                                        <div className={style["search_button"]} onClick={findInfoItems}>검색하기</div>

                                        <div className={style["add_new"]} onClick={toAddNewItemMode}>
                                            <div className={style["icon"]}/>
                                            새로 등록
                                        </div>
                                    </>
                                )
                        }
                    </section>

                    <section className={style["search"]}>
                        {_.isEmpty(searchedItemList)? "검색 결과가 없습니다.": (
                            searchedItemList.map((item, idx) => {
                                return (
                                    <SearchedItem key={idx} idx={idx} item={item} size={searchedItemList.length}/>
                                )
                            })
                        )}
                    </section>
                </>
            ): (
                <section className={style["input"]}>
                    <div className={style["close_button"]} onClick={cancelAddNewMode}/>

                    <p>{
                        (infoType===COMICS_INFO_TYPE.WRITER)? "작가 이름(영문)":
                        (infoType===COMICS_INFO_TYPE.ARTIST)? "아티스트 이름(영문)":
                        (infoType===COMICS_INFO_TYPE.PUBLISHER)? "코믹스 퍼블리셔 이름(영문)": ""
                    }</p>

                    <div>
                        <input type="text" spellCheck={false} onInput={e => setTextToAdd(e.target.value)}/>
                        <span onClick={createNewInfoAndAdd}>등록하기</span>
                    </div>
                </section>
            )}
        </div>
    )
})


/**
 * ITEM ADD BUTTON
 * */
const AddButton = forwardRef(({}, ref) => {

    const [visible, setVisible] = useState(false);



    useImperativeHandle(ref, () => ({
        visible: () => {
            setVisible(true);
        },
        invisible: () => {
            setVisible(false);
        }
    }))


    return (
        <span className={`${style["add_button"]} ${visible? style["visible"]: ""}`}>
            <label className={style["icon"]}/>
            추가하기
        </span>
    )
})


const ComicsInfoItem = ({infoType}) => {
    const comicsState = useSelector(state => state.comics);
    const userState = useSelector(state => state.user);

    const {title, writers, artists, publisher, genres, contentDescriptors} = comicsState;


    const {loggedIn: isRegUser} = userState;
    
    const [isAddButtonVisible, setIsAddButtonVisible] = useState(false);


    const addMenuPanelRef = useRef(null);



    let list;
    let item;
    let info;
    let _style;

    switch (infoType) {
        case COMICS_INFO_TYPE.WRITER:
            list = writers;
            info = "작가";
            _style = style["writer"];
            break;
        case COMICS_INFO_TYPE.ARTIST:
            list = artists;
            info = "아티스트";
            _style = style["artist"];
            break;
        case COMICS_INFO_TYPE.PUBLISHER:
            item = publisher;
            info = "퍼블리셔";
            _style = style["publisher"];
            break;
        case COMICS_INFO_TYPE.GENRE:
            list = genres;
            info = "장르";
            _style = style["genre"];
            break;
        case COMICS_INFO_TYPE.CONTENT_DESCRIPTOR:
            list = contentDescriptors;
            info = "태그";
            _style = style["content_descriptor"];
            break;
    }



    const showAddMenuPanel = () => {
        if (!isRegUser) return;
        
        addMenuPanelRef.current.enable();
    }


    return (
        <div className={_style} onMouseOver={()=>setIsAddButtonVisible(true)} onMouseLeave={()=>setIsAddButtonVisible(false)}>
            {/** INFO TITLE */}
            <p>
                {info}
                {isRegUser && (
                    <span className={`${style["add_button"]} ${isAddButtonVisible? style["visible"]: ""}`} onClick={showAddMenuPanel} title={!isRegUser? "회원만이 할 수 있다.": ""}>
                    <label className={style["icon"]}/>
                    추가하기
                </span>
                )}
            </p>

            {/** INFO CONTENT */}
            <p>{
                (
                    infoType===COMICS_INFO_TYPE.ARTIST ||
                    infoType===COMICS_INFO_TYPE.WRITER
                )? (
                    _.isEmpty(list)? "-": (
                        list.map((item, idx) => {
                            return (
                                <span key={idx}>
                                    <span>{item.name.origin}</span>
                                    {idx!==list.length-1 && (<span>, </span>)}
                                </span>
                            )
                        })
                    )
                ):
                (
                    infoType===COMICS_INFO_TYPE.PUBLISHER
                )? (
                    _.isEmpty(item)? "-": item.name.origin
                ):
                (
                    infoType===COMICS_INFO_TYPE.GENRE
                )? (
                    _.isEmpty(list)? "-": (
                        list.map((item, idx) => {
                            console.log(list)
                            return (
                                <span key={idx}>
                                    <span>{item.name.korean}</span>
                                    {idx!==list.length-1 && (<span>, </span>)}
                                </span>
                            )
                        })
                    )
                ):
                    (
                        infoType===COMICS_INFO_TYPE.CONTENT_DESCRIPTOR
                    ) && (
                        _.isEmpty(list)? "-": (
                            list.map((item, idx) => {
                                return (
                                    <span key={idx}>
                                    <span>{item.name.korean}</span>
                                        {idx!==list.length-1 && (<span>, </span>)}
                                </span>
                                )
                            })
                        )
                    )
            }</p>

            <AddMenuPanel infoType={infoType} ref={addMenuPanelRef}/>
        </div>
    )
}



const ComicsSummary = () => {
    const router = useRouter();

    const userState = useSelector(state => state.user);
    const comicsState = useSelector(state => state.comics);

    const {comicsDataId, summary: comicsSummary} = comicsState;

    const [inputMode, setInputMode] = useState(false);

    const comicsSummaryTextareaRef = useRef(null);


    const isRegUser = userState.loggedIn;



    const autoResizeTextarea = () => {
        const textarea = comicsSummaryTextareaRef.current;
        textarea.style.height = "auto";

        const scrollHeight = textarea.scrollHeight;

        textarea.style.height = `${scrollHeight}px`;
    }


    const submit = async () => {
        const textarea = comicsSummaryTextareaRef.current;

        const {status} = await requestUpdateComicsSummary(comicsDataId, textarea.value);

        switch (status) {
            case 200:
            case 400:
            case 403:
                router.reload();
                break;
        }
    }


    return (
        <div className={`${style["comics_summary"]} ${!comicsSummary? style["no_border"]: style["updatable"]}`}>
            {
                (!inputMode && comicsSummary)
            && (
                <p>{comicsSummary}</p>
            )}


            {(isRegUser && !inputMode) && (
                <div className={style["update_summary_button"]} onClick={()=>setInputMode(true)}>
                    <div className={style["icon"]}/>
                    <span>{comicsSummary? "작품 설명 수정": "작품 설명 등록"}</span>
                </div>
            )}

            {(isRegUser && inputMode) && (
                <div className={style["input_summary"]}>
                    <textarea spellCheck={false} rows={3} defaultValue={comicsSummary} onInput={autoResizeTextarea} ref={comicsSummaryTextareaRef}/>
                    <span className={style["submit"]} onClick={submit}>작성완료</span>
                </div>
            )}
        </div>
    )
}



const ComicsPage = ({comicsId, comicsNumber}) => {
    const router = useRouter();

    const comicsState = useSelector(state => state.comics);
    const userState = useSelector(state => state.user);

    const {title, comicsDataId, coverImage} = comicsState;
    const {loggedIn: isRegUser} = userState;

    const coverImgInputRef = useRef(null);
    const coverImgInputMessageRef = useRef(null);


    const isCoverImage = !_.isEmpty(coverImage);





    const onCoverImgInput = () => {
        const coverImgInput = coverImgInputRef.current;

        if (coverImgInput.files.length!==1) return;

        const file = coverImgInput.files[0]
        const fileExt = file.type.split("/")[1];


        /**
         * Check file type
         * */
        if (![
            "image/jpg",
            "image/jpeg",
            "image/png"
        ].includes(file.type)) {
            coverImgInputMessageRef.current.open(StretchOpenMessageType.COMICS.COVER_IMAGE_INPUT_MESSAGE.INVALID_IMAGE_TYPE);
            return;
        }

        /**
         * Check file size
         * */
        if (file.size > 1.5e+7) {
            coverImgInputMessageRef.current.open(StretchOpenMessageType.COMICS.COVER_IMAGE_INPUT_MESSAGE.TOO_LARGE_FILE);
            return;
        }


        const reader = new FileReader();


        /**
         * File reader on-load
         * */
        reader.onload = async (e) => {
            const dataUrl = e.target.result;
            const loadedFile = dataURLtoFile(dataUrl, `cover.${fileExt}`)

            const {status} = await requestUpdateComicsCoverImage(comicsDataId, loadedFile);

            switch (status) {
                case 200:
                case 400:
                case 403:
                    router.reload();
                    break;
            }
        }

        /**
         * File reader on-error
         * */
        reader.onerror = (e) => {

        }

        /**
         * Read image file
         * */
        reader.readAsDataURL(file);


        /**
         * Clear input
         * */
        coverImgInput.value = "";
    }


    const coverImgUploadButtonClick = () => {
        const coverImgInput = coverImgInputRef.current;

        coverImgInput.click();
    }


    const toWriteComicsPage = async () => {
        await router.push({
            pathname: "/write/comics/book",
            query: {
                id: comicsDataId
            }
        })
    }

    console.log(comicsState)
    return (
        <>
            <DocHead title={`${title.korean}(${title.origin}) | 미만웹`}/>

            <LeftSidebar/>


            <main className={style["container"]}>

                {/** COMICS INFO */}
                <section className={style["comics_info"]}>

                    {/** BACKGROUND */}
                    {isCoverImage && (
                        <img className={style["background"]} src={parseImgId(coverImage.large)}/>
                    )}
                    <div className={style["background_overlay"]}/>

                    {/** FRONT */}
                    <div className={style["front"]}>

                        {/** PAGE TITLE */}
                        <section className={style["page_index"]}>
                            <a className={style["comics_page_title"]}>
                                <span>MIMANWEB</span>
                                <span>번역 코믹스</span>
                            </a>
                        </section>

                        {/** COMICS INFO */}
                        <section className={style["content"]}>
                            <div className={style["image"]}>
                                {isCoverImage && (
                                    <img src={parseImgId(coverImage.large)}/>
                                )}

                                {isRegUser && (
                                    <section className={`${style["add_image_message"]} ${isCoverImage? style["update_mode"]: ""}`}>
                                        <div className={style["button"]} onClick={coverImgUploadButtonClick}>
                                            <div className={style["icon"]}/>
                                            <span>{isCoverImage? "커버 이미지 바꾸기": "커버 이미지 등록"}</span>
                                            <input type="file" accept=".jpg,.jpeg,.png" multiple={false} onChange={onCoverImgInput} ref={coverImgInputRef}/>
                                        </div>

                                        <ComicsCoverImageInputMessage boardType={StretchOpenBoardType.COMICS.COVER_IMAGE_INPUT_MESSAGE} ref={coverImgInputMessageRef}/>
                                    </section>
                                )}
                            </div>

                            <div className={style["info"]}>

                                {/** COMICS TITLE */}
                                <div className={style["title"]}>
                                    <p>{title.korean}</p>
                                    <p>{title.origin}</p>
                                </div>

                                {/** COMICS WRITERS */}
                                <ComicsInfoItem infoType={COMICS_INFO_TYPE.WRITER}/>

                                {/** COMICS ARTISTS */}
                                <ComicsInfoItem infoType={COMICS_INFO_TYPE.ARTIST}/>

                                {/** COMICS PUBLISHER */}
                                <ComicsInfoItem infoType={COMICS_INFO_TYPE.PUBLISHER}/>

                                {/** COMICS GENRE */}
                                <ComicsInfoItem infoType={COMICS_INFO_TYPE.GENRE}/>

                                {/** COMICS CONTENT DESCRIPTOR */}
                                <ComicsInfoItem infoType={COMICS_INFO_TYPE.CONTENT_DESCRIPTOR}/>

                                {/** COMICS SUMMARY */}
                                <ComicsSummary/>

                            </div>
                        </section>

                        {/** REGISTER TRANSLATED WORK */}
                        <section className={style["register_book"]}>
                            {isRegUser? (
                                <div onClick={toWriteComicsPage}>
                                    <div className={style["icon"]}/>
                                    <p>번역 볼륨/이슈 등록</p>
                                </div>
                            ): (
                                <div>
                                    <div className={style["icon"]}/>
                                    <p>번역 볼륨/이슈 등록(회원가입 필요)</p>
                                </div>
                            )}
                        </section>
                    </div>
                </section>


                {/** COMICS EDITIONS */}
                <section className={style["comics_editions"]}>
                    <ComicsEditionList/>
                </section>

            </main>


            <RightSidebar/>
        </>
    )
}



export default ComicsPage;