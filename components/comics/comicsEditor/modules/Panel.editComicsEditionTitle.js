import React from "react";
import style from "./Panel.editComicsEditionTitle.module.scss";
import OptionToggleTable from "../optionToggleTable";
import {setComicsEditionTitleState, setIsComicsEditionTitleState, requestUpdateComicBookInfo} from "../../../../features/comicsBook/actions";
import {useDispatch, useSelector} from "react-redux";



const ComicsEditionTitleEditPanel = () => {
    const dispatch = useDispatch();

    const comicsBookEditorState = useSelector(state => state.comicBook);

    const {comicBookDataId, isEditionTitle, editionTitle} = comicsBookEditorState;

    const options = {
        exist: {
            text: "있음",
            callback: (async ()=>{
                dispatch(setIsComicsEditionTitleState(true));

                await requestUpdateComicBookInfo({comicBookDataId: comicBookDataId, isTitle: true})
            })
        },
        none: {
            text: "없음",
            callback: (async ()=>{
                dispatch(setIsComicsEditionTitleState(false));

                await requestUpdateComicBookInfo({comicBookDataId: comicBookDataId, isTitle: false})
            })
        }
    }


    const onTitleInput = async (e) => {
        const inputVal = e.target.value;

        dispatch(setComicsEditionTitleState(inputVal));

        await requestUpdateComicBookInfo({comicBookDataId: comicBookDataId, title: inputVal})
    }


    return (
        <div className={`${style["container"]} ${!isEditionTitle? style["disabled"]: ""}`}>
            <div>
                <span>볼륨/이슈 부제목</span>
                <OptionToggleTable options={options} defOption={isEditionTitle? options.exist: options.none}/>
            </div>

            {isEditionTitle && (
                <input type="text" spellCheck={false} disabled={!isEditionTitle} value={editionTitle} onInput={onTitleInput}/>
            )}
        </div>
    )
}



export default ComicsEditionTitleEditPanel;