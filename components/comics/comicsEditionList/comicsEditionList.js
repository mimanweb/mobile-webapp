import React from "react";
import style from "./comicsEditionList.module.scss";
import {useSelector} from "react-redux";
import _ from "underscore";
import {COMIC_BOOK_CONTENT_TYPE, COMIC_BOOK_IMAGE_STATUS} from "../../../utils/index/comics/book";
import {parseImgId} from "../../../utils/image/parse";
import Link from "next/link";




const Book = ({imgUrl, translator, editionTitle, volume, issue, shortStoryNumber, isEditionTitle, isShortStory, isVolume}) => {
    const comicsState = useSelector(state => state.comics);
    const {title, comicsNumber, comicsId} = comicsState;


    const comicBookPagePathOption = {
        pathname: `/comics/book/${comicsNumber}/${comicsId}`,
        query: {
            translator: translator
        }
    }

    if (isShortStory) comicBookPagePathOption.query["short-story"] = shortStoryNumber;
    if (!isShortStory && isVolume) comicBookPagePathOption.query.vol = volume;
    if (!isShortStory) comicBookPagePathOption.query.issue = issue;



    return (
        <div className={style["book"]}>
            <Link href={comicBookPagePathOption}>
                <a className={style["cover_img"]}>
                    {imgUrl? (
                        <img src={parseImgId(imgUrl)}/>
                    ): (
                        <div className={style["no_thumbnail"]}>
                            <p>{title.korean}</p>
                            <p className={style["origin_title"]}>{title.origin}</p>
                            {editionTitle && (
                                <p>{editionTitle}</p>
                            )}
                        </div>
                    )}
                </a>
            </Link>

            <div className={style["episode"]}>
                {isShortStory && (
                    <Link href={comicBookPagePathOption}>
                        <a>단편</a>
                    </Link>
                )}

                {(!isShortStory && isVolume) && (
                    <Link href={comicBookPagePathOption}>
                        <a>볼륨 {volume}</a>
                    </Link>
                )}

                {!isShortStory && (
                    <Link href={comicBookPagePathOption}>
                        <a>이슈 {issue}</a>
                    </Link>
                )}

                {isEditionTitle && (
                    <Link href={comicBookPagePathOption}>
                        <a>:{editionTitle}</a>
                    </Link>
                )}
            </div>
        </div>
    )
}


const BookList = ({translator, bookList}) => {
    return (
        <div className={style["book_list"]}>

            {/** TRANSLATOR INFO */}
            <section className={style["translator"]}>
                <p>{translator}</p>
                <p>번역, 총 {bookList.length}편</p>
            </section>

            {/** BOOK LIST */}
            <section className={style["list"]}>
                {bookList.map((book, key) => {
                    const {contentType, isShortStory, isTitle, isVolume, volume, issue, title: editionTitle, shortStoryNumber} = book.index;

                    let thumbnail;

                    switch (contentType) {
                        case COMIC_BOOK_CONTENT_TYPE.LINK:
                            const linkThumbnailInfo = book.linkThumbnails.find(img => img.status===COMIC_BOOK_IMAGE_STATUS.PUBLISH);

                            thumbnail = linkThumbnailInfo? linkThumbnailInfo.imgId.large: null;
                            break;
                        case COMIC_BOOK_CONTENT_TYPE.IMAGE:
                            thumbnail = book.pages.find(book => book.pageNumber===1).imgId.large;
                            break;
                    }

                    return (
                        <Book key={key}
                              imgUrl={thumbnail}
                              translator={book.owner.userId}
                              isEditionTitle={isTitle}
                              isShortStory={isShortStory}
                              isVolume={isVolume}
                              volume={volume}
                              issue={issue}
                              shortStoryNumber={shortStoryNumber}
                              editionTitle={book.title}
                        />
                    )
                })}
            </section>
        </div>
    )
}


const ComicsEditionList = () => {
    const comicsState = useSelector(state => state.comics);

    const {editions, publishedEditions, publishedEditionsGroupByTranslator} = comicsState;

    const isEditions = !_.isEmpty(editions);
    const isPublishedEditions = !_.isEmpty(publishedEditions);


    return (
        <div className={style["container"]}>

            {/** EDITIONS COUNT SECTION */}
            <section className={style["editions_count_message"]}>
                <p>{isPublishedEditions? `번역본 총 ${publishedEditions.length}편 등록됨`: "아직 번역된 볼륨/이슈가 없습니다."}</p>
            </section>


            {/** LIST SECTION */}
            {isPublishedEditions && (
                <section className={style["book_list"]}>
                    {Object.entries(publishedEditionsGroupByTranslator).map(([translator, bookList], key) => {

                        return <BookList key={key} translator={translator} bookList={bookList}/>
                    })}
                </section>
            )}

        </div>
    )
}



export default ComicsEditionList;