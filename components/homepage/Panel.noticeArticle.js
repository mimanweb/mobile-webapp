import style from "./Panel.noticeArticle.module.scss";
import React from "react";
import Link from "next/link";
import time from "../../utils/time";
import {SiteIndex} from "../../utils/index/siteIndex";


export default function HomepageNoticeArticle({postData}) {


    const likePath = {
        pathname: `/post/${SiteIndex.Community.code}/${SiteIndex.Community.boards.Notice.code}/${postData.number}`,
        query: {
            list: "notice"
        }
    }

    return (

        <article className={style["container"]}>
            <Link href={likePath}>
                <a>
                    <div className={style["content"]}>
                        <label>공지</label>

                        <div className={style["title"]} title={postData.title}>
                            {postData.title}
                        </div>

                        <div className={style["time"]}>{time.shortFormatText(postData.time.published)}</div>
                    </div>
                </a>
            </Link>
        </article>

    )
}