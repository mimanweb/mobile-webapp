import React from "react";
import style from "./Panel.articleList.module.scss";
import Link from "next/link";
import {SiteIndex} from "../../utils/index/siteIndex";
import _ from "underscore";



const ARTICLE_HEIGHT = 27.2;


export default function HomepageArticleListPanel({postList=[], max, showBoardTag, setMinHeight}) {

    if (max && postList.length>max) {
        postList = postList.slice(0, max);

    }


    const _style = {minHeight: `${ARTICLE_HEIGHT*max}px`}


    return (
        <div className={style["post_list"]} style={setMinHeight? _style: null}>
            {_.isEmpty(postList) && (
                <div className={style["empty"]}>
                    게시글이 없습니다.
                </div>
            )}

            {postList.map((post, key) => {
                const {index, record, title, number} = post;

                const linkPath = `/post/${index.category}/${index.board}/${number}`

                return (
                    <article key={key}>
                        {showBoardTag && (()=>{
                            const categoryIndex = Object.values(SiteIndex).find(category => category.code===index.category);
                            const boardIndex = Object.values(categoryIndex.boards).find(board => board.code===index.board);

                            return (
                                <Link href={linkPath}>
                                    <a className={style["category_icon"]}>
                                        {boardIndex.tagName}
                                    </a>
                                </Link>
                            )
                        })()}

                        <div className={`${style["content"]} ${showBoardTag? style["width_type1"]: style["width_type2"]}`}>
                            <Link href={linkPath}>
                                <a className={style["post_title"]} title={title}>{title}</a>
                            </Link>
                            {record.commentCount>0 && (
                                <Link href={linkPath}>
                                    <a className={style["comment_count"]}>{record.commentCount>=100? "99+": record.commentCount}</a>
                                </Link>
                            )}
                        </div>
                    </article>
                )
            })}
        </div>
    )
}