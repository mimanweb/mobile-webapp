import React, {useState} from "react";
import style from "./siteMenuNav.module.scss";
import {SiteIndex} from "../../utils/index/siteIndex";
import {map} from "underscore";
import Link from "next/link";


/**
 * Website sitemap menu navigator
 * Horizontally scrollable
 * Toggle stretch open category sub menus
 * */




const Tab = ({info, isOpened, isFiller}) => {
    const {name, boards} = info;

    return (
        <div className={`${style["tab"]} ${isOpened? "": style["closed"]} ${isFiller? style["blank"]: ""}`}>
            <section className={style["category"]}>
                <div className={style["item"]}>{name}</div>
            </section>

            <section className={style["board"]}>
                {
                    !isFiller && map(boards, (board, key) => {
                        if (board.showOnSidebar) {
                            return (
                                <Link href={board.path}>
                                    <a key={key} className={style["item"]}>
                                        {board.name}
                                    </a>
                                </Link>
                            )
                        }
                    })
                }
            </section>
        </div>
    )
}



const SiteMenuNavigator = () => {
    const [isOpened, setIsOpened] = useState(false);
    let touchStartY;


    const ArrowToggle = () => {

        const setInitialTouchY = (e) => {
            touchStartY = e.touches[0].clientY;
        }

        const swipeTab = (e) => {
            const swipingDown = e.touches[0].clientY>touchStartY;

            if (!isOpened && touchStartY && swipingDown) {
                setIsOpened(true);
            }
            else if (isOpened && !swipingDown) {
                setIsOpened(false);
            }

            touchStartY = null;
            e.stopPropagation();
        }

        const toggleTab = (e) => {
            setIsOpened(!isOpened);

            touchStartY = null;
            e.stopPropagation();
        }

        return (
            <div className={`${style["arrow"]}`} onTouchStart={setInitialTouchY} onTouchMove={swipeTab} onMouseUp={toggleTab}>
                <div/>
            </div>
        )
    }


    return (
        <div className={style["container"]}>
            <section className={style["category_board_tab"]}>
                {map(SiteIndex, (index, key) => {
                    return (
                        <Tab key={key} info={index} isOpened={isOpened}/>
                    )
                })}

                <Tab info={{}} isOpened={isOpened} isFiller={true}/>
            </section>

            <section className={style["toggle_button"]}>
                <ArrowToggle/>
                <ArrowToggle/>
                <ArrowToggle/>
            </section>
        </div>
    )
}


export default SiteMenuNavigator;