import React, {useRef, useEffect} from "react";
import {useSelector,useDispatch} from "react-redux";
import {useRouter} from "next/router";
import {
    StretchOpenMessageBoard as ErrorMessageBoard,
    StretchOpenMessageBoard as InvalidPostParamMessage,
    StretchOpenBoardType,
    StretchOpenMessageType} from "../../components/panel/index";
import {requestPublishPost, setPostOwnerInfo, requestUpdatePost} from "../../features/post/actions";
import {
    checkEditorContentElTypes,
    findUniqueElFromLine,
    checkBlankText
} from "../../utils/editor/commonUtils";
import POST_ from "../../utils/index/post";



/** STYLE */
import style from "./writer.module.scss"


/** COMPONENTS */
import Editor from "../editor/editor";
import BoardInfo from "./sections/article/Panel.board-info"
import TitleInput from "./sections/article/Panel.title.input";
import UpvoteSignatureInput from "./sections/article/Area.upvote_signature.input";
import UnregUserInfoInput from "./sections/article/Panel.unregUserInfo.input";
import {Ad_300x250} from "../ad";
import SmallButton, {SmallButtonType} from "../button/small";
import {cancelUpdatingPostAfterDelay} from "../../utils/editor/delayExecutor";
import {USER_TYPE} from "../../utils/index/user";


/**
 * Post writing module(panel)
 * */
const PostWriter = ({hideTitle, hideSubmitButtons}) => {
    const dispatch = useDispatch();
    const router = useRouter();

    const userState = useSelector(state => state.user);
    const postState = useSelector(state => state.post);
    const editorState = useSelector(state => state.editor);

    const {editMode, number: postNumber, postId, userId, nickname, password, board, category, title, content, phrase} = postState;
    const {displayUnregUserInfo} = editorState;


    const isRegUser = userState.loggedIn;
    const isRegUserPost = userId;
    const isEditMode = editMode===POST_.EDIT_MODE.EDIT;
    const isAdminUser = isRegUser && userState.type===USER_TYPE.ADMIN;

    const editorRef = useRef(null);
    const tempSaveResultMessageRef = useRef(null);
    const invalidPostParamMessageRef = useRef(null);



    const onTempSaveButtonClick = async () => {
        const tempSaveResult = await editorRef.current.updateTempPostData(0);

        if (tempSaveResult) tempSaveResultMessageRef.current.open(StretchOpenMessageType.POST_TEMP_SAVE_RESULT.TEMP_SAVE_SUCCESS);
        else tempSaveResultMessageRef.current.open(StretchOpenMessageType.POST_TEMP_SAVE_RESULT.TEMP_SAVE_FAILED)
    }

    const onCancelButtonClick = async () => {
        if (editMode===POST_.EDIT_MODE.WRITING) {
            await router.push(`/board/${category}/${board}`);
        }
        else if (editMode===POST_.EDIT_MODE.EDIT) {
            await router.push(`/post/${category}/${board}/${postNumber}`);
        }
    }

    const onPostUploadButtonClick = async (type) => {

        if (!title || checkBlankText(title)) {
            invalidPostParamMessageRef.current.open(StretchOpenMessageType.POST.INVALID_PARAM.NO_TITLE);
            return;
        }
        else if (!nickname || checkBlankText(nickname)) {
            invalidPostParamMessageRef.current.open(StretchOpenMessageType.POST.INVALID_PARAM.NO_NICKNAME);
            return;
        }
        else if (!isEditMode && !password) {
            invalidPostParamMessageRef.current.open(StretchOpenMessageType.POST.INVALID_PARAM.NO_PASSWORD);
            return;
        }
        else if (!isEditMode && password && password.length<4) {
            invalidPostParamMessageRef.current.open(StretchOpenMessageType.POST.INVALID_PARAM.TOO_SHORT_PASSWORD);
            return;
        }
        else if (!content) {
            invalidPostParamMessageRef.current.open(StretchOpenMessageType.POST.INVALID_PARAM.EMPTY_CONTENT);
            return;
        }


        const div = document.createElement("div");
        div.innerHTML = content;
        const els = [...div.childNodes.values()];

        const hasUniqueEl = els.some(el => findUniqueElFromLine(el));
        const hasOnlyBlankText = els.every(el => !el.textContent || checkBlankText(el.textContent));

        if (!hasUniqueEl && hasOnlyBlankText) {
            invalidPostParamMessageRef.current.open(StretchOpenMessageType.POST.INVALID_PARAM.EMPTY_CONTENT);
            return;
        }


        if (!checkEditorContentElTypes(div.childNodes)) {
            invalidPostParamMessageRef.current.open(StretchOpenMessageType.POST.INVALID_PARAM.CONTENT_INVALID_EL_INCLUDED);
            return;
        }



        switch (editMode) {
            case POST_.EDIT_MODE.WRITING:
                const publishRes = await requestPublishPost(postId, category, board, nickname, password, title, content, phrase, type);

                switch (publishRes.status) {
                    case 200:
                        await cancelUpdatingPostAfterDelay();
                        await router.push(`/post/${category}/${board}/${publishRes.data.postNumber}`);
                        break;
                    case 404:
                        alert("게시글이 존재하지 않습니다.");
                        await router.push(`/board/${category}/${board}`);
                        break;
                }
                break;
            case POST_.EDIT_MODE.EDIT:
                const postPassword = router.query.password;
                const updateRes = await requestUpdatePost(postId, postPassword, category, board, title, content, phrase);

                switch (updateRes.status) {
                    case 200:
                        await router.push(`/post/${category}/${board}/${postNumber}`);
                        break;
                    case 400:
                        invalidPostParamMessageRef.current.open(StretchOpenMessageType.POST.INVALID_PARAM.CONTENT_INVALID_EL_INCLUDED);
                        break;
                    case 404:
                        alert("게시글이 존재하지 않습니다.");
                        await router.push(`/board/${category}/${board}`);
                        break;
                }
                break;
        }

    }




    return (
        <div className={style["container"]}>


            {/** BOARD INFO */}
            <section className={style["first"]}>
                <BoardInfo/>
            </section>


            {/** POST TITLE */}
            {!hideTitle && (
                <section className={`${style["second"]} ${isRegUser? style["tall"]: ""}`}>
                    <TitleInput/>
                </section>
            )}


            {/** UNREG USER INFO */}
            {(
                ((!isEditMode && !isRegUser) || (isEditMode && !isRegUserPost)) && displayUnregUserInfo

            )
            && (
                <section className={style["third"]}>
                    <UnregUserInfoInput/>
                </section>
            )}


            {/** EDITOR */}
            <section className={style["forth"]}>
                <Editor ref={editorRef}/>
            </section>


            {/** AD, UPVOTE, PHRASE */}
            <section className={style["fifth"]}>
                <Ad_300x250/>
                <UpvoteSignatureInput/>
            </section>


            {/** UPLOAD, TEMP-SAVE BUTTON */}
            {!hideSubmitButtons && (
                <section className={style["sixth"]}>

                    {!isEditMode
                    && (
                        <SmallButton buttonType={SmallButtonType.POST_TEMP_SAVE} onClick={onTempSaveButtonClick}/>
                    )}

                    <SmallButton buttonType={SmallButtonType.POST_CANCEL} onClick={onCancelButtonClick}/>

                    {(
                        !isEditMode &&
                        isAdminUser
                    )
                    && (
                        <SmallButton buttonType={SmallButtonType.UPLOAD_NOTICE_POST} onClick={()=>onPostUploadButtonClick(POST_.TYPE.NOTICE)}/>
                    )}

                    <SmallButton buttonType={SmallButtonType.POST_UPLOAD} onClick={()=>onPostUploadButtonClick()}/>
                </section>
            )}


            {/** TEMP SAVE MESSAGE */}
            <section className={style["seventh"]}>
                <ErrorMessageBoard boardType={StretchOpenBoardType.POST_TEMP_SAVE_RESULT} ref={tempSaveResultMessageRef}/>
                <InvalidPostParamMessage boardType={StretchOpenBoardType.POST_PARAM_CHECK} ref={invalidPostParamMessageRef}/>
            </section>
        </div>
    )
}


export default PostWriter;