import React, {useState, useRef, useEffect, forwardRef, useImperativeHandle} from "react";
import {useSelector, useDispatch} from "react-redux";
import style from "./comment.module.scss";
import {
    setCommentNumberToWrite,
    requestCommentsInSpecificPage,
    setCommentState,
    requestUpdateCommentUpvote,
    setPageCommentsState,
    setBestCommentsState,
    setCommentNumberToDelete,
    requestDeleteComment
} from "../../features/comment/actions";
import {COMMENT_TYPE, COMMENT_PAGE_PER_INDEX} from "../../utils/index/comment";
import _ from "underscore";
import timeUtil from "../../utils/time";
import {StretchOpenMessageBoard, StretchOpenBoardType} from "../../components/panel/index";
import {useRouter} from "next/router";
import HorizontalTextPageNavigator from "../pageNavigator/pageNavigator";



/**
 * Sections
 * */
import Panel_CommentWrite from "./sections/comment/Panel.comment-write";
import time from "../../utils/time";



const toggleUpvote = async (postNumber, commentNumber, pageComments, bestComments, dispatch) => {
    const {status, data} = await requestUpdateCommentUpvote(postNumber, commentNumber);


    switch (status) {
        case 200:
            const {updatedUpvoteCount, isUpvoted} = data;

            const upvoteAppliedPageComments = pageComments.map(comment => {
                if (comment.number===commentNumber) {
                    comment.record.upvoteCount = updatedUpvoteCount;
                    comment.isUpvoted = isUpvoted;
                }

                return comment;
            })

            dispatch(setPageCommentsState(upvoteAppliedPageComments));

            const upvoteAppliedBestComments = bestComments.map(comment => {
                if (comment.number===commentNumber) {
                    comment.record.upvoteCount = updatedUpvoteCount;
                    comment.isUpvoted = isUpvoted;
                }

                return comment;
            })

            dispatch(setBestCommentsState(upvoteAppliedBestComments));
            break;
    }
}




/**
 * Comment Options
 * Report/Delete buttons, Created time
 * */
const CommentOptions = ({commentNumber, commentUserId, isBestSection, upvoteCount, createdTime, deletedTime, isMyComment, isUpvoted, isDeleted}) => {
    const dispatch = useDispatch();
    const router = useRouter();

    const userState = useSelector(state => state.user);
    const postState = useSelector(state => state.post);
    const commentState = useSelector(state => state.comment);

    const {number: postNumber} = postState;
    const {pageComments, bestComments, commentNumberToDelete} = commentState;

    const userId = userState.id;
    const isRegUser = userState.loggedIn;
    const isRegUserComment = commentUserId;




    const onDeleteClick = async () => {
        /**
         * Delete directly if a reg-user comment
         * */
        if (isRegUserComment) {
            const {status, data: commentData} = await requestDeleteComment(postNumber, commentNumber);

            switch (status) {
                case 200:
                    dispatch(setCommentState(commentData))
                    break;
                default:
                    router.reload();
                    break;
            }

        } else {
            if (commentNumber===commentNumberToDelete) {
                dispatch(setCommentNumberToDelete(null));
            } else {
                dispatch(setCommentNumberToDelete(commentNumber));
            }
        }
    }


    return (
        <div className={style["options"]}>
            <section className={style["second"]}>
                {/** Comment Report Button */}
                {(
                    !isDeleted &&
                    !isMyComment
                )
                && (
                    <div className={style["report"]} title="댓글신고">
                        신고
                    </div>
                )}

                {/** Comment Delete Button*/}
                {(
                    !isDeleted &&
                    !isBestSection &&
                    !(isRegUserComment && !isRegUser) &&
                    !(isRegUserComment && isRegUser && commentUserId!==userId)
                )
                && (
                    <div className={`${style["delete"]} ${(isRegUserComment && isMyComment)? style["indicate"]: ""}`} title="댓글삭제" onClick={onDeleteClick}>
                        삭제
                    </div>
                )}

                {/** Comment Created Time */}
                <div className={style["time"]} title={`등록시각: ${timeUtil.fullFormatText(createdTime)}${isDeleted? "\n삭제시각: "+timeUtil.fullFormatText(deletedTime): ""}`}>
                    {timeUtil.shortFormatText(createdTime)}
                </div>
            </section>
        </div>
    )
}


/**
 * Comment Reply/Upvote buttons
 * */
const CommentReplyUpvote = ({commentNumber, isMyComment, isDeleted}) => {
    const dispatch = useDispatch();

    const postState = useSelector(state => state.post);
    const commentState = useSelector(state => state.comment);

    const {number: postNumber} = postState;
    const {pageComments, bestComments, commentNumberToWrite} = commentState;


    const onReplyButtonClick = () => {
        dispatch(setCommentNumberToWrite(commentNumber===commentNumberToWrite? null: commentNumber));
    }

    const onUpvoteClick = async () => {
        if (isDeleted) return;
        if (isMyComment) return;

        await toggleUpvote(postNumber, commentNumber, pageComments, bestComments, dispatch);
    }


    return (
        <span className={style["tail"]}>
            <span className={style["reply"]} onClick={onReplyButtonClick}>
                <label className={style["icon"]}/>
                <label>답글</label>
            </span>

            {!isMyComment && (
                <span className={style["upvote"]} onClick={onUpvoteClick}>
                <label className={style["icon"]}/>
                <label>추천</label>
            </span>
            )}
        </span>
    )
}


/**
 * Comment
 * */
const Comment = ({commentData, isBestSection, isReplyComment}) => {
    const dispatch = useDispatch();
    const commentState = useSelector(state => state.comment);
    const postState = useSelector(state => state.post);

    const {commentNumberToWrite, commentNumberToDelete, pageComments, bestComments, commentToScroll} = commentState;


    const {number: postNumber} = postState;

    const {number: commentNumber, owner, type, content, record, time, isBestComment, repliedComment, isMyComment, isUpvoted, isDeleted} = commentData;
    const {userId: commentUserId, nickname, ipAddress} = owner;
    const {text: commentText, youtubeHTML, imageURL} = content;
    const {upvoteCount} = record;

    const isRegUserPost = postState.userId;
    const postOwnerUserId = postState.userId;
    const isPostOwnerComment = isRegUserPost && (postOwnerUserId===commentUserId);
    const isRegUserComment = commentUserId;
    const isHotUpvoteComment = upvoteCount>=3;


    const passwordCheckingBoardRef = useRef(null);
    const commentArticleRef = useRef(null);


    const onUpvoteClick = async () => {
        if (isMyComment) return;

        await toggleUpvote(postNumber, commentNumber, pageComments, bestComments, dispatch);
    }



    useEffect(() => {

        if (!isRegUserComment && !isBestSection) {

            /**
             * Opening up comment-password checking boardPage
             * And scroll to the screen center
             * */
            if (commentNumber===commentNumberToDelete) {
                passwordCheckingBoardRef.current.open();

                const articleOffsetTop = commentArticleRef.current.offsetTop;
                const articleOffsetHeight = commentArticleRef.current.offsetHeight

                window.scroll({
                    top: articleOffsetTop + articleOffsetHeight - window.screen.height/2 + 60,
                    behavior: 'smooth'
                });
            } else {
                passwordCheckingBoardRef.current.close();
            }
        }

    }, [commentNumberToDelete])



    useEffect(() => {
        /**
         * Comment number to scroll given
         * */
        if (commentToScroll && commentToScroll===commentNumber) {
            const articleOffsetTop = commentArticleRef.current.offsetTop;
            const articleOffsetHeight = commentArticleRef.current.offsetHeight

            window.scroll({
                top: articleOffsetTop + articleOffsetHeight - window.screen.height/2 + 60,
            });
        }
    }, [])


    return (
        <article className={`${style["comment"]} ${isReplyComment? style["reply_comment"]: ""} ${isBestComment? style["best"]: ""} ${isDeleted? style["deleted"]: ""} ${commentToScroll===commentNumber? style["outline"]: ""}`} ref={commentArticleRef}>
            {commentToScroll===commentNumber && (
                <>
                    <div className={style["focused_outline_top"]}/>
                    <div className={style["focused_outline_bottom"]}/>
                    <div className={style["focused_outline_right"]}/>
                    <div className={style["focused_outline_left"]}/>
                </>
            )}

            {/** COMMENT CONTENTS */}
            <section className={style["top"]}>


                {/** FIRST BLOCK */}
                <section className={style["first"]}>
                    {
                        isReplyComment
                    && (
                        <div className={style["reply_comment_icon"]}/>
                    )}
                    <label className={`${style["nickname"]} ${isPostOwnerComment? style["post-owner"]: ""}`} title={isRegUserComment? commentUserId: null}>{isDeleted? "삭제된 댓글": nickname}</label>
                </section>


                {/** SECOND BLOCK */}
                <section className={style["second"]}>

                    {/** BEST ICON/UPVOTE COUNT ICON */}
                    {(isBestComment || upvoteCount>0) && (
                        <section className={`${style["best_icon_upvote_count_icon"]} ${isDeleted? style["blank"]: ""}`}>
                            {(isBestComment || isBestSection)
                            && (
                                <div className={style["best-icon"]}>
                                    <span>BEST</span>
                                </div>
                            )}

                            {(upvoteCount>0)
                            && (
                                <div className={`${style["upvote_count_icon"]} ${isUpvoted? style["fill_icon"]: ""} ${isHotUpvoteComment? style["fill"]: ""}`} onClick={onUpvoteClick}>
                                    <div className={style["icon"]}/>
                                    <span>{upvoteCount}</span>
                                </div>
                            )}
                        </section>
                    )}



                    {/** REPLIED COMMENT NICKNAME */}
                    {(
                        !isDeleted &&
                        repliedComment
                    )
                    && (<div className={style["replied_nickname"]}>{repliedComment.owner.nickname}</div>)}


                    {/** COMMENT MAIN CONTENT */}
                    <div className={style["content"]}>

                        {/** REPLY ICON */}
                        {/*{isReplyComment && (*/}
                        {/*    <div className={style["reply_comment_icon"]}/>*/}
                        {/*)}*/}

                        {/** CONTENT SECTION */}
                        <div className={`${style["comment"]} ${isDeleted? style["deleted"]: ""}`}>
                            { youtubeHTML && (
                                <div className={style["media"]} dangerouslySetInnerHTML={{__html: youtubeHTML}}/>
                            )}

                            { imageURL && (
                                <div className={style["media"]}>
                                    <img src={imageURL}/>
                                </div>
                            )}

                            <div className={style["text"]}>
                                {isDeleted? '': commentText}

                                {
                                    (!isBestSection && !isDeleted)
                                    && (<CommentReplyUpvote commentNumber={commentNumber} isMyComment={isMyComment} isDeleted={isDeleted}/>)
                                }
                            </div>
                        </div>
                    </div>
                </section>


                {/** THIRD BLOCK */}
                <section className={style["third"]}>
                    <CommentOptions
                        commentNumber={commentNumber}
                        isBestSection={isBestSection}
                        commentUserId={commentUserId}
                        upvoteCount={upvoteCount}
                        createdTime={time.created}
                        deletedTime={time.deleted}
                        isMyComment={isMyComment}
                        isUpvoted={isUpvoted}
                        isDeleted={isDeleted}/>
                </section>
            </section>

            {/** COMMENT WRITING */}
            {(!isBestSection && (commentNumber===commentNumberToWrite))
            && (
                <section className={`${style["bottom"]} ${isReplyComment? style["reply_comment"]: ""}`}>
                    <Panel_CommentWrite inCommentSection={true} inReplyCommentSection={isReplyComment}/>
                </section>
            )}

            {/** VERIFY PASSWORD BEFORE DELETE (FOR UNREG-USER COMMENT) */}
            {!isBestSection && (
                <section>
                    <StretchOpenMessageBoard boardType={StretchOpenBoardType.COMMENT.VERIFY_OWNER_BEFORE_DELETE_COMMENT} ref={passwordCheckingBoardRef}/>
                </section>
            )}

        </article>
    )
}



const CommentPanel = forwardRef(({}, ref) => {
    const dispatch = useDispatch();

    const postState = useSelector(state => state.post);
    const commentState = useSelector(state => state.comment);

    const {number: postNumber} = postState;
    const {pageComments, bestComments, itemsPerPageCount: commentPerPageCount, totalPageCount: totalCommentPageCount, searchedPage: currCommentPage, commentToScroll} = commentState;

    const currCommentPageIndex = Math.ceil(currCommentPage/COMMENT_PAGE_PER_INDEX);

    const pageStartOfCurrCommentPageIndex = (currCommentPageIndex-1)*COMMENT_PAGE_PER_INDEX;
    const pageEndOfCurrCommentPageIndex = (currCommentPageIndex)*COMMENT_PAGE_PER_INDEX;




    const setCommentPage = async (pageToSearch) => {
        const commentData = await requestCommentsInSpecificPage(postNumber, pageToSearch);

        dispatch(setCommentState(commentData.data))

        dispatch(setCommentNumberToWrite(null));
    }



    return (
        <div className={style["container"]}>


            {/** BEST COMMENTS SECTION */}
            {!_.isEmpty(bestComments) && (
                <>
                    <section className={style["best-comments"]}>
                        {bestComments.map((comment, key) => {

                            return <Comment key={key} isBest={true} isBestSection={true} commentData={comment}/>
                        })}
                    </section>

                    <div className={style["gap"]}/>
                </>
            )}


            {/** ALL COMMENTS SECTION */}
            <section className={style["all-comments"]}>
                {pageComments.map((comment, key) => {
                    const isReplyComment = comment.type===COMMENT_TYPE.REPLY_COMMENT;

                    return <Comment key={key} commentData={comment} isReplyComment={isReplyComment}/>
                })}
            </section>


            {/** NAVIGATION SECTION */}
            {!_.isEmpty(pageComments) && (
                <section>
                    <HorizontalTextPageNavigator
                        currPage={currCommentPage}
                        totalPageCount={totalCommentPageCount}
                        pagePerPageGroupCount={COMMENT_PAGE_PER_INDEX}
                        toFirstPageFunc={()=>setCommentPage(1)}
                        toPrevPageFunc={()=>setCommentPage(pageStartOfCurrCommentPageIndex)}
                        toPageFunc={(page)=>setCommentPage(page)}
                        toNextPageFunc={()=>setCommentPage(pageEndOfCurrCommentPageIndex+1)}/>
                </section>
            )}


            {/** COMMENT WRITING MODULE */}
            <section className={style["new_comment"]}>
                <Panel_CommentWrite/>
            </section>
        </div>
    )
})


export default CommentPanel;