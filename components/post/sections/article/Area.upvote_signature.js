import React from "react";
import {useSelector, useDispatch} from "react-redux";
import style from "./styles/Area.upvote_signature.module.scss";
import {requestUpdatePostUpvoteCount, updatePostUpvoteState} from "../../../../features/post/actions";



const UpvoteSignature = () => {
    const dispatch = useDispatch();

    const postState = useSelector(state => state.post);

    const {isMyPost, phrase, record, isUpvoted, number: postNumber, category, board} = postState;
    const {upvoteCount} = record;


    const onUpvoteIconClick = async () => {
        if (isMyPost) return;


        const res = await requestUpdatePostUpvoteCount(postNumber, category, board);

        const {status, data} = res;
        const {isIncreased, upvoteCount} = data;

        switch (status) {
            case 200:
                dispatch(updatePostUpvoteState(isIncreased, upvoteCount));
                break;
        }

    }


    return (
        <div className={style["container"]}>
            <div className={style["upvote"]}>
                <div className={`${style["icon_box"]} ${isMyPost? style["blank"]: ""}`} onClick={onUpvoteIconClick}>
                    <label className={style["count"]}>{upvoteCount}</label>
                    <div className={`${style["icon"]} ${!isUpvoted && !isMyPost? style["blank"]: style["bling"]}`} title={isMyPost? "본인 글": null}/>
                </div>

                <div className={style["phrase"]}>
                    <p>{phrase}</p>
                </div>
            </div>
        </div>
    )
}


export default UpvoteSignature;