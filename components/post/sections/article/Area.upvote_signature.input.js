import React, {useState, useRef, useEffect} from "react";
import {useSelector, useDispatch} from "react-redux";
import style from "./styles/Area.upvote_signature.module.scss";
import {requestUpdateTempPost, updatePostState, requestUpdatePostUpvoteCount} from "../../../../features/post/actions";
import {updatePostAfterDelay} from "../../../../utils/editor/delayExecutor";
import POST_ from "../../../../utils/index/post";



const UpvoteSignatureInput = () => {
    const dispatch = useDispatch();

    const postState = useSelector(state => state.post);
    const editorState = useSelector(state => state.post);

    const {routeOnAutoSaveFail} = editorState;
    const {editMode, record} = postState;
    const {upvoteCount} = record;
    const isEditMode = editMode===POST_.EDIT_MODE.EDIT;


    const textareaRef = useRef(null);



    const autoResize = () => {
        const textarea = textareaRef.current;
        textarea.style.height = "auto";

        const scrollHeight = textarea.scrollHeight;

        textarea.style.height = `${scrollHeight}px`;
    }


    const onKeyDown = (e) => {
        // Prevent input line break
        if (e.keyCode===13) e.preventDefault();
    }


    const onInput = async () => {
        autoResize();

        const phrase = textareaRef.current.value? textareaRef.current.value: "";


        dispatch(updatePostState("phrase", phrase));

        if (!isEditMode) await updatePostAfterDelay(postState, "phrase", phrase, undefined, routeOnAutoSaveFail);
    }


    useEffect(() => {
        autoResize();
    }, [textareaRef])


    useEffect(() => {
        textareaRef.current.value = postState.phrase;
    }, [postState.phrase])


    return (
        <div className={style["container"]}>
            <div className={style["upvote"]}>
                <div className={`${style["icon_box"]} ${style["blank"]}`}>
                    <label className={style["count"]}>{editMode===POST_.EDIT_MODE.EDIT? upvoteCount: 0}</label>
                    <div className={`${style["icon"]} ${editMode!==POST_.EDIT_MODE.EDIT? style["blank"]: ""}`}/>
                </div>
                <div className={style["phrase"]}>
                    <textarea rows={1} onKeyDown={onKeyDown} onInput={onInput} ref={textareaRef} spellCheck={false}/>
                </div>
            </div>
        </div>
    )
}


export default UpvoteSignatureInput;