import React, {useRef, useState, useImperativeHandle, forwardRef} from "react";
import style from "./styles/Panel.youtube-link-input.module.scss";


const YoutubeLinkInput = forwardRef(({apply}, ref) => {
    const textareaRef = useRef(null);


    const autoResize = () => {
        const textarea = textareaRef.current;
        textarea.style.height = "auto";

        const scrollHeight = textarea.scrollHeight;

        textarea.style.height = `${scrollHeight}px`;
    }

    const onInput = () => {
        autoResize();

        apply(textareaRef.current.value);
    }


    useImperativeHandle(ref, () => ({
        clearText: () => {
            textareaRef.current.value = "";
        }
    }))


    return (
        <div className={style["container"]}>
            <textarea placeholder="YouTube 링크 입력" spellCheck={false} onInput={onInput} ref={textareaRef}/>
        </div>
    )
})

export default YoutubeLinkInput;