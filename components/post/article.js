import React, {useRef} from "react";
import style from "./article.module.scss"
import {useSelector} from "react-redux";
import StretchOpenMessageBoard from "../panel/stretchOpenMessageBoard";
import {StretchOpenMessageType, StretchOpenBoardType} from "../panel";


/** SECTIONS */
import Area_metainfo from "./sections/article/Area.meta-info"
import Area_report_share_bookmark from "./sections/article/Area.report_share_bookmark"
import Area_upvote_signature from "./sections/article/Area.upvote_signature"
import Buttons_edit_delete from "./sections/article/Buttons.edit_delete"
import Panel_article from "./sections/article/Panel.article"
import Panel_boardinfo from "./sections/article/Panel.board-info"
import Panel_title from "./sections/article/Panel.title"


/** COMPONENTS */
import {Ad_300x250} from "../ad";
import DocHead from "../meta/head";




const Article = ({hideTitle, hideMetaInfo, hideEditMenus}) => {
    const userState = useSelector(state => state.user);
    const postState = useSelector(state => state.post);

    const {isMyPost, title: postTitle, textContent} = postState;
    const isRegUserPost = postState.userId;
    const isRegUser = userState.loggedIn;


    const postUrlCopiedMessageRef = useRef(null);



    const postUrlCopiedMessageFunc = () => {
        postUrlCopiedMessageRef.current.open(StretchOpenMessageType.POST.COPY_CURRENT_URL_TO_CLIPBOARD_COMPLETE);
    }



    return (
        <>
            <DocHead title={`${postTitle} | 미만웹`} desc={textContent}/>
            
            <div className={style["container"]}>


                {/** BOARD INFO */}
                <section className={style["first"]}>
                    <Panel_boardinfo/>
                </section>


                {/** TITLE */}
                {!hideTitle && (
                    <section className={style["second"]}>
                        <Panel_title/>
                    </section>
                )}


                {/** POST META INFO, owner, replies, views ... */}
                {!hideMetaInfo && (
                    <section className={style["third"]}>
                        <Area_metainfo/>
                    </section>
                )}


                {/** CONTENT */}
                <section className={style["forth"]}>
                    <Panel_article/>
                </section>


                {/** AD, UPVOTE, SIGNATURE */}
                <section className={style["fifth"]}>
                    <Ad_300x250/>
                    <Area_upvote_signature/>
                </section>


                {/** EDIT, DELETE */}
                {(
                    !hideEditMenus &&
                    ((isRegUser && isMyPost) || !isRegUserPost)
                )
                && (
                    <section className={style["sixth"]}>
                        <Buttons_edit_delete/>
                    </section>
                )}


                {/** META INFO, REPORT, SHARE, BOOKMARK */}
                {!hideMetaInfo && (
                    <section className={style["seventh"]}>
                        <section>
                            <Area_metainfo/>
                            <Area_report_share_bookmark postUrlCopiedMessageFunc={postUrlCopiedMessageFunc}/>
                        </section>

                        <section>
                            <StretchOpenMessageBoard ref={postUrlCopiedMessageRef} boardType={StretchOpenBoardType.POST.COPY_CURRENT_URL_TO_CLIPBOARD}/>
                        </section>
                    </section>
                )}


            </div>
        </>
    )
}


export default Article;