import React, {useRef, useState, useImperativeHandle, forwardRef} from "react";
import style from "./uniqueElControlMenu.module.scss";
import {findLineContainer, findUniqueElFromLine, findLinkElFromLine, checkSelectionWithinUniqueElLine, checkSelectionWithinLinkLine, setNewRange} from "../../../utils/editor/commonUtils";


const IMG_WIDTH_OPTIONS = [
    "80px", "160px", "320px", "640px", "1280px"
];
let IS_MOUSE_OVER_KEYPAD = false;
const CONTAINER_BORDER_WIDTH = 3;


const findMatchingWidthOpt = (options, imgWidth) => {
    return options.find((text, idx) => {
        const prevItem = idx===0? null: options[idx-1];
        const prevValue = prevItem? Number(prevItem.substr(0, prevItem.length-2)): 0;
        const currValue = Number(text.substr(0, text.length-2))

        return (prevValue<imgWidth && imgWidth<=currValue) || (idx===options.length-1 && imgWidth>currValue);
    })
}

const setKeypadMouseOver = (value) => {
    IS_MOUSE_OVER_KEYPAD = value;
}


const ArrowIcon = () => {
    return (
        <svg className={style["arrow"]} viewBox="0 0 10 10" width="100%" height="100%">
            <polygon points="5,0 10,10 0,10" style={{fill: "#262626"}}/>
        </svg>
    )
}

const ArrowKey = ({className, keyType, keyPress, onClick}) => {
    const _style = keyPress && keyPress===keyType? `${className} ${style["pressed"]}`: className;

    return (
        <kbd className={_style}
             onClick={onClick}
             onMouseOver={()=>setKeypadMouseOver(true)}
             onMouseLeave={()=>setKeypadMouseOver(false)}>
            <ArrowIcon/>
        </kbd>
    )
}

const SizeNumKey = ({idx, keyType, keyPress, currElType, currMaxWidth, naturalWidth, text, onClick}) => {
    let styleType;

    switch (currElType) {
        case "IMG":
            const maxWidth = IMG_WIDTH_OPTIONS[idx];
            const maxWidthPrevOpt = idx-1 >= 0? IMG_WIDTH_OPTIONS[idx-1]: null;

            if (maxWidth===currMaxWidth) styleType = "selected";
            else if (maxWidthPrevOpt && Number(maxWidthPrevOpt.substr(0, maxWidthPrevOpt.length-2))>=naturalWidth) styleType = "disabled";
            break;
        default:
            styleType = "disabled";
            break;
    }

    if (keyPress && keyType===keyPress && styleType!=="disabled") styleType = "pressed"

    return (
        <kbd
            className={(styleType || keyPress) && style[styleType]}
            onClick={styleType!=="disabled"? onClick: null}
            onMouseOver={()=>setKeypadMouseOver(true)}
            onMouseLeave={()=>setKeypadMouseOver(false)}>
            {text}
        </kbd>
    )
}



const UniqueElControlMenu = forwardRef(({editorSection, updatePostContentFunc}, ref) => {
    const [visible, setVisible] = useState(false);
    const [currImgNaturalWidth, setCurrImgNaturalWidth] = useState(undefined);
    const [currImgMaxWidth, setCurrImgMaxWidth] = useState(undefined);
    const [currElType, setCurrElType] = useState(undefined);
    const [offsetTop, setOffsetTop] = useState(0);
    const [offsetWidth, setOffsetWidth] = useState(0);
    const [offsetHeight, setOffsetHeight] = useState(0);
    const [keyBeingPressed, setKeyBeingPressed] = useState(null);




    const updateState = () =>{
        const selection = document.getSelection();
        const range = selection.getRangeAt(0);
        const currLine = findLineContainer(range.startContainer);
        const uniqueEl = findUniqueElFromLine(currLine) || findLinkElFromLine(currLine);


        /**
         * Set state,
         * Apply pre-render element's original offsetHeight to the offsetTop of current element
         * */
        setCurrElType(uniqueEl.nodeName)
        setOffsetTop(uniqueEl.offsetTop);
        setOffsetWidth(uniqueEl.offsetWidth);
        setOffsetHeight(uniqueEl.offsetHeight);

        /**
         * When the uniqueEl is IMG,
         * Calculate the width with max-width options and set state
         * */
        if (uniqueEl.nodeName==="IMG") {
            const currImgWidth = uniqueEl.offsetWidth;
            const currImgMaxWidthOpt = findMatchingWidthOpt(IMG_WIDTH_OPTIONS, currImgWidth)

            setCurrImgNaturalWidth(uniqueEl.naturalWidth);
            setCurrImgMaxWidth(currImgMaxWidthOpt);
        }


        // Set selection back
        setNewRange(selection, range.startContainer, range.startOffset, range.endContainer, range.endOffset);

        /**
         * Scroll the selected item to the middle of the screen
         * (set the middle of the keypad section to the screen center)
         * */
        if (editorSection) {
            window.scrollTo({
                top: editorSection.offsetTop + uniqueEl.offsetTop - window.innerHeight/2 + 53,
                left: 0,
                behavior: "smooth"
            })
        }
    }

    const resetState = () => {
        setOffsetTop(0);
        setOffsetWidth(0);
        setOffsetHeight(0);
    }


    const toggleUp = () => {
        const selection = document.getSelection();
        const range = selection.getRangeAt(0);

        const currLine = findLineContainer(range.startContainer);
        const prevLine = currLine.previousSibling;

        if (!prevLine) {
            updateState();
            return;
        }
        /**
         * Clone and delete prev line and insert it after curr line
         * */
        const prevLineClone = prevLine.cloneNode(true);

        range.selectNode(prevLine);
        range.deleteContents();

        range.setStartAfter(currLine);
        range.setEndAfter(currLine);

        range.insertNode(prevLineClone);
        selection.setPosition(currLine, 0);

        updateState();

        updatePostContentFunc();
    }


    const toggleDown = () => {
        const selection = document.getSelection();
        const range = selection.getRangeAt(0);

        const currLine = findLineContainer(range.startContainer);
        const nextLine = currLine.nextSibling;

        if (!nextLine) {
            updateState();
            return;
        }
        /**
         * Clone and delete next line and insert it before curr line,
         * */
        const nextLineClone = nextLine.cloneNode(true);

        range.selectNode(nextLine);
        range.deleteContents();

        range.setStartBefore(currLine);
        range.setEndBefore(currLine);

        range.insertNode(nextLineClone);
        selection.setPosition(currLine, 0);

        updateState();

        updatePostContentFunc();
    }


    const toggleImgWidth = (optIndex) => {
        const selection = document.getSelection();
        const range = selection.getRangeAt(0);

        const currLine = findLineContainer(range.startContainer);
        const uniqueEl = findUniqueElFromLine(currLine);

        if (uniqueEl.nodeName!=="IMG") return;

        const targetWidthPx = IMG_WIDTH_OPTIONS[optIndex];

        const naturalWidth = uniqueEl.naturalWidth;
        const naturalHeight = uniqueEl.naturalHeight;

        /**
         * Set img width/height
         * */
        uniqueEl.setAttribute("data-width", uniqueEl.naturalWidth);
        uniqueEl.setAttribute("data-height", uniqueEl.naturalHeight);


        const optionWidthLimitPx = findMatchingWidthOpt(IMG_WIDTH_OPTIONS, uniqueEl.naturalWidth);

        const targetWidth = Number(targetWidthPx.substr(0, targetWidthPx.length-2));
        const optionWidth = Number(optionWidthLimitPx.substr(0, optionWidthLimitPx.length-2));

        const isLastOption = targetWidth===optionWidth;

        /**
         * Set img style width/height which actually effects on displaying
         * */
        if (targetWidth<=optionWidth) {
            const lineWidth = currLine.offsetWidth;
            const ratio = lineWidth/naturalWidth;

            uniqueEl.style.width = (isLastOption? naturalWidth: targetWidth) + "px";
            // uniqueEl.style.height = Math.round(isLastOption? (ratio>=1)? naturalHeight: naturalHeight*ratio: targetWidth/naturalWidth*naturalHeight) + "px";
        }

        updateState();

        updatePostContentFunc();
    }


    useImperativeHandle(ref, () => ({
        show: () => {
            if (!checkSelectionWithinUniqueElLine() && !checkSelectionWithinLinkLine()) return;

            updateState();
            setVisible(true);
        },
        hide: () => {
            resetState();
            setVisible(false);
        },
        checkVisibility: () => {
            return visible;
        },
        checkMouseOverKeypad: () => {
            return IS_MOUSE_OVER_KEYPAD;
        },
        toggleUp: (keyType) => {
            setKeyBeingPressed(keyType);
            toggleUp();
        },
        toggleDown: (keyType) => {
            setKeyBeingPressed(keyType);
            toggleDown();
        },
        toggleImgMaxWidth: (index, keyType) => {
            setKeyBeingPressed(keyType);
            toggleImgWidth(index);
        },
        resetKeyPress: () => {
            setKeyBeingPressed(null);
        }
    }))

    return (
        <div
            className={`${style["container"]} ${visible? style["visible"]: ""}`}
            style={visible? {top: `${offsetTop-CONTAINER_BORDER_WIDTH}px`, left: `${-CONTAINER_BORDER_WIDTH}px`, width: `${offsetWidth+CONTAINER_BORDER_WIDTH*2}px`, height: `${offsetHeight+CONTAINER_BORDER_WIDTH*2}px`}: {}}
        >
            <section className={style["keypad"]}>

                {/** SIZE ADJ KEYS */}
                <section className={style["size-change-keys"]}>
                    <div className={style["row"]}>
                        <SizeNumKey idx={0} keyType="NUM1" keyPress={keyBeingPressed} text={<>!<br/>1</>} currElType={currElType} currMaxWidth={currImgMaxWidth} naturalWidth={currImgNaturalWidth} onClick={()=>toggleImgWidth(0)}/>
                        <SizeNumKey idx={1} keyType="NUM2" keyPress={keyBeingPressed} text={<>@<br/>2</>} currElType={currElType} currMaxWidth={currImgMaxWidth} naturalWidth={currImgNaturalWidth} onClick={()=>toggleImgWidth(1)}/>
                        <SizeNumKey idx={2} keyType="NUM3" keyPress={keyBeingPressed} text={<>#<br/>3</>} currElType={currElType} currMaxWidth={currImgMaxWidth} naturalWidth={currImgNaturalWidth} onClick={()=>toggleImgWidth(2)}/>
                        <SizeNumKey idx={3} keyType="NUM4" keyPress={keyBeingPressed} text={<>$<br/>4</>} currElType={currElType} currMaxWidth={currImgMaxWidth} naturalWidth={currImgNaturalWidth} onClick={()=>toggleImgWidth(3)}/>
                        <SizeNumKey idx={4} keyType="NUM5" keyPress={keyBeingPressed} text={<>%<br/>5</>} currElType={currElType} currMaxWidth={currImgMaxWidth} naturalWidth={currImgNaturalWidth} onClick={()=>toggleImgWidth(4)}/>
                    </div>
                </section>

                {/** LINE MOVE KEYS */}
                <section className={style["line-move-keys"]}>
                    <div className={style["row"]}>
                        <ArrowKey className={style["up"]} keyType="UP" keyPress={keyBeingPressed} onClick={toggleUp}/>
                    </div>
                    <div className={style["row"]}>
                        <ArrowKey className={style["left"]}/>
                        <ArrowKey className={style["down"]} keyType="DOWN" keyPress={keyBeingPressed} onClick={toggleDown}/>
                        <ArrowKey className={style["right"]}/>
                    </div>
                </section>

            </section>
        </div>
    );
});


export default UniqueElControlMenu;