import React, {useState, useRef} from "react";
import style from "./inlineElControlMenu.module.scss";


const InlineElControlMenu = ({enabled, yPos}) => {
    const menuBar = useRef();


    return (
        <div
            className={`${style["menu-bar"]} ${enabled && style["active"]}`}
            ref={menuBar}
            style={enabled? {top: `${yPos}px`}: {}}
            >
            CONTROL
        </div>
    );
};


export default InlineElControlMenu;