import React, {useRef, useState, useEffect, forwardRef, useImperativeHandle} from "react";
import {useSelector, useDispatch} from "react-redux"
import style from "./editor.module.scss"
import {updatePostAfterDelay} from "../../utils/editor/delayExecutor";
import {
    findLineContainer,
    findTextWrapperNode,
    findLineStartSpan,
    findCursorOffsetFromLine,
    findRangeOffsetFromLine,
    findUniqueElFromLine,
    checkLineContainsUniqueEl,
    checkEditorContentElTypes,
    checkLineContainsLinkEl
} from "../../utils/editor/commonUtils"
import {updatePostState} from "../../features/post/actions";
import {setEditorWritingBoard} from "../../features/editor/actions";


import STYLE_ from "../../utils/editor/styleOptions";
import KEYBOARD_ from "../../utils/editor/keyboard";
import POST_ from "../../utils/index/post";


let IS_EDITOR_FOCUSED;


/** Components */
import ToggleBar from "./modules/toggleBar";
import UniqueElControlMenu from "./modules/uniqueElControlMenu";
import UniqueElToggleKeys from "./modules/uniqueElToggleKeys";
import EditorPlaceholder from "./modules/editorPlaceholder";
import ClearPostAndUpdateTimeSection from "./modules/clearPostAndUpdateTimeSection";




const checkNextSiblingExist = (node) => {
    const nextSibling = node.nextSibling;

    if (!nextSibling || nextSibling && !nextSibling.textContent && !nextSibling.nextSibling) return null;
    else if (nextSibling && nextSibling.textContent && !nextSibling.nextSibling) return !!nextSibling;
    else if (nextSibling && nextSibling.nextSibling) return !!checkNextSiblingExist(nextSibling) || !!nextSibling;
}

const checkEmptyEditor = (writingBoard) => {
    const contents = writingBoard.current.children

    return contents.length===1 && !findUniqueElFromLine(contents[0]) && !contents[0].textContent;
}

const applySpanStyleToLine = (span, line) => {
    const _span = findTextWrapperNode(span);

    STYLE_.ATTRIBUTES.forEach(attr => {
        const spanStyle = _span.style[attr];

        if (spanStyle) line.style[attr] = STYLE_.DEFAULT_OPTIONS[attr].value===spanStyle? null: spanStyle;
    });

    if (STYLE_.ATTRIBUTES.every(attr => !line.style[attr])) line.removeAttribute("style");
}

const deleteLine = (range, line) => {
    range.selectNode(line);
    range.deleteContents();
}

const insertEmptyCloneLineBeforeLine = (range, lineToClone, spanAtLineStart) => {
    const emptyLine = lineToClone.cloneNode();

    emptyLine.appendChild(document.createElement("br"));

    if (spanAtLineStart) {
        STYLE_.ATTRIBUTES.forEach(attr => {
            const spanStyle = spanAtLineStart.style[attr];
            if (spanStyle) emptyLine.style[attr] = spanStyle===STYLE_.DEFAULT_OPTIONS[attr].value? null: spanStyle;
        })

        if (STYLE_.ATTRIBUTES.every(attr => !emptyLine.style[attr])) emptyLine.removeAttribute("style");
    }

    range.setStartBefore(lineToClone);
    range.setEndBefore(lineToClone);
    range.insertNode(emptyLine);
}

const rangeWork__deletingSpanAtInitLine = (e, range, selection, rangeStartLine, rangeEndLine) => {
    /**
     * Range end at offset 0 of the next line
     * Delete the range and the range start line
     * :: When triple click to select the line or the cursor at offset 0 of a line
     * */
    if (range.endContainer.nodeName==="P") {
        e.preventDefault();
        range.deleteContents();

        range.selectNode(rangeStartLine);
        range.deleteContents();

        selection.setPosition(rangeEndLine, 0);
    }
    /**
     * Range end within a line
     * */
    else {
        /**
         * Range end is at the end of the line
         * Make the range an empty line
         * */
        if (findRangeOffsetFromLine(range.endContainer, range.endOffset)===rangeEndLine.textContent.length) {
            e.preventDefault();
            range.deleteContents();

            // Range within the same line
            if (rangeStartLine===rangeEndLine) {

                // Remove automatically generated SPAN
                range.selectNodeContents(rangeStartLine);
                range.deleteContents();

                range.insertNode(document.createElement("br"));
                selection.setPosition(rangeStartLine, 0);
            }
            // Range contains multiple lines
            // Remove all in range and leave the rest of range end line
            else {
                range.selectNode(rangeStartLine);
                range.deleteContents();

                range.selectNodeContents(rangeEndLine);
                range.deleteContents();

                rangeEndLine.appendChild(document.createElement("br"));
                selection.setPosition(rangeEndLine, 0);
            }
        }
        /**
         * Range end within a line, and not at the end of the line
         * Delete the range and leave the rest of end line
         * */
        else {
            // Range within the same line
            if (rangeStartLine===rangeEndLine) {
                /**
                 * Range start, end both within the first SPAN of the line
                 * */
                const rangeStartWrapper = findTextWrapperNode(range.startContainer);
                const rangeEndWrapper = findTextWrapperNode(range.endContainer);

                // Range end is also at the end of the SPAN, i.g. Range is wrapping the SPAN
                if (rangeStartWrapper===rangeEndWrapper && rangeEndWrapper.textContent.length===range.endOffset) {
                    e.preventDefault();

                    // Remove SPAN
                    range.selectNode(rangeStartWrapper);
                    range.deleteContents();

                    selection.setPosition(rangeEndLine, 0);
                }
            }
            // Range contains multiple lines
            else {
                e.preventDefault();
                range.deleteContents();

                range.selectNode(rangeStartLine);
                range.deleteContents();

                range.setStartBefore(rangeEndLine.firstChild);
                range.setEndBefore(rangeEndLine.firstChild);
            }
        }
    }
}

const rangeWork__deleteRange_insertLineWithContentsAfterRangeEnd = (e, selection, range, rangeEndLine, isRangeStartAtLineStart) => {
    e.preventDefault();

    const currLine = findLineContainer(range.startContainer);

    // Delete range and extract the right side contents from the range
    range.deleteContents();
    range.setEndAfter(currLine.lastChild);

    const contentsAfterRangeEnd = [...range.extractContents().childNodes.values()];


    /**
     * Extracted range-right contents have text node,
     * Preserve the line style and line break
     * */
    if (contentsAfterRangeEnd.some(node => node.nodeName==="#text" && node.textContent)) {
        const lineClone = currLine.cloneNode();

        contentsAfterRangeEnd.forEach(node => lineClone.appendChild(node));

        range.setStartAfter(currLine);
        range.setEndAfter(currLine);
        range.insertNode(lineClone);

        const span = findLineStartSpan(lineClone);

        if (isRangeStartAtLineStart) {
            deleteLine(range, currLine);
            insertEmptyCloneLineBeforeLine(range, lineClone, span);
        }

        selection.setPosition(span? span.firstChild: lineClone, 0);
    }
    /**
     * All of the contents in the extracted range-right contents are SPAN,
     * Remove duplicated styles from contents and apply to the line style
     * */
    else {
        e.preventDefault();
        const newLine = document.createElement("p");

        // Merge duplicated styles from the contents and append to the new line
        contentsAfterRangeEnd.filter(node => node.textContent).forEach(node => {
            const newNodeStyles = [];

            STYLE_.ATTRIBUTES.forEach(attr => {
                const nodeStyle = node.style[attr];
                const defStyle = STYLE_.DEFAULT_OPTIONS[attr].value;

                if (nodeStyle && nodeStyle!==defStyle) newNodeStyles.push(attr);
            })

            if (newNodeStyles.length>0) {
                const span = document.createElement("span");
                span.appendChild(document.createTextNode(node.textContent));

                newNodeStyles.forEach(attr => span.style[attr] = node.style[attr]);

                newLine.appendChild(span);
            }
            else newLine.appendChild(document.createTextNode(node.textContent));
        })


        range.setStartAfter(currLine);
        range.setEndAfter(currLine);
        range.insertNode(newLine);

        const span = findLineStartSpan(newLine);

        if (isRangeStartAtLineStart) {
            deleteLine(range, currLine);
            insertEmptyCloneLineBeforeLine(range, newLine, span);
        }

        selection.setPosition(span? span.firstChild: rangeEndLine, 0);
    }
}


const insertHTML = (html) => {
    html = typeof html === "string" ? html : html.outerHTML;
    document.execCommand("insertHTML", false, html);
}

const insertText = (text) => {
    document.execCommand("insertText", false, text);
}



/**
 * Editor
 * */
const Editor = forwardRef(({}, ref) => {
    const dispatch = useDispatch();



    const postState = useSelector(state => state.post);
    const {editMode} = postState;
    const isEditMode = editMode===POST_.EDIT_MODE.EDIT;

    const editorState = useSelector(state => state.editor);
    const {editorWritingBoardRef, routeOnAutoSaveFail} = editorState;


    const topMenuToggleBarRef = useRef(null);
    const writingBoardRef = useRef(null);
    const editorSectionRef = useRef(null);
    const editorPlaceholderRef = useRef(null);
    const uniqueElControlMenuRef = useRef(null);
    const uniqueElToggleKeyRef = useRef(null);





    const updatePostContentStateAndTempPostData = async (delay) => {
        if (!checkEditorContentElTypes(writingBoardRef.current.childNodes)) return false;

        const content = writingBoardRef.current.innerHTML;

        dispatch(updatePostState("content", content));


        if (!isEditMode) await updatePostAfterDelay(postState, "content", content, delay, routeOnAutoSaveFail);

        return true;
    }


    const clearWritingBoard = () => {
        const p = document.createElement("p");

        p.appendChild(document.createElement("br"));

        writingBoardRef.current.innerHTML = p.outerHTML;

        if (checkEmptyEditor(writingBoardRef)) editorPlaceholderRef.current.show();
    }


    // ON BLUR
    const onFocus = (e) => {
        IS_EDITOR_FOCUSED = true;

        topMenuToggleBarRef.current.setWritingBoardNode(writingBoardRef.current)

        editorPlaceholderRef.current.hide();
    }

    // ON BLUR
    const onBlur = (e) => {
        IS_EDITOR_FOCUSED = false;

        if (!uniqueElToggleKeyRef.current.checkMouseOverKeypad()) uniqueElToggleKeyRef.current.hide();

        if (!uniqueElControlMenuRef.current.checkMouseOverKeypad()) uniqueElControlMenuRef.current.hide();

        if (checkEmptyEditor(writingBoardRef)) editorPlaceholderRef.current.show();
    }


    // ON KEY DOWN
    const onKeyDown = (e) => {
        const selection = document.getSelection();
        const range = selection.getRangeAt(0);
        const keyCode = e.keyCode;
        const ctrlKey = e.ctrlKey;


        if (selection.focusNode.nodeName==="DIV") {
            e.preventDefault();
            return;
        }



        /**
         * On press repeat
         * */
        if (e.repeat) {
            /**
             * Unique El Menu toggle
             * */
            if (uniqueElControlMenuRef.current.checkVisibility()) {
                /**
                 * Image max width toggle
                 * */
                if ([
                    KEYBOARD_.KEY._1,
                    KEYBOARD_.KEY._2,
                    KEYBOARD_.KEY._3,
                    KEYBOARD_.KEY._4,
                    KEYBOARD_.KEY._5,
                    KEYBOARD_.KEY.UP,
                    KEYBOARD_.KEY.DOWN
                ].includes(keyCode)) {

                    e.preventDefault();

                    switch (keyCode) {
                        case KEYBOARD_.KEY.UP:
                            uniqueElControlMenuRef.current.toggleUp("UP");
                            break;
                        case KEYBOARD_.KEY.DOWN:
                            uniqueElControlMenuRef.current.toggleDown("DOWN");
                            break;
                        case KEYBOARD_.KEY._1:
                            uniqueElControlMenuRef.current.toggleImgMaxWidth(0, "NUM1");
                            break;
                        case KEYBOARD_.KEY._2:
                            uniqueElControlMenuRef.current.toggleImgMaxWidth(1, "NUM2");
                            break;
                        case KEYBOARD_.KEY._3:
                            uniqueElControlMenuRef.current.toggleImgMaxWidth(2, "NUM3");
                            break;
                        case KEYBOARD_.KEY._4:
                            uniqueElControlMenuRef.current.toggleImgMaxWidth(3, "NUM4");
                            break;
                        case KEYBOARD_.KEY._5:
                            uniqueElControlMenuRef.current.toggleImgMaxWidth(4, "NUM5");
                            break;
                    }

                    // Stop propagation
                    return;
                }
            }


            if (ctrlKey && keyCode===KEYBOARD_.KEY._Z) e.preventDefault();
            else if (keyCode===KEYBOARD_.KEY.TAB) e.preventDefault();


            if (selection.type==="Caret") {
                const currLine = findLineContainer(range.endContainer);
                const prevLine = currLine.previousSibling;
                const nextLine = currLine.nextSibling;

                const isCursorAtLineStart = findCursorOffsetFromLine(selection)===0;
                const isCursorAtLineEnd = findCursorOffsetFromLine(selection)===currLine.textContent.length;
                const spanAtLineStart = findTextWrapperNode(range.startContainer);
                const isCurrLineHasUniqueEl = checkLineContainsUniqueEl(currLine);
                const isPrevLineHasUniqueEl = checkLineContainsUniqueEl(prevLine);
                const isNextLineHasUniqueEl = checkLineContainsUniqueEl(nextLine);
                const isCurrLineHasLink = checkLineContainsLinkEl(currLine);
                const isPrevLineHasLink = checkLineContainsLinkEl(prevLine);
                const isNextLineHasLink = checkLineContainsLinkEl(nextLine);


                switch (keyCode) {
                    case KEYBOARD_.KEY.ENTER:
                        /**
                         * ENTER at the link
                         * Not at the start/end of the line
                         * */
                        if (isCurrLineHasLink && !isCursorAtLineStart && !isCursorAtLineEnd) {
                            e.preventDefault();
                        }
                        /**
                         * ENTER at the link
                         * At the start of the line
                         * Make new line before the line
                         * */
                        else if (isCurrLineHasLink && isCursorAtLineStart && !isCursorAtLineEnd) {
                            e.preventDefault();

                            const newLine = document.createElement("p");
                            newLine.appendChild(document.createElement("br"));

                            range.setStartBefore(currLine);
                            range.setEndBefore(currLine);

                            range.insertNode(newLine);
                            selection.setPosition(currLine, 0);
                        }
                        /**
                         * ENTER at the link
                         * At the end of the line
                         * Make a new line after the line
                         * */
                        else if (isCurrLineHasLink && !isCursorAtLineStart && isCursorAtLineEnd) {
                            e.preventDefault();

                            const newLine = document.createElement("p");
                            newLine.appendChild(document.createElement("br"));

                            range.setStartAfter(currLine);
                            range.setEndAfter(currLine);

                            range.insertNode(newLine);
                            selection.setPosition(newLine, 0);
                        }
                        /**
                         * ENTER at the 0 offset of a text node, start of a text node
                         * Clone the line to prevent automatically generate SPAN at the start of the line
                         * */
                        else if (isCursorAtLineStart && !spanAtLineStart && !isCurrLineHasUniqueEl) {
                            e.preventDefault();

                            const lineClone = currLine.cloneNode();
                            lineClone.appendChild(document.createElement("br"));

                            range.setStartBefore(currLine);
                            range.setEndBefore(currLine);

                            range.insertNode(lineClone);
                            selection.setPosition(currLine, 0);
                        }
                        /**
                         * ENTER at the 0 offset of a text line, start of a SPAN,
                         * Apply SPAN's style to the new line to break(a clone of current line)
                         * */
                        else if (isCursorAtLineStart && !isCurrLineHasUniqueEl && spanAtLineStart) {
                            e.preventDefault();

                            const lineClone = currLine.cloneNode();
                            lineClone.appendChild(document.createElement("br"));

                            const span = findTextWrapperNode(range.startContainer);

                            STYLE_.ATTRIBUTES.forEach(attr => {
                                const spanStyle = span.style[attr];

                                if (spanStyle) lineClone.style[attr] = STYLE_.DEFAULT_OPTIONS[attr].value===spanStyle? null: spanStyle;
                            })

                            range.setStartBefore(currLine);
                            range.setEndBefore(currLine);

                            range.insertNode(lineClone);

                            selection.setPosition(span.firstChild, 0);
                        }
                        /**
                         * ENTER at the end of a text line, end of a SPAN,
                         * Apply SPAN's style to the new line to break(a clone of current line)
                         * */
                        else if (isCursorAtLineEnd && !isCurrLineHasUniqueEl && spanAtLineStart) {
                            e.preventDefault();

                            const lineClone = currLine.cloneNode();
                            lineClone.appendChild(document.createElement("br"));

                            const span = findTextWrapperNode(range.startContainer);

                            STYLE_.ATTRIBUTES.forEach(attr => {
                                const spanStyle = span.style[attr];

                                if (spanStyle) lineClone.style[attr] = STYLE_.DEFAULT_OPTIONS[attr].value===spanStyle? null: spanStyle;
                            })

                            range.setStartAfter(currLine);
                            range.setEndAfter(currLine);

                            range.insertNode(lineClone);
                            selection.setPosition(lineClone, 0);
                        }
                        break;


                    /**
                     * BACKSPACE
                     * */
                    case KEYBOARD_.KEY.BACKSPACE:
                        /**
                         * Cursor at init line and init position,
                         * At a text line
                         * */
                        if (isCursorAtLineStart && writingBoardRef.current.children.length===1 && !isCurrLineHasUniqueEl) {
                            e.preventDefault();
                        }
                        /**
                         * BACKSPACE at the start of next line of a unique content line
                         * */
                        else if (isCursorAtLineStart && prevLine && isPrevLineHasUniqueEl) {
                            e.preventDefault();
                        }
                        /**
                         * BACKSPACE at the unique content line,
                         * Not at the only line left in the editor
                         * */
                        else if (isCurrLineHasUniqueEl) {
                            e.preventDefault();
                        }
                        /**
                         * BACKSPACE at 1 step before the 0 offset inside a SPAN which is the only wrapping SPAN left in the line,
                         * Extract styles from the SPAN and merge it to the lines
                         * */
                        else if (findCursorOffsetFromLine(selection)===1 && !isCurrLineHasUniqueEl && spanAtLineStart) {
                            const span = findTextWrapperNode(range.startContainer);

                            if ([...currLine.childNodes.values()].every(node => node!==span && node.nodeName!=="SPAN")) applySpanStyleToLine(range.startContainer, currLine);
                        }
                        /**
                         * BACKSPACE at the start of the line
                         * Prev line is link
                         * Move the cursor to the end of the prev line
                         * */
                        else if (isCursorAtLineStart && prevLine && isPrevLineHasLink) {
                            e.preventDefault();
                        }
                        /**
                         * BACKSPACE at the link
                         * Cursor at the start of the line
                         * Move the cursor to the end of the prev line
                         * */
                        else if (isCurrLineHasLink && isCursorAtLineStart && prevLine) {
                            e.preventDefault();
                        }
                        break;
                    case KEYBOARD_.KEY.DELETE:
                        /**
                         * DELETE at the end of the prev line of a unique content line,
                         * And the curr line is not empty
                         * */
                        if (currLine.textContent && isCursorAtLineEnd && isNextLineHasUniqueEl) {
                            e.preventDefault();
                        }
                        /**
                         * DELETE at the unique content line
                         * */
                        else if (isCurrLineHasUniqueEl) {
                            e.preventDefault();
                        }
                        /**
                         * DELETE at the 0 offset in a SPAN which is only wrapper in the line,
                         * Extract styles from the SPAN and merge it to the lines
                         * */
                        else if (isCursorAtLineStart && spanAtLineStart && currLine.textContent.length===1) {
                            applySpanStyleToLine(range.startContainer, currLine);
                        }
                        /**
                         * DELETE at the prev line of a link
                         * Move the cursor to the start of the link
                         * */
                        else if (nextLine && isCursorAtLineEnd && isNextLineHasLink) {
                            e.preventDefault();

                            selection.setPosition(nextLine, 0);
                        }
                        /**
                         * DELETE at the link
                         * Cursor at the end of the line
                         * Move the cursor to the start of the next line
                         * */
                        else if (isCurrLineHasLink && nextLine && isCursorAtLineEnd) {
                            e.preventDefault();

                            selection.setPosition(nextLine, 0);
                        }
                        break;
                    case KEYBOARD_.KEY.CTRL:
                    case KEYBOARD_.KEY.ALT:
                    case KEYBOARD_.KEY.ESC:
                    case KEYBOARD_.KEY.UP:
                    case KEYBOARD_.KEY.DOWN:
                        break;
                    default:
                        /**
                         * Cursor in a unique content line,
                         * At the start of a unique content line
                         * */
                        if (range.startOffset===0 && checkLineContainsUniqueEl(findLineContainer(range.endContainer))) {
                            if (prevLine && prevLine.textContent) {
                                range.setStartAfter(prevLine.lastChild)
                                range.setEndAfter(prevLine.lastChild)
                            }
                            else if (prevLine && !prevLine.textContent) {
                                e.preventDefault();
                                selection.setPosition(prevLine.firstChild, 0);
                            }
                        }
                        break;
                }
            }

        }
        if (e.repeat) return;



        // CTRL
        if (ctrlKey) {
            const keysPushWith = [
                KEYBOARD_.KEY.SPACE,
                KEYBOARD_.KEY._X,
                KEYBOARD_.KEY._C,
                KEYBOARD_.KEY._D,
                KEYBOARD_.KEY._V,
                KEYBOARD_.KEY._A,
                KEYBOARD_.KEY._F,
                KEYBOARD_.KEY._B,
                KEYBOARD_.KEY._I
            ].concat(KEYBOARD_.CURSOR_MOVE_KEYS);

            if (!keysPushWith.includes(keyCode)) e.preventDefault();


            /** CTRL + SPACE */
            if (keyCode === KEYBOARD_.KEY.SPACE) {
                uniqueElControlMenuRef.current.show();
                uniqueElToggleKeyRef.current.updateState();
            } else {
                uniqueElControlMenuRef.current.hide();
            }

            switch (keyCode) {
                case KEYBOARD_.KEY._D:
                    e.preventDefault();
                    topMenuToggleBarRef.current.toggleFontSize();
                    break;
                case KEYBOARD_.KEY._B:
                    e.preventDefault();
                    topMenuToggleBarRef.current.toggleFontWeight();
                    break;
                case KEYBOARD_.KEY._I:
                    e.preventDefault();
                    topMenuToggleBarRef.current.toggleFontStyle();
                    break;
                case KEYBOARD_.KEY._E:
                    e.preventDefault();
                    topMenuToggleBarRef.current.toggleColor();
                    break;
            }
        }


        // DEFAULT
        else {
            switch (keyCode) {

                /**
                 * ENTER
                 * */
                case KEYBOARD_.KEY.ENTER:
                    if (selection.type==="Caret") {
                        const currLine = findLineContainer(range.startContainer);
                        const prevLine = currLine.previousSibling;
                        const nextLine = currLine.nextSibling;

                        const isCursorAtLineStart = findCursorOffsetFromLine(selection)===0;
                        const isCursorAtLineEnd = findCursorOffsetFromLine(selection)===currLine.textContent.length;
                        const isSpanAtLineStart = findTextWrapperNode(range.startContainer);
                        const isCurrLineHasUniqueEl = checkLineContainsUniqueEl(currLine);
                        const isCurrLineHasLinkEl = checkLineContainsLinkEl(currLine);


                        /**
                         * ENTER at the link
                         * Not at the start/end of the line
                         * */
                        if (isCurrLineHasLinkEl && !isCursorAtLineStart && !isCursorAtLineEnd) {
                            e.preventDefault();
                        }
                        /**
                         * ENTER at the link
                         * At the start of the line
                         * Make new line before the line
                         * */
                        else if (isCurrLineHasLinkEl && isCursorAtLineStart && !isCursorAtLineEnd) {
                            e.preventDefault();

                            const newLine = document.createElement("p");
                            newLine.appendChild(document.createElement("br"));

                            range.setStartBefore(currLine);
                            range.setEndBefore(currLine);

                            range.insertNode(newLine);
                            selection.setPosition(currLine, 0);
                        }
                        /**
                         * ENTER at the link
                         * At the end of the line
                         * Make a new line after the line
                         * */
                        else if (isCurrLineHasLinkEl && !isCursorAtLineStart && isCursorAtLineEnd) {
                            e.preventDefault();

                            const newLine = document.createElement("p");
                            newLine.appendChild(document.createElement("br"));

                            range.setStartAfter(currLine);
                            range.setEndAfter(currLine);

                            range.insertNode(newLine);
                            selection.setPosition(newLine, 0);
                        }
                        /**
                         * ENTER at the 0 offset of a text node, start of a text node
                         * Clone the line to prevent automatically generate SPAN at the start of the line
                         * */
                        else if (isCursorAtLineStart && !isSpanAtLineStart && !isCurrLineHasUniqueEl) {
                            e.preventDefault();

                            const lineClone = currLine.cloneNode();
                            lineClone.appendChild(document.createElement("br"));

                            range.setStartBefore(currLine);
                            range.setEndBefore(currLine);

                            range.insertNode(lineClone);
                            selection.setPosition(currLine, 0);
                        }
                        /**
                         * ENTER at the 0 offset of a text line, start of a SPAN,
                         * Apply SPAN's style to the new line to break(a clone of current line)
                         * */
                        else if (isCursorAtLineStart && !isCurrLineHasUniqueEl && isSpanAtLineStart) {
                            e.preventDefault();

                            const lineClone = currLine.cloneNode();
                            lineClone.appendChild(document.createElement("br"));

                            const span = findTextWrapperNode(range.startContainer);

                            STYLE_.ATTRIBUTES.forEach(attr => {
                                const spanStyle = span.style[attr];

                                if (spanStyle) lineClone.style[attr] = STYLE_.DEFAULT_OPTIONS[attr].value===spanStyle? null: spanStyle;
                            })

                            range.setStartBefore(currLine);
                            range.setEndBefore(currLine);

                            range.insertNode(lineClone);

                            selection.setPosition(span.firstChild, 0);
                        }
                        /**
                         * ENTER at the end of a text line, end of a SPAN,
                         * Apply SPAN's style to the new line to break(a clone of current line)
                         * */
                        else if (isCursorAtLineEnd && !isCurrLineHasUniqueEl && isSpanAtLineStart) {
                            e.preventDefault();

                            const lineClone = currLine.cloneNode();
                            lineClone.appendChild(document.createElement("br"));

                            const span = findTextWrapperNode(range.startContainer);

                            STYLE_.ATTRIBUTES.forEach(attr => {
                                const spanStyle = span.style[attr];

                                if (spanStyle) lineClone.style[attr] = STYLE_.DEFAULT_OPTIONS[attr].value===spanStyle? null: spanStyle;
                            })

                            range.setStartAfter(currLine);
                            range.setEndAfter(currLine);

                            range.insertNode(lineClone);
                            selection.setPosition(lineClone, 0);
                        }
                    }
                    else if (selection.type==="Range") {
                        const rangeStartLine = findLineContainer(range.startContainer);
                        const rangeEndLine = findLineContainer(range.endContainer);

                        const isRangeStartLineHasUniqueEl = checkLineContainsUniqueEl(rangeStartLine);
                        const isRangeEndLineHasUniqueEl = checkLineContainsUniqueEl(rangeEndLine);
                        const isRangeInline = rangeStartLine===rangeEndLine;
                        const isRangeStartAtLineStart = findRangeOffsetFromLine(range.startContainer, range.startOffset)===0;
                        const isRangeEndAtLineEnd = findRangeOffsetFromLine(range.endContainer, range.endOffset)===rangeEndLine.textContent.length;

                        /**
                         * ENTER range start at offset 0 of a text line
                         * Range end in the same line,
                         * Range end within a text line, Not at the end of the line
                         * */
                        if (isRangeInline && isRangeStartAtLineStart && !isRangeEndAtLineEnd && !isRangeStartLineHasUniqueEl && !isRangeEndLineHasUniqueEl) {
                            const spanAtRangeEnd = findTextWrapperNode(range.endContainer);
                            const isRangeEndAtNodeEnd = range.endContainer.textContent.length===range.endOffset;
                            const spanAtNodeNext = (range.endContainer.nextSibling && range.endContainer.nextSibling.nodeName==="SPAN") && range.endContainer.nextSibling;

                            if (
                                /**
                                 * Range end in a #text,
                                 * At the end of element,
                                 * A SPAN right next to the element
                                 * */
                                (range.endContainer.nodeName==="#text" && isRangeEndAtNodeEnd && spanAtNodeNext) ||
                                /**
                                 * Range end in a SPAN
                                 * */
                                (spanAtRangeEnd)
                            ) {
                                rangeWork__deleteRange_insertLineWithContentsAfterRangeEnd(e, selection, range, rangeEndLine, true);
                            }
                        }
                        /**
                         * ENTER range start at offset 0 of a text line,
                         * Range end is not in the same line,
                         * Range end within a text line, Not at the end of the line
                         * */
                        else if (!isRangeInline && isRangeStartAtLineStart && !isRangeEndAtLineEnd && !isRangeStartLineHasUniqueEl && !isRangeEndLineHasUniqueEl) {
                            e.preventDefault();

                            range.deleteContents();

                            range.selectNode(rangeStartLine);
                            range.deleteContents();

                            const span = findLineStartSpan(rangeEndLine);

                            insertEmptyCloneLineBeforeLine(range, rangeEndLine, span);

                            selection.setPosition(span? span.firstChild: rangeEndLine, 0);
                        }
                        /**
                         * ENTER range within a text line,
                         * Range start within a SPAN,
                         * Range start offset >= 1(not at the start of the line),
                         * Organize nested styles of the new line
                         * */
                        else if (!isRangeStartAtLineStart && isRangeInline && findTextWrapperNode(range.startContainer)) {
                            rangeWork__deleteRange_insertLineWithContentsAfterRangeEnd(e, selection, range, rangeEndLine);
                        }
                    }
                    break;


                /**
                 * BACKSPACE
                 * */
                case KEYBOARD_.KEY.BACKSPACE:
                    const cursorAtInitLine = findCursorOffsetFromLine(selection)===0 && writingBoardRef.current.children.length === 1;

                    if (selection.type === "Caret") {
                        const currLine = findLineContainer(range.startContainer);
                        const prevLine = currLine.previousSibling;
                        const nextLine = currLine.nextSibling;

                        const isCursorAtLineStart = findCursorOffsetFromLine(selection)===0;
                        const isCursorAtLineEnd = findCursorOffsetFromLine(selection)===currLine.textContent.length;
                        const isSpanAtLineStart = findTextWrapperNode(range.startContainer);
                        const isCurrLineHasUniqueEl = checkLineContainsUniqueEl(currLine);
                        const isPrevLineHasUniqueEl = checkLineContainsUniqueEl(prevLine);
                        const isCurrLineHasLink = checkLineContainsLinkEl(currLine);
                        const isPrevLineHasLink = checkLineContainsLinkEl(prevLine);
                        const isNextLineHasLink = checkLineContainsLinkEl(nextLine);


                        /**
                         * BACKSPACE at the init line and offset 0 of the editor,
                         * Not at the unique content line
                         **/
                        if (cursorAtInitLine && !isCurrLineHasUniqueEl) {
                            e.preventDefault();
                        }
                        /**
                         * BACKSPACE at the start of the next line of a unique content line,
                         * Move cursor to the end of the prev line
                         **/
                        else if (prevLine && range.startOffset===0 && (currLine.textContent || isCurrLineHasUniqueEl) && (isPrevLineHasUniqueEl || isPrevLineHasLink)) {
                            e.preventDefault();
                            range.setEndAfter(prevLine.lastChild);
                        }
                        /**
                         * BACKSPACE at the start of the unique content line, and prev line exists
                         * Move the cursor
                         * */
                        else if (prevLine && prevLine.textContent && range.startOffset===0 && (isCurrLineHasUniqueEl || isCurrLineHasLink)) {
                            e.preventDefault();
                            range.setEndAfter(prevLine.lastChild);
                        }
                        /**
                         * BACKSPACE at 1 step before the 0 offset inside a SPAN which is the only wrapping SPAN left in the line,
                         * Extract styles from the SPAN and merge it to the lines
                         * */
                        else if (findCursorOffsetFromLine(selection)===1 && !isCurrLineHasUniqueEl && isSpanAtLineStart) {
                            const span = findTextWrapperNode(range.startContainer);

                            if ([...currLine.childNodes.values()].every(node => node!==span && node.nodeName!=="SPAN")) applySpanStyleToLine(range.startContainer, currLine);
                        }
                    }
                    else if (selection.type === "Range") {
                        const rangeStartLine = findLineContainer(range.startContainer);
                        const rangeEndLine = findLineContainer(range.endContainer);
                        const rangeEndPrevLine = rangeEndLine.previousSibling;

                        const isRangeStartLineHasUniqueEl = checkLineContainsUniqueEl(rangeStartLine);
                        const isRangeEndLineHasUniqueEl = checkLineContainsUniqueEl(rangeEndLine);
                        const isRangeStartLineHasLink = checkLineContainsLinkEl(rangeStartLine);
                        const isRangeEndLineHasLink = checkLineContainsLinkEl(rangeEndLine);

                        const isRangeInline = rangeStartLine===rangeEndLine;
                        const isRangeStartAtLineStart = findRangeOffsetFromLine(range.startContainer, range.startOffset)===0;
                        const isRangeEndAtLineStart = findRangeOffsetFromLine(range.endContainer, range.endOffset)===0;
                        const isRangeEndAtLineEnd = findRangeOffsetFromLine(range.endContainer, range.endOffset)===rangeEndLine.textContent.length;
                        const isSpanAtRangeStart = findTextWrapperNode(range.startContainer);



                        /**
                         * BACKSPACE at the init line and offset 0 of editor
                         **/
                        if (cursorAtInitLine) {
                            e.preventDefault();

                            const currLine = findLineContainer(range.startContainer);
                            const cloneLine = currLine.cloneNode();

                            if (currLine.hasChildNodes() && !checkLineContainsUniqueEl(currLine)) {
                                const list = [...currLine.childNodes].filter(node => node.textContent);

                                if (list.length>0 && list[0].nodeName==="SPAN") applySpanStyleToLine(list[0], cloneLine);
                            }

                            cloneLine.appendChild(document.createElement("br"));

                            range.setStartBefore(currLine);
                            range.setEndAfter(currLine);
                            range.deleteContents();
                            range.insertNode(cloneLine);

                            range.setStartBefore(cloneLine.firstChild);
                            range.setEndBefore(cloneLine.firstChild);
                        }
                        /**
                         * BACKSPACE range start at a text line,
                         * Range end at the unique content line,
                         * Move range end to the prev line end
                         * */
                        else if (rangeEndPrevLine && rangeEndPrevLine.textContent && range.endOffset===0 && isRangeEndLineHasUniqueEl) {
                            range.setEndAfter(rangeEndPrevLine.lastChild);
                        }
                        /**
                         * BACKSPACE range start at a unique content line,
                         * Range end at a unique content line,
                         * Collapse range to the prev line end
                         * */
                        else if (isRangeStartLineHasUniqueEl && isRangeEndLineHasUniqueEl) {
                            range.setEndAfter(rangeEndPrevLine.lastChild);
                        }
                        /**
                         * BACKSPACE range start at the end of a unique content line,
                         * range end at a text line,
                         * Delete the range and set cursor at the start of the end container line
                         * */
                        else if (range.startOffset===1 && isRangeStartLineHasUniqueEl && rangeEndLine.textContent) {
                            e.preventDefault();
                            range.deleteContents();

                            if (!rangeEndLine.textContent) rangeEndLine.appendChild(document.createElement("br"));

                            selection.setPosition(rangeEndLine, 0);
                        }
                        /**
                         * BACKSPACE range start at offset 0 of a text line, has SPAN at the start of the line
                         * Range end is in a text line
                         * */
                        else if (isRangeStartAtLineStart && !isRangeStartLineHasUniqueEl && isSpanAtRangeStart && !isRangeEndLineHasUniqueEl) {
                            rangeWork__deletingSpanAtInitLine(e, range, selection, rangeStartLine, rangeEndLine);
                        }
                        /**
                         * BACKSPACE Range start within a link,
                         * Multiple lines,
                         * Set range start at the start of the next line
                         * */
                        else if (rangeStartLine!==rangeEndLine && isRangeStartLineHasLink) {
                            if (rangeEndLine===rangeStartLine.nextSibling && isRangeEndAtLineStart) {
                                range.setEndAfter(rangeStartLine.lastChild);
                            } else {
                                range.setStartBefore(rangeStartLine.nextSibling.firstChild);
                            }
                        }
                        /**
                         * BACKSPACE Range end within a link,
                         * Multiple lines,
                         * Set range end at the end of the prev line
                         * */
                        else if (rangeStartLine!==rangeEndLine && isRangeEndLineHasLink) {
                            range.setEndAfter(rangeEndLine.previousSibling.lastChild);
                        }
                    }
                    break;


                /**
                 * DELETE
                 * */
                case KEYBOARD_.KEY.DELETE:
                    if (selection.type === "Caret") {
                        const currLine = findLineContainer(range.endContainer);
                        const nextLine = currLine.nextSibling;
                        const endOffset = range.endOffset;

                        const isCursorAtLineStart = findCursorOffsetFromLine(selection)===0;
                        const isCursorAtLineEnd = findCursorOffsetFromLine(selection)===currLine.textContent.length;
                        const isSpanAtLineStart = findTextWrapperNode(range.startContainer);
                        const isCurrLineHasUniqueEl = checkLineContainsUniqueEl(currLine);
                        const isNextLineHasUniqueEl = checkLineContainsUniqueEl(nextLine);

                        const isCurrLineHasLink = checkLineContainsLinkEl(currLine);
                        const isNextLineHasLink = checkLineContainsLinkEl(nextLine);


                        /**
                         * DELETE at the end of unique content line,
                         * Move cursor to the start of the next line
                         * */
                        if (nextLine && endOffset===1 && isCurrLineHasUniqueEl) {
                            e.preventDefault();
                            selection.setPosition(nextLine.firstChild, 0);
                        }

                        /**
                         * DELETE cursor at the end of a text line,
                         * Next line is a unique content line,
                         * Move cursor to the start of the next line
                         * */
                        else if (isCursorAtLineEnd && nextLine && isNextLineHasUniqueEl && !isCurrLineHasUniqueEl) {
                            e.preventDefault();
                            /**
                             * Cursor at the line start, and line is empty, delete it then.
                             * */
                            if (isCursorAtLineStart && !currLine.textContent) {
                                range.selectNode(currLine);
                                range.deleteContents();
                            }

                            selection.setPosition(nextLine, 0);
                        }
                        /**
                         * DELETE at the 0 offset in a SPAN which is only wrapper in the line,
                         * Extract styles from the SPAN and merge it to the lines
                         * */
                        else if (endOffset===0 && isSpanAtLineStart && currLine.textContent.length===1) {
                            applySpanStyleToLine(range.startContainer, currLine);
                        }
                        /**
                         * DELETE at the prev line of a link
                         * Move the cursor to the start of the link
                         * */
                        else if (nextLine && isCursorAtLineEnd && isNextLineHasLink) {
                            e.preventDefault();

                            selection.setPosition(nextLine, 0);
                        }
                        /**
                         * DELETE at the link
                         * Cursor at the end of the line
                         * Move the cursor to the start of the next line
                         * */
                        else if (isCurrLineHasLink && nextLine && isCursorAtLineEnd) {
                            e.preventDefault();

                            selection.setPosition(nextLine, 0);
                        }
                    }
                    else if (selection.type === "Range") {
                        const rangeStartLine = findLineContainer(range.startContainer);
                        const rangeEndLine = findLineContainer(range.endContainer);
                        const rangeEndPrevLine = rangeEndLine.previousSibling;
                        const rangeStartNextLine = rangeStartLine.nextSibling;

                        const isRangeStartLineHasUniqueEl = checkLineContainsUniqueEl(rangeStartLine);
                        const isRangeEndLineHasUniqueEl = checkLineContainsUniqueEl(rangeEndLine);
                        const isRangeStartLineHasLink = checkLineContainsLinkEl(rangeStartLine);
                        const isRangeEndLineHasLink = checkLineContainsLinkEl(rangeEndLine);
                        const isRangeStartLineHasOnlyText = !isRangeStartLineHasUniqueEl && !isRangeStartLineHasLink
                        const isRangeEndLineHasOnlyText = !isRangeEndLineHasUniqueEl && !isRangeEndLineHasLink

                        const isRangeInline = rangeStartLine===rangeEndLine;
                        const isRangeStartAtLineStart = findRangeOffsetFromLine(range.startContainer, range.startOffset)===0;
                        const isRangeStartAtLineEnd = findRangeOffsetFromLine(range.startContainer, range.startOffset)===rangeStartLine.textContent.length;
                        const isRangeEndAtLineEnd = findRangeOffsetFromLine(range.endContainer, range.endOffset)===rangeEndLine.textContent.length;
                        const isSpanAtRangeStart = findTextWrapperNode(range.startContainer);

                        /**
                         * DELETE range start at a text line,
                         * Range end at the start of a unique content line,
                         * Move range end cursor to the end of prev line
                         * */
                        if (rangeEndPrevLine && rangeStartLine.textContent && range.endOffset===0 && isRangeEndLineHasUniqueEl) {
                            if (rangeStartLine.textContent.length!==0 && isRangeStartAtLineEnd) e.preventDefault();

                            range.setEndAfter(rangeEndPrevLine.lastChild);
                            range.collapse(true);
                        }
                        /**
                         * DELETE range start at a unique content line,
                         * Range end at the start of a unique content line,
                         * */
                        else if (rangeEndPrevLine && isRangeStartLineHasUniqueEl && range.endOffset===0 && isRangeEndLineHasUniqueEl) {
                            // No other selection between the start and the end of range
                            if (range.cloneContents().childNodes.length === 2) {
                                e.preventDefault();
                                range.collapse(true);
                            }
                            // Other lines exists between
                            else {
                                range.setEndAfter(rangeEndPrevLine.lastChild);
                            }
                        }
                        /**
                         * DELETE range start at the end of a unique content line,
                         * */
                        else if (range.startOffset===1 && isRangeStartLineHasUniqueEl && rangeStartNextLine.nextSibling) {
                            range.setStartBefore(rangeStartNextLine.firstChild);
                        }
                        /**
                         * DELETE range start at the offset 0 of a text line, has SPAN at the start of the line
                         * Range end in a text line
                         * */
                        else if (isRangeStartAtLineStart && !isRangeStartLineHasUniqueEl && isSpanAtRangeStart && !isRangeEndLineHasUniqueEl) {
                            rangeWork__deletingSpanAtInitLine(e, range, selection, rangeStartLine, rangeEndLine);
                        }
                        /**
                         * DELETE range start within a link,
                         * Multiple lines,
                         * Set range start at the start of the next line
                         * */
                        else if (rangeStartLine!==rangeEndLine && isRangeStartLineHasLink) {
                            range.setStartBefore(rangeStartLine.nextSibling.firstChild);
                        }
                        /**
                         * DELETE range end within a link,
                         * Multiple lines,
                         * Set range end at the end of the prev line
                         * */
                        else if (rangeStartLine!==rangeEndLine && isRangeEndLineHasLink) {
                            range.setEndAfter(rangeEndLine.previousSibling.lastChild);

                            if (isRangeStartAtLineEnd) {
                                e.preventDefault();
                                range.collapse(true);
                            }
                        }
                    }
                    break;


                case KEYBOARD_.KEY.TAB:
                    e.preventDefault();

                    if (selection.type==="Caret") {
                        if (!checkLineContainsUniqueEl(findLineContainer(range.startContainer))) {
                            insertText("    ");
                        }
                    }
                    else if (selection.type==="Range") {

                    }
                    break;


                case KEYBOARD_.KEY.INC:
                    e.preventDefault();
                    break


                case KEYBOARD_.KEY.ALT:
                case KEYBOARD_.KEY.ESC:
                    break;


                // ANY KEY
                default:
                    /**
                     * Menu toggle
                     * */
                    if (uniqueElControlMenuRef.current.checkVisibility()) {
                        /**
                         * Image max width toggle
                         * */
                        if ([
                            KEYBOARD_.KEY._1,
                            KEYBOARD_.KEY._2,
                            KEYBOARD_.KEY._3,
                            KEYBOARD_.KEY._4,
                            KEYBOARD_.KEY._5,
                            KEYBOARD_.KEY.UP,
                            KEYBOARD_.KEY.DOWN,
                        ].includes(keyCode)) {
                            e.preventDefault();

                            switch (keyCode) {
                                case KEYBOARD_.KEY._1:
                                    uniqueElControlMenuRef.current.toggleImgMaxWidth(0, "NUM1");
                                    break;
                                case KEYBOARD_.KEY._2:
                                    uniqueElControlMenuRef.current.toggleImgMaxWidth(1, "NUM2");
                                    break;
                                case KEYBOARD_.KEY._3:
                                    uniqueElControlMenuRef.current.toggleImgMaxWidth(2, "NUM3");
                                    break;
                                case KEYBOARD_.KEY._4:
                                    uniqueElControlMenuRef.current.toggleImgMaxWidth(3, "NUM4");
                                    break;
                                case KEYBOARD_.KEY._5:
                                    uniqueElControlMenuRef.current.toggleImgMaxWidth(4, "NUM5");
                                    break;
                                case KEYBOARD_.KEY.UP:
                                    uniqueElControlMenuRef.current.toggleUp("UP");
                                    break;
                                case KEYBOARD_.KEY.DOWN:
                                    uniqueElControlMenuRef.current.toggleDown("DOWN");
                                    break;
                            }
                            // Stop propagation
                            return;
                        }
                    }


                    if (selection.type === "Caret") {
                        const currLine = findLineContainer(range.endContainer);
                        const nextLine = currLine.nextSibling;
                        const isLineHasUniqueEl = checkLineContainsUniqueEl(currLine);
                        const cursorOffset = range.endOffset;

                        const isCursorAtLineStart = findCursorOffsetFromLine(selection)===0;
                        const isCursorAtLineEnd = findCursorOffsetFromLine(selection)===currLine.textContent.length;
                        const isSpanAtLineStart = findTextWrapperNode(range.startContainer);
                        const isCurrLineHasUniqueEl = checkLineContainsUniqueEl(currLine);
                        const isNextLineHasUniqueEl = checkLineContainsUniqueEl(nextLine);
                        const isCurrLineHasLink = checkLineContainsLinkEl(currLine);
                        const includeCursorMoveKeys = KEYBOARD_.CURSOR_MOVE_KEYS.includes(keyCode);



                        /**
                         * Cursor at the offset 0 of a text node,
                         * Replacing the whole line to prevent auto-generating SPAN tag at the start of the line
                         * */
                        if (isCursorAtLineStart && !isSpanAtLineStart && !isCurrLineHasLink && !isLineHasUniqueEl && !includeCursorMoveKeys) {
                            const currLine = findLineContainer(range.startContainer);
                            const clone = currLine.cloneNode(true);

                            range.selectNode(currLine);
                            range.deleteContents();

                            range.insertNode(clone);
                            selection.setPosition(clone, 0);
                        }
                        /**
                         * Cursor before a unique content
                         * */
                        else if (cursorOffset === 0 && isLineHasUniqueEl && !includeCursorMoveKeys) {
                            const newLine = document.createElement("p");

                            newLine.appendChild(document.createElement("br"));
                            range.setStartBefore(currLine);
                            range.insertNode(newLine);
                            selection.setPosition(newLine.firstChild, 0);
                        }
                        /**
                         * Cursor after a unique content
                         * */
                        else if (cursorOffset === 1 && isLineHasUniqueEl && !includeCursorMoveKeys) {
                            const newLine = document.createElement("p");

                            newLine.appendChild(document.createElement("br"));
                            range.setStartAfter(currLine);
                            range.insertNode(newLine);
                            selection.setPosition(newLine.firstChild, 0);
                        }
                        /**
                         * Cursor at the start of the line
                         * Line has link
                         * Create a new line before the current line
                         * */
                        else if (isCursorAtLineStart && isCurrLineHasLink && !includeCursorMoveKeys) {
                            const newLine = document.createElement("p");

                            newLine.appendChild(document.createElement("br"));
                            range.setStartBefore(currLine);
                            range.insertNode(newLine);
                            selection.setPosition(newLine.firstChild, 0);
                        }
                        /**
                         * Cursor at the end of the line
                         * Line has link
                         * Create a new line after the current line
                         * */
                        else if (isCursorAtLineEnd && isCurrLineHasLink && !includeCursorMoveKeys) {
                            const newLine = document.createElement("p");

                            newLine.appendChild(document.createElement("br"));
                            range.setStartAfter(currLine);
                            range.insertNode(newLine);
                            selection.setPosition(newLine.firstChild, 0);
                        }
                    }
                    else if (selection.type === "Range") {
                        const rangeStartLine = findLineContainer(range.startContainer);
                        const rangeEndLine = findLineContainer(range.endContainer);
                        const startOffset = range.startOffset;
                        const endOffset = range.endOffset;
                        const excludeFuncArrowKeys = !KEYBOARD_.FUNC_KEYS.includes(keyCode) && !KEYBOARD_.CURSOR_MOVE_KEYS.includes(keyCode)

                        const isRangeStartLineHasUniqueEl = checkLineContainsUniqueEl(rangeStartLine);
                        const isRangeEndLineHasUniqueEl = checkLineContainsUniqueEl(rangeEndLine);
                        const isRangeInline = rangeStartLine===rangeEndLine;
                        const isRangeStartAtLineStart = findRangeOffsetFromLine(range.startContainer, range.startOffset)===0;
                        const isRangeEndAtLineEnd = findRangeOffsetFromLine(range.endContainer, range.endOffset)===rangeEndLine.textContent.length;
                        const spanAtRangeStart = findTextWrapperNode(range.startContainer);
                        const spanAtRangeEnd = findTextWrapperNode(range.endContainer);
                        const isCursorAtLineStart = findCursorOffsetFromLine(selection)===0;



                        /**
                         * Move the range out of A tag
                         * */
                        if (!KEYBOARD_.CURSOR_MOVE_KEYS.includes(e.keyCode) && findLineContainer(range.startContainer) !== findLineContainer(range.endContainer)) {
                            const checkLinkTagInNode = (node) => {
                                return (node.nodeName==="#text" && node.parentNode.nodeName==="A") ||
                                    (node.nodeName==="A") ||
                                    (node.nodeName==="P" && [...node.children].some(_node => _node.nodeName==="A"));
                            }


                            if (checkLinkTagInNode(range.startContainer)) {
                                const currLine = findLineContainer(range.startContainer);

                                range.setStartBefore(currLine.nextSibling.firstChild);
                            }
                            else if (checkLinkTagInNode(range.endContainer)) {
                                const currLine = findLineContainer(range.endContainer);

                                range.setEndAfter(currLine.previousSibling.lastChild);
                            }
                        }



                        /**
                         * Range start at the offset 0 of a text node,
                         * SPAN at the range start, range end is not in the same SPAN,
                         * Range within a line
                         * */
                        if (isRangeInline && isCursorAtLineStart && spanAtRangeStart && spanAtRangeStart!==spanAtRangeEnd && !isRangeStartLineHasUniqueEl && excludeFuncArrowKeys) {
                            range.deleteContents();

                            // SPAN at the range start
                            const span1 = findLineStartSpan(rangeEndLine);

                            if (span1 && !span1.textContent) {
                                range.selectNode(span1);
                                range.deleteContents();
                            }

                            // SPAN at the range end
                            const span2 = findLineStartSpan(rangeEndLine);

                            if (span2 && !span2.textContent) {
                                range.selectNode(span2);
                                range.deleteContents();
                            }

                            selection.setPosition(rangeEndLine, 0);
                        }
                        /**
                         * Range start at the offset 0 of a text node,
                         * SPAN at the range start,
                         * Range end within a text line,
                         * Range in multiple lines
                         * */
                        else if (!isRangeInline && isCursorAtLineStart && spanAtRangeStart && !isRangeStartLineHasUniqueEl && !isRangeEndLineHasUniqueEl && excludeFuncArrowKeys) {
                            range.deleteContents();

                            range.selectNode(rangeStartLine);
                            range.deleteContents();

                            const span = findLineStartSpan(rangeEndLine);

                            // Delete the span if it is empty(due to the range end at the end of the SPAN)
                            if (span && !span.textContent) {
                                range.selectNode(span);
                                range.deleteContents();
                            }

                            selection.setPosition(rangeEndLine, 0);
                        }
                        /**
                         * Range is not within a line,
                         * Range start is not in a unique content line,
                         * Range end at the start of a unique content line,
                         * */
                        else if (!isRangeInline && endOffset===0 && !isRangeStartLineHasUniqueEl && isRangeEndLineHasUniqueEl && excludeFuncArrowKeys) {

                            if (startOffset === 0) {
                                range.deleteContents();

                                const span = rangeStartLine.hasChildNodes()? [...rangeStartLine.childNodes.values()].find(node => node.nodeName === "SPAN"): null;

                                // Apply start line first el(if span) style to the line style
                                if (span) {
                                    Object.entries(STYLE_.DEFAULT_OPTIONS).forEach(entry => {
                                        const attr = entry[0];
                                        const defValue = entry[1];
                                        const style = span.style[attr];

                                        if (style) rangeStartLine.style[attr] = style === defValue? null: style;
                                    })

                                    rangeStartLine.removeChild(span);
                                }

                                const br = document.createElement("br");
                                rangeStartLine.appendChild(br);
                                range.setStartBefore(br);
                                range.setEndBefore(br);
                            } else {
                                range.setEndAfter(rangeEndLine.previousSibling.lastChild);
                            }
                        }
                        /**
                         * Range start is after a unique content,
                         * Range end is before a unique content,
                         * Add a new line between
                         * */
                        else if (endOffset===0 && startOffset===1 && isRangeStartLineHasUniqueEl && isRangeEndLineHasUniqueEl && excludeFuncArrowKeys) {
                            const newLine = document.createElement("p");
                            newLine.appendChild(document.createElement("br"));

                            range.deleteContents();

                            range.setStartAfter(rangeStartLine.lastChild)
                            range.setEndAfter(rangeStartLine.lastChild)

                            insertHTML(newLine);
                            selection.setPosition(newLine.firstChild, 0)
                        }
                        /**
                         * Cursor focus at the end of a unique content,
                         * Add a new line after,
                         * Set cursor at the start of the next line if it is a text line
                         * */
                        else if (startOffset===1 && isRangeStartLineHasUniqueEl && excludeFuncArrowKeys) {
                            if (endOffset!==0 && rangeEndLine.textContent.length===endOffset) {
                                const br = document.createElement("br");

                                range.deleteContents();
                                rangeEndLine.appendChild(br);
                                range.setStartBefore(br);
                                range.setEndBefore(br);
                            } else {
                                range.deleteContents();
                            }
                        }
                    }
                    break;
            }


            /** ANY KEY */
            uniqueElControlMenuRef.current.hide();
        }


    }

    // ON KEY UP {
    const onKeyUp = () => {
        uniqueElControlMenuRef.current.resetKeyPress();
        topMenuToggleBarRef.current.updateState();
        uniqueElToggleKeyRef.current.updateState();
    }



    /**
     * MOUSE EVENT
     * */
    // ON MOUSE DOWN
    const onMouseDown = (e) => {
            /**
             * Point target is a IMG,
             * Select target and show the control menu
             * */
            if (e.target.nodeName==="IMG") {
                e.preventDefault();
                e.stopPropagation();

                const uniqueEl = e.target;
                const line = findLineContainer(uniqueEl);

                const selection = document.getSelection();
                const newRange = document.createRange();

                newRange.setStart(line, 0);
                newRange.setEnd(line, 0);

                selection.removeAllRanges();
                selection.addRange(newRange);

                uniqueElToggleKeyRef.current.hide();
                uniqueElControlMenuRef.current.show();
            } else {
                uniqueElControlMenuRef.current.hide();
            }
        }


    // ON MOUSE UP
    const onMouseUp = () => {
        topMenuToggleBarRef.current.updateState();
        uniqueElToggleKeyRef.current.updateState();
    }



    // ON PASTE
    const onPaste = (e) => {
        /**
         * Copy text from clipboard to prevent copying html tags
         * */
        e.preventDefault();


        let clipboardText = e.clipboardData.getData("text/plain");
        const textList = []
        let brWasThere = undefined;

        // Parse \r\n \n in the text into <br>
        clipboardText.split("\n").forEach(text => {
            const p = document.createElement("p");

            if (/[\S]/.test(text)) {
                p.appendChild(document.createTextNode(text.replace("\r", "")));

                textList.push(p.outerHTML);
                brWasThere = false;
            } else {
                if (brWasThere === undefined || brWasThere === true) {
                    p.appendChild(document.createElement("br"));

                    textList.push(p.outerHTML);
                    brWasThere = false;
                } else {
                    brWasThere = true;
                }
            }
        });

        const normalizedText = textList.join("")


        // Avoid paste within a unique content containing range
        const selection = document.getSelection();
        const range = selection.getRangeAt(0);

        const rangeStartLine = findLineContainer(range.startContainer);
        const rangeEndLine = findLineContainer(range.endContainer);

        const isRangeStartLineHasUniqueEl = checkLineContainsUniqueEl(rangeStartLine);
        const isRangeEndLineHasUniqueEl = checkLineContainsUniqueEl(rangeEndLine);

        const isRangeStartLineHasLink = checkLineContainsLinkEl(rangeStartLine);
        const isRangeEndLineHasLink = checkLineContainsLinkEl(rangeEndLine);

        /**
         * Range within a line
         * */
        if (rangeStartLine===rangeEndLine && isRangeStartLineHasLink) {
            const newLine = document.createElement("p").appendChild(document.createElement("br"));

            range.setStartAfter(rangeEndLine);
            range.setEndAfter(rangeEndLine);

            range.insertNode(newLine);
            selection.setPosition(newLine, 0);
        }
        if (rangeStartLine!==rangeEndLine && isRangeStartLineHasLink) {
            range.setStartBefore(rangeStartLine.nextSibling.firstChild);
        }
        if (rangeStartLine!==rangeEndLine && isRangeEndLineHasLink) {
            range.setEndAfter(rangeEndLine.previousSibling.lastChild);
        }


        /**
         * Handle unique el
         * */
        if (range.startOffset===1 && isRangeStartLineHasUniqueEl) {
            const newLine = document.createElement("p");
            newLine.appendChild(document.createElement("br"));

            insertHTML(newLine);
            insertHTML(normalizedText);
        }
        else if (range.endOffset===0 && isRangeEndLineHasUniqueEl) {
            const newLine = document.createElement("p");
            newLine.appendChild(document.createElement("br"));


            insertHTML(normalizedText);

            range.setStartAfter(selection.focusNode);
            insertHTML(newLine)
        } else {
            insertHTML(normalizedText);
        }


    }


    // ON DRAG START
    const onDragStart = (e) => {
        console.log(document.getSelection());
        e.preventDefault();
    }

    // ON DROP
    const onDrop = (e) => {
        e.preventDefault();
        console.log(document.getSelection());
    }


    // ON INPUT
    const onInput = async () => {
        await updatePostContentStateAndTempPostData();
    }






    useEffect(() => {
        dispatch(setEditorWritingBoard(writingBoardRef));

        if (checkEmptyEditor(writingBoardRef)) editorPlaceholderRef.current.show();
    }, [writingBoardRef.current])


    useEffect(() => {
        /**
         * Set temp-saved post content to the editor if exists
         * */
        if (!isEditMode && postState.temp.content) {
            /**
             * Check if temp saved content is empty text data
             * */
            const tester = document.createElement("div");

            tester.innerHTML = postState.temp.content;

            const content = [...tester.children];

            if (content.length===1 && !checkLineContainsUniqueEl(content[0]) && !content[0].textContent) return;

            writingBoardRef.current.innerHTML = postState.temp.content;

            if (!checkEmptyEditor(writingBoardRef)) editorPlaceholderRef.current.hide();
        }
    }, [postState.temp.content])


    /**
     * Fill editor with content state when edit mode
     * */
    useEffect(() => {
        if (isEditMode) {
            editorPlaceholderRef.current.hide();
            writingBoardRef.current.innerHTML = postState.content;
        }
    }, [editMode])




    useImperativeHandle(ref, ()=>({
        updateTempPostData: async (delay) => {
            return await updatePostContentStateAndTempPostData(delay);
        }
    }))




    return (
        <div className={style["container"]}>

            {/** EDITOR TOGGLE BUTTONS */}
            <section className={style["menus"]}>
                <ToggleBar ref={topMenuToggleBarRef} writingBoardRef={writingBoardRef} updatePostContentFunc={updatePostContentStateAndTempPostData}/>
            </section>


            {/** EDITOR MAIN WRITING BOARD */}
            <section className={style["editor"]} ref={editorSectionRef}>
                <div
                    className={style["writing-board"]}
                    ref={writingBoardRef}

                    contentEditable={true}
                    suppressContentEditableWarning={true}
                    spellCheck={false}

                    onInput={onInput}

                    onKeyDown={onKeyDown}
                    onKeyUp={onKeyUp}

                    onPaste={onPaste}
                    onFocus={onFocus}
                    onBlur={onBlur}

                    onMouseUp={onMouseUp}
                    onMouseDown={onMouseDown}

                    onDragStart={onDragStart}
                    onDrop={onDrop}>
                    <p><br/></p>
                </div>

                {/** EDITING MENU BAR */}
                {/*<InlineElControlMenu/>*/}
                <UniqueElControlMenu editorSection={editorSectionRef.current} updatePostContentFunc={updatePostContentStateAndTempPostData} ref={uniqueElControlMenuRef}/>
                <UniqueElToggleKeys editorRef={writingBoardRef} uniqueElControllerRef={uniqueElControlMenuRef} ref={uniqueElToggleKeyRef}/>
                <EditorPlaceholder ref={editorPlaceholderRef}/>
                <ClearPostAndUpdateTimeSection clearEditorContentFunc={clearWritingBoard}/>
            </section>

        </div>
    )
})

export default Editor;