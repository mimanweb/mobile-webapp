import React from "react";
import style from "./notificationPanel.module.scss";
import RightSidebarUserNotificationArticle from "./article";
import {useDispatch, useSelector} from "react-redux";
import HorizontalTextPageNavigator, {PAGE_NAVIGATOR_THEME} from "../../../pageNavigator/pageNavigator";
import {NOTIFICATION_ITEMS_PER_PAGE, NOTIFICATION_PAGE_PER_NAVIGATOR} from "../../../../utils/index/user";
import {
    requestSetAllUserNotificationsChecked,
    requestUserNotificationList,
    setUserNotificationListState,
    setUserUnreadNotificationState
} from "../../../../features/user/actions";
import router from "next/router";
import _ from "underscore";




export default function RightSidebarUserNotificationPanel({closeMenuPanelFunc}) {
    const dispatch = useDispatch();

    const userState = useSelector(state => state.user);
    const {notificationList, notificationPage, notificationTotalPageCount} = userState;



    const pageNavigate = async (pageToSearch) => {
        const {status, data} = await requestUserNotificationList(pageToSearch, NOTIFICATION_ITEMS_PER_PAGE)

        switch (status) {
            case 200:
                dispatch(setUserUnreadNotificationState(false));
                dispatch(setUserNotificationListState(data.notificationList, data.totalPageCount, pageToSearch));
                break;
            case 403:
                router.reload();
                break;
        }
    }



    const currPageIndex = Math.ceil(notificationPage/NOTIFICATION_PAGE_PER_NAVIGATOR);
    const pageStartOfCurrPageIndex = (currPageIndex-1)*NOTIFICATION_PAGE_PER_NAVIGATOR;
    const pageEndOfCurrPageIndex = currPageIndex*NOTIFICATION_PAGE_PER_NAVIGATOR;




    const clickAllChecked = async () => {
        const {status} = await requestSetAllUserNotificationsChecked();

        switch (status) {
            case 200:
                const {status: _status, data} = await requestUserNotificationList(1, NOTIFICATION_ITEMS_PER_PAGE)

                switch (_status) {
                    case 200:
                        dispatch(setUserUnreadNotificationState(false));
                        dispatch(setUserNotificationListState(data.notificationList, data.totalPageCount, 1));
                        break;
                    case 403:
                        router.reload();
                        break;
                }
                break;
            case 403:
                router.reload();
                break;
        }
    }


    return (
        <div className={style["container"]}>
            {/** SECTION */}
            {/** CHECK ALL LIST AS READ BUTTON */}
            <section className={style["menu"]}>
                <label className={style["check_all_list_button"]} onClick={clickAllChecked}>모두 읽음</label>
            </section>


            {/** NOTIFICATION LIST SECTION */}
            <section className={style["list"]}>
                {_.isEmpty(notificationList) && (
                    <div className={style["no_items_message"]}>알림 없음</div>
                )}

                {notificationList.map((notification, key) => {
                    return (
                        <RightSidebarUserNotificationArticle key={key} data={notification} closeMenuPanelFunc={closeMenuPanelFunc}/>
                    )
                })}
            </section>


            <section className={style["page_nav"]}>
                <HorizontalTextPageNavigator
                    isLinkMode={false}
                    currPage={notificationPage}
                    totalPageCount={notificationTotalPageCount}
                    pagePerPageGroupCount={NOTIFICATION_PAGE_PER_NAVIGATOR}
                    toFirstPageFunc={()=>pageNavigate(1)}
                    toPrevPageFunc={()=>pageNavigate(pageStartOfCurrPageIndex)}
                    toPageFunc={(p)=>pageNavigate(p)}
                    toNextPageFunc={()=>pageNavigate(pageEndOfCurrPageIndex+1)}
                    theme={PAGE_NAVIGATOR_THEME.DARK}
                />
            </section>
        </div>
    )
}