import React from "react";
import style from "./article.module.scss";
import timeUtil from "../../../../utils/time";
import {NOTIFICATION_TYPE} from "../../../../utils/index/notification";
import Link from "next/link";
import {requestSetUserNotificationChecked} from "../../../../features/user/actions";


export default function RightSidebarUserNotificationArticle({data, closeMenuPanelFunc}) {

    let {type, time} = data;
    let text = "";
    let link = "/";

    switch (type) {
        case NOTIFICATION_TYPE.POST.COMMENT:
        case NOTIFICATION_TYPE.COMMENT.COMMENT:
            const {comment, post} = data;

            time = time.created;

            if (comment.isDeleted) {
                text = "--삭제된 댓글--";
            }
            else if (comment.content.text) {
                text = comment.content.text;
            }
            else {
                if (comment.content.youtubeHTML) text = "--유튜브 댓글--";
                else if (comment.content.imageId) text = "--이미지 댓글--";
            }
            
            
            link = {
                pathname: `/post/${post.index.category}/${post.index.board}/${post.number}`,
                query: {
                    comment: comment.number
                }
            };
            break;
    }



    let iconType = "";
    let typeText = "";

    switch (type) {
        case NOTIFICATION_TYPE.POST.COMMENT:
            typeText = "글 댓글";
            iconType = "post_comment";
            break;
        case NOTIFICATION_TYPE.COMMENT.COMMENT:
            typeText = "댓글 답변";
            iconType = "replied_comment";
            break;
    }



    const linkClick = async () => {
        const {status} = await requestSetUserNotificationChecked(data._id);

        closeMenuPanelFunc();
    }



    return (
        <div className={`${style["container"]} ${data.isChecked? style["checked"]: ""}`}>
            <div className={style["content"]}>
                {type && (
                    <label>
                        <div className={`${style["icon"]} ${style[iconType]}`}/>
                        <span>{typeText}</span>
                    </label>
                )}


                <Link href={link}>
                    <a onClick={linkClick}>{text}</a>
                </Link>
            </div>


            <p className={style["time"]}>{timeUtil.shortFormatText(time)}</p>
        </div>
    )
}