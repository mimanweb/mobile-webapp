import React from "react";
import style from "./board.module.scss";
import Link from "next/link";
import _ from "underscore";
import {useSelector, useDispatch} from "react-redux";
import HorizontalTextPageNavigator from "../pageNavigator/pageNavigator";
import {POST_PAGE_PER_PAGE_GROUP_COUNT, BOARD_LIST_TYPE} from "../../utils/index/board";
import {useRouter} from "next/router";
import {SiteIndex} from "../../utils/index/siteIndex";
import {USER_TYPE} from "../../utils/index/user";
import SmallButton, {SmallButtonType} from "../../components/button/small";
import PostArticle from "./modules/postArticle";





const Tab = ({boardListType}) => {
    const boardState = useSelector(state => state.board);
    const {category, board, listType} = boardState;

    let text;
    let link = {
        pathname: `/board/${category}/${board}`
    };
    let isCurrPostType = boardListType===listType;

    switch (boardListType) {
        case BOARD_LIST_TYPE.ALL:
            text = "전체글";
            break;
        case BOARD_LIST_TYPE.HOT:
            text = "인기글";
            link.query = {"list": BOARD_LIST_TYPE.HOT};
            break;
        case BOARD_LIST_TYPE.NOTICE:
            text = "공지";
            link.query = {"list": BOARD_LIST_TYPE.NOTICE};
            break;
    }

    return (
        <Link href={link}>
            <a className={isCurrPostType? style["fill"]: null}>{text}</a>
        </Link>
    )
}







/**
 * Board
 * */
const Board = ({category, board}) => {
    const router = useRouter();

    const boardState = useSelector(state => state.board);
    const userState = useSelector(state => state.user);

    const {type: userType} = userState;
    const {postList, noticePostList, currPage, totalPageCount, postPerPage, listType, hotPostList_24h} = boardState;

    const currPageGroup = Math.ceil(currPage/POST_PAGE_PER_PAGE_GROUP_COUNT);
    const pageStartOfCurrPageGroup = (currPageGroup-1)*POST_PAGE_PER_PAGE_GROUP_COUNT;
    const pageEndOfCurrPageGroup = (currPageGroup)*POST_PAGE_PER_PAGE_GROUP_COUNT;


    const isAdminUser = userType===USER_TYPE.ADMIN;
    const atCommunityNoticeBoard = category===SiteIndex.Community.code && board===SiteIndex.Community.boards.Notice.code;


    const currBoardLinkPath = `/board/${category}/${board}`
    let boardListQuery = {};


    switch (listType) {
        case BOARD_LIST_TYPE.HOT:
            boardListQuery = {list: BOARD_LIST_TYPE.HOT};
            break;
        case BOARD_LIST_TYPE.NOTICE:
            boardListQuery = {list: BOARD_LIST_TYPE.NOTICE};
            break;
    }


    const isBoardEmpty = _.isEmpty(postList);



    const onPostWriteButtonClick = async () => {
        await router.push(`/post/write/${category}/${board}`);
    }




    return (
        <div className={style["container"]}>


            {!_.isEmpty(hotPostList_24h) && (
                <section className={style["hot_post_list"]}>
                    {hotPostList_24h.map((post, key) => {
                        return (
                            <PostArticle key={key} postParams={post} skipSecondRow={true} isHotPost={true} narrowPadding={true} cleanTopBottomBorder={true}/>
                        )
                    })}
                </section>
            )}



            {!atCommunityNoticeBoard && (
                <section className={style["tab"]}>
                    <Tab boardListType={BOARD_LIST_TYPE.ALL}/>
                    <Tab boardListType={BOARD_LIST_TYPE.HOT}/>
                    <Tab boardListType={BOARD_LIST_TYPE.NOTICE}/>
                </section>
            )}



            <section className={style["list"]}>

                {!_.isEmpty(noticePostList) && (
                    <>
                        {/** NOTICE LIST */}
                        <section className={style["notice_list"]}>
                            {noticePostList.map((post, key) => {
                                return (
                                    <PostArticle key={key} postParams={post} inNoticeList={true} narrowPadding={true}/>
                                )
                            })}
                        </section>

                        {/** GAP */}
                        <div className={style["gap"]}/>
                    </>
                )}




                {/** POST LIST */}
                <section className={style["post_list"]}>
                    {isBoardEmpty? (
                        <div className={style["empty_board_message"]}>
                            <span>{
                                listType===BOARD_LIST_TYPE.HOT? "아직 인기글이 없습니다.":
                                    listType===BOARD_LIST_TYPE.NOTICE? "공지글이 없습니다.":
                                        "게시글이 없습니다, 첫 게시글을 작성해 주십시오."
                            }</span>
                        </div>
                    ): (
                        postList.map((post, key) => {
                            return (
                                <PostArticle key={key} postParams={post} showUpvoteCount={true}/>
                            )
                        })
                    )}
                </section>

            </section>



            <section className={style["ad"]}>

            </section>



            {/** PAGE NAVIGATION */}
            <section className={style["page_nav_post_write_button"]}>
                <HorizontalTextPageNavigator
                    isLinkMode={true}
                    currPage={currPage}
                    totalPageCount={totalPageCount}
                    pagePerPageGroupCount={POST_PAGE_PER_PAGE_GROUP_COUNT}
                    toFirstPageLinkPath={{
                        pathname: currBoardLinkPath,
                        query: {...{page: 1}}
                    }}
                    toPrevPageLinkPath={{
                        pathname: currBoardLinkPath,
                        query: {...{page: pageStartOfCurrPageGroup}}
                    }}
                    toPageLinkPath={currBoardLinkPath}
                    toNextPageLinkPath={{
                        pathname: currBoardLinkPath,
                        query: {...{page: pageEndOfCurrPageGroup+1}}
                    }}
                    additionalLinkQuery={boardListQuery}/>

                {(
                    !atCommunityNoticeBoard ||
                    (atCommunityNoticeBoard && isAdminUser)
                )
                && (
                    <SmallButton buttonType={SmallButtonType.WRITE_POST} onClick={onPostWriteButtonClick}/>
                )}
            </section>


        </div>
    )
};


export default Board;