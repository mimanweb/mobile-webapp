import {useSelector} from "react-redux";
import POST_ from "../../../utils/index/post";
import style from "./postArticle.module.scss";
import {BOARD_LIST_TYPE} from "../../../utils/index/board";
import Link from "next/link";
import _ from "underscore";
import {parseFullUserInfo} from "../../../utils/format";
import timeUtil from "../../../utils/time";



const PostTypeIcon = ({type}) => {
    switch (type) {
        case POST_.TYPE.NOTICE:
            return (
                <div className={`${style["post_type_icon"]} ${style["notice"]}`}>
                    <span>공지</span>
                </div>
            )
        case POST_.TYPE.HOT:
            return (
                <div className={`${style["post_type_icon"]} ${style["hot"]}`}>
                    <span>인기</span>
                </div>
            )
    }
}



const PostArticle = ({postParams, inNoticeList, skipSecondRow, isHotPost, showUpvoteCount, narrowPadding, cleanTopBottomBorder}) => {

    const postState = useSelector(state => state.post);
    const boardState = useSelector(state => state.board);


    const {number: postPagePostNumber} = postState;
    const {number: postNumber, owner, index: postIndex, title, time, record, images, isMyPost, type: postType} = postParams;
    const {upvoteCount, commentCount, viewCount} = record;
    const {userId, nickname, ipAddress} = owner || {};

    const isRegUserPost = userId;
    const isPostPagePost = postNumber===postPagePostNumber;
    const isNoticePost = postType===POST_.TYPE.NOTICE;



    const upvote_icon_style = () => {
        const styles = []

        if (upvoteCount===1) {
            styles.push(style["lv1"]);
        }
        else if (upvoteCount>=2 && upvoteCount<=3) {
            styles.push(style["lv2"]);
        }
        else if (upvoteCount>=4 && upvoteCount<=5) {
            styles.push(style["lv3"]);
        }
        else if (upvoteCount>=6 && upvoteCount<=7) {
            styles.push(style["lv4"]);
        }
        else if (upvoteCount>=8 && upvoteCount<=10) {
            styles.push(style["lv5"]);
        }
        else if (upvoteCount>=11) {
            styles.push(style["lv6"]);
        }


        return styles.join(" ");
    }


    const postLink = {
        pathname: `/post/${postIndex.category}/${postIndex.board}/${postNumber}`,
    }

    if (BOARD_LIST_TYPE.ALL!==boardState.listType) postLink.query = {list: boardState.listType};




    return (
        <Link href={postLink}>
            <a className={`${style["container"]} ${isPostPagePost? style["highlight"]: ""} ${narrowPadding? style["narrow"]: ""} ${cleanTopBottomBorder? style["clean_top_bottom_border"]: ""} ${inNoticeList? style["background_blend"]: ""}`}>

                <section className={style["first_row"]}>
                    {(
                        showUpvoteCount &&
                        !inNoticeList &&
                        upvoteCount>0
                    ) && (
                        <div className={`${style["upvote_count_icon"]} ${upvote_icon_style()}`}>
                            {upvoteCount<100? upvoteCount: "99+"}
                        </div>
                    )}


                    {inNoticeList && (
                        <PostTypeIcon type={POST_.TYPE.NOTICE}/>
                    )}


                    {isHotPost && (
                        <PostTypeIcon type={POST_.TYPE.HOT}/>
                    )}


                    {postType && (
                        <PostTypeIcon type={postType}/>
                    )}


                    <div className={style["post_title"]}>
                        {title}
                    </div>

                    {/** POST CONTAINS IMAGE ICON */}
                    {
                        !_.isEmpty(images)
                        && (<div className={style["image_icon"]}/>)
                    }

                    {/** POST COMMENT COUNT ICON */}
                    {(
                        !inNoticeList &&
                        commentCount>0
                    )
                        && (<label className={style["comment_count"]}>{commentCount}</label>)
                    }
                </section>


                {(
                    !inNoticeList &&
                    !skipSecondRow
                )
                && (
                    <section className={style["second_row"]}>
                        <div className={style["nickname"]}>
                            <p title={parseFullUserInfo(userId, nickname, ipAddress)}>{nickname}</p>
                        </div>

                        <div className={style["post_views"]}>
                            <p title={`조회수: ${viewCount}`}>{viewCount}</p>
                        </div>

                        <div className={style["divider"]}/>

                        <div className={style["post_time"]}>
                            <p title={timeUtil.fullFormatText(time.published)}>{timeUtil.shortFormatText(time.published)}</p>
                        </div>
                    </section>
                )}

            </a>
        </Link>
    )
}


export default PostArticle