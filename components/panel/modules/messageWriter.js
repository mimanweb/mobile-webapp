import React, {useState, useEffect, useImperativeHandle, forwardRef, useRef} from "react";
import style from "./styles/messageWriter.module.scss"



const typeDelay = (ms) => new Promise(resolve => setTimeout(resolve, ms));


const textDeleteMs = 200
const textWriteMs = 300


const MessageWriter = forwardRef(({printLeftToRight}, ref) => {
    const [text, setText] = useState("");
    const [cursorBlink, setCursorBlink] = useState(false);



    const deleteText = async () => {
        const ms = textDeleteMs/text.length;

        setCursorBlink(false);

        for (let i = 1; i <= text.length; i++) {
            if (printLeftToRight) setText(text.substr(0, text.length-i));
            else setText(text.substr(i))
            await typeDelay(ms)
        }

        setCursorBlink(true);
    }


    const writeText = async (text) => {
        const ms = textWriteMs/text.length;

        setCursorBlink(false);

        for (let i = 1; i <= text.length; i++) {
            if (printLeftToRight) setText(text.substr(0, i));
            else setText(text.substr(-i))
            await typeDelay(ms)
        }

        setCursorBlink(true);
    }


    useImperativeHandle(ref, () => ({
        clear: async () => {
            await deleteText();
        },
        write: async (newText) => {
            if (text) await deleteText();

            await writeText(newText);
        }
    }))
    return (
        <div className={style["container"]}>
            {printLeftToRight && (<p>{text}</p>)}
            <div className={`${style["cursor"]} ${cursorBlink && style["blink"]}`}/>
            {!printLeftToRight && (<p>{text}</p>)}
        </div>
    )
})


export default MessageWriter;