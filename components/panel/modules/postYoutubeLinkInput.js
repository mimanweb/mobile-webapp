import React, {useState, useRef, useEffect} from "react";
import style from "./styles/postYoutubeLinkInput.module.scss";
import {StretchOpenBoardType} from "../index";
import MessageWriter from "./messageWriter";
import {findLineContainer, selectEditorFirstLine, checkEditorHasFocus} from "../../../utils/editor/commonUtils";


const IFRAME_WIDTH = "420";
const IFRAME_HEIGHT = "248";



const PostYoutubeLinkInput = ({editorRef, isBoardOpened, closeBoardFunc, updatePostContentFunc, setAdditionalBoardTypeFunc}) => {
    const textareaRef = useRef(null);
    const messageWriter = useRef(null);
    const mediaRef = useRef(null);

    const [showMessage, setShowMessage] = useState(false);
    const [iframe, setIframe] = useState(null);



    const openMessage = (text) => {
        setAdditionalBoardTypeFunc(StretchOpenBoardType.POST_YOUTUBE_LINK_INPUT_WITH_ERROR_MESSAGE);
        messageWriter.current.write(text);
        setShowMessage(true);
    }

    const closeMessage = () => {
        setAdditionalBoardTypeFunc(StretchOpenBoardType.POST_YOUTUBE_LINK_INPUT);
        messageWriter.current.clear();
        setShowMessage(false);
    }

    const clearContent = () => {
        mediaRef.current.innerHTML = "";
        textareaRef.current.value = "";
        setIframe(null);
    }


    const onInput = () => {
        closeMessage();

        mediaRef.current.innerHTML = "";
        setIframe(null);

        const link = textareaRef.current.value;

        if (!link) return;

        const testEl = document.createElement("div");

        testEl.innerHTML = link;

        const children = testEl.children;

        /**
         * When iframe html is given,
         * Validate the link
         * */
        if (!testEl.textContent && children.length===1 && children[0].nodeName==="IFRAME") {
            const iframe = children[0];

            // Attributes and src url validation
            if (Number(iframe.width)===0 || Number(iframe.height)===0 || iframe.src.substr(0, "https://www.youtube.com/".length)!=="https://www.youtube.com/") {
                openMessage("잘못된 YouTube 링크");
                return;
            }

            iframe.width = IFRAME_WIDTH;
            iframe.height = IFRAME_HEIGHT;

            setIframe(iframe);
            mediaRef.current.innerHTML = iframe.outerHTML;
        }
        /**
         * Any text is given,
         * Validate if it is a youtube link
         * */
        else {
            const stripLink = link.replace(" ", "");

            // Find suffix
            const domainMatch = /https:\/\/www\.youtube\.com\/watch\?v=([\S]+)/g.exec(stripLink) || /https:\/\/youtu\.be\/([\S]+)/g.exec(stripLink);

            let suffix;

            if (domainMatch && domainMatch.length>=2) suffix = domainMatch[1];

            let id;
            let startTime;

            // Find start time
            const startTimeMatch = /(.+)\?t=([\d]+)/.exec(suffix);

            if (startTimeMatch && startTimeMatch.length>=3 && startTimeMatch[2]) {
                id = startTimeMatch[1];
                startTime = startTimeMatch[2];
            } else {
                id = suffix
            }


            if (id) {
                const iframe = document.createElement("iframe");

                iframe.width = IFRAME_WIDTH;
                iframe.height = IFRAME_HEIGHT;
                iframe.src = `https://www.youtube.com/embed/${id}${startTime? `?start${startTime}`: ""}`;

                setIframe(iframe);
                mediaRef.current.innerHTML = iframe.outerHTML;

                return;
            }

            setIframe(null);
            openMessage("잘못된 YouTube 링크");
        }
    }



    const insertYoutubeToEditor = () => {
        const isEditorFocused = checkEditorHasFocus(editorRef.current);


        if (!iframe) return;

        const p = document.createElement("p");
        p.appendChild(iframe.cloneNode(true));

        /**
         * Editor not focused
         * */
        if (!isEditorFocused && iframe) {
            selectEditorFirstLine(editorRef);

            const selection = document.getSelection();
            const range = selection.getRangeAt(0);

            const line = findLineContainer(range.endContainer);

            range.setStartBefore(line);
            range.setEndBefore(line);
            range.insertNode(p);

            selection.setPosition(line, 0);
        }
        /**
         * Editor has focus
         * */
        else if (isEditorFocused && iframe) {
            const selection = document.getSelection();
            const range = selection.getRangeAt(0);
            const line = findLineContainer(range.endContainer);

            range.setStartAfter(line);
            range.setEndAfter(line);
            range.insertNode(p);

            selection.setPosition(line, 0);
        }


        closeMessage();
        clearContent();
        closeBoardFunc();

        updatePostContentFunc();
    }


    useEffect(() => {
        if (!isBoardOpened) {
            closeMessage();
            clearContent();
        }

    }, [isBoardOpened])



    return (
        <div className={style["container"]}>
            <textarea placeholder="YouTube 링크 입력" rows={3} spellCheck={false} defaultValue="" onInput={onInput} ref={textareaRef}/>

            <section className={`${style["error_message"]} ${!showMessage? style["hide"]: ""}`}>
                <MessageWriter printLeftToRight={true} ref={messageWriter}/>
            </section>

            <section className={style["input"]}>
                <div ref={mediaRef}/>
                <button onClick={insertYoutubeToEditor}>본문삽입</button>
            </section>
        </div>
    )
}


export default PostYoutubeLinkInput;