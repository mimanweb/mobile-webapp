import React, {useRef, useEffect} from "react";
import style from "./styles/Message.postUrlCopied.module.scss";
import MessageWriter from "./messageWriter";
import {StretchOpenMessageType} from "../index";


const message = (type, url) => {


    switch (type) {
        case StretchOpenMessageType.POST.COPY_CURRENT_URL_TO_CLIPBOARD_COMPLETE:
            return `${url} 링크가 복사되었습니다.`;
    }

}


export default function PostUrlCopiedMessage({messageType, isBoardOpened, closeBoardFunc}) {

    const messageWriterRef = useRef(null);



    useEffect(() => {
        if (isBoardOpened) {
            const dummy = document.createElement('input')
            const text = window.location.href;

            document.body.appendChild(dummy);
            dummy.value = text;
            dummy.select();
            document.execCommand('copy');
            document.body.removeChild(dummy);


            messageWriterRef.current.write(message(messageType, text));
        }

        return () => {
            if (messageWriterRef.current) messageWriterRef.current.clear();
        }
    }, [isBoardOpened])


    return (
        <div className={style["container"]}>
            <MessageWriter printLeftToRight={true} ref={messageWriterRef}/>
            <button className={style["confirm"]} onClick={closeBoardFunc}>--&gt; 확인 &lt;--</button>
        </div>
    )
}

