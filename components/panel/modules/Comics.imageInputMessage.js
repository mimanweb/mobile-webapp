import React, {useRef, useEffect} from "react";
import style from "./styles/Message.postUpload.module.scss";
import MessageWriter from "./messageWriter";
import {StretchOpenMessageType} from "../index";


const message = (type) => {
    switch (type) {
        case StretchOpenMessageType.COMICS.COVER_IMAGE_INPUT_MESSAGE.INVALID_IMAGE_TYPE:
            return "jpg, jpeg, png 파일만 등록할 수 있습니다.";
        case StretchOpenMessageType.COMICS.COVER_IMAGE_INPUT_MESSAGE.TOO_LARGE_FILE:
            return "파일 크기가 15MB를 초과하였습니다.";
    }

}


const ComicsImageInputMessage = ({messageType, isBoardOpened, closeBoardFunc}) => {
    const messageWriterRef = useRef(null);



    useEffect(() => {
        if (isBoardOpened) messageWriterRef.current.write(message(messageType));

        return () => {
            if (messageWriterRef.current) messageWriterRef.current.clear();
        }
    }, [isBoardOpened])


    return (
        <div className={style["container"]}>
            <MessageWriter printLeftToRight={true} ref={messageWriterRef}/>
            <button className={style["confirm"]} onClick={closeBoardFunc}>--&gt; 확인 &lt;--</button>
        </div>
    )
}


export default ComicsImageInputMessage;