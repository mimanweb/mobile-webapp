import style from "./styles/Panel.verify_owner_before_delete_post.module.scss";
import MessageWriter from "./messageWriter";
import React, {useRef, useEffect, useState} from "react";
import {useSelector, useDispatch} from "react-redux";
import {requestPostOwnershipCheck, requestDeletePost} from "../../../features/post/actions";
import {useRouter} from "next/router";
import Keyboard from "../../../utils/editor/keyboard";
import {requestDeleteComment, setCommentState} from "../../../features/comment/actions";





const VerifyOwnerBeforeDeleteComment = ({isBoardOpened, closeBoardFunc}) => {
    const router = useRouter();
    const dispatch = useDispatch();

    const postState = useSelector(state => state.post);
    const commentState = useSelector(state => state.comment);

    const [password, setPassword] = useState(null);


    const {number: postNumber, category, board} = postState;
    const {commentNumberToDelete} = commentState;


    const messageWriterRef = useRef(null);
    const confirmButtonRef = useRef(null);



    const onKeyDown = (e) => {
        if (e.keyCode===Keyboard.KEY.ENTER) {
            confirmButtonRef.current.click();
        }
    }


    const onConfirmButtonClick = async () => {
        if (!password) {
            messageWriterRef.current.write("비밀번호가 입력되지 않았습니다.");
            return;
        }

        const {status, data: commentData} = await requestDeleteComment(postNumber, commentNumberToDelete, password);

        switch (status) {
            case 200:
                dispatch(setCommentState(commentData))

                closeBoardFunc();
                break;
            case 403:
                messageWriterRef.current.write("비밀번호가 틀립니다.");
                break;
            default:
                router.reload();
                break;
        }
    }


    useEffect(() => {
        if (isBoardOpened) messageWriterRef.current.write("댓글을 삭제하시겠습니까? ^-^");

        return () => {
            if (messageWriterRef.current) messageWriterRef.current.clear();
        }
    }, [isBoardOpened])


    return (
        <div className={style["container"]}>
            <section>
                <MessageWriter printLeftToRight={false} ref={messageWriterRef}/>
            </section>
            <section>
                <input type="password" placeholder="비밀번호" autoComplete="new-password" onKeyDown={onKeyDown} onChange={e => setPassword(e.target.value)}/>
                <button className={style["confirm"]} onClick={onConfirmButtonClick} ref={confirmButtonRef}>--&gt; 네 &lt;--</button>
            </section>
        </div>
    )
}


export default VerifyOwnerBeforeDeleteComment;