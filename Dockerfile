# Maybe i am going to fix the node version in later
FROM node:12.18.2-alpine3.12

WORKDIR /app

# Install all dependencies
COPY package.json ./
RUN npm install

# Copy all application sources to bundle
# Considering put  the node_modules in cache for later use
COPY . .

# Build here
RUN npm run stagingbuild

# Openning port
EXPOSE 3000

# Running the app
CMD [ "npm", "start" ]