import moment from "moment";
import _ from "underscore";


const isLocalServer = process.env.LOCAL_DEV==="1";



const now = () => {
    const today = moment();

    if (!_.isEmpty(process.env) && !isLocalServer) today.add(9, "hours");

    return today;
}


const parseMomentObj = (time) => {
    time = moment(time);

    if (!_.isEmpty(process.env) && !isLocalServer) time.add(9, "hours");

    const year = time.year();
    const month = time.month()+1;
    const date = time.date();
    const hour = time.hours();
    const minute = time.minutes();

    return {
        year: year,
        month: month,
        date: date,
        hour: hour,
        minute: minute
    }
}


const timeShortFormat = (time) => {
    const {year, month, date, hour, minute} = parseMomentObj(time);

    const today = moment();

    if (!_.isEmpty(process.env) && !isLocalServer) today.add(9, "hours");

    if (today.date() !== date) {
        return `${month}/${date}`;
    } else {
        return `${hour<10? `0${hour}`: hour}:${minute<10? `0${minute}`: minute}`;
    }
}


const timeFullFormat = (time) => {
    const {year, month, date, hour, minute} = parseMomentObj(time);

    return `${year}년 ${month}월 ${date}일 ${hour}시 ${minute}분`;
}





export default {
    fullFormatText: timeFullFormat,
    shortFormatText: timeShortFormat,
    now: now
}