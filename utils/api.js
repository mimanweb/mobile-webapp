const serverUrl = process.env["API_SERVER_URL"];

export const API = {
    User: {
        login: {
            url: serverUrl + "/auth/login",
            method: "post"
        },
        logout: {
            url: serverUrl + "/auth/logout",
            method: "post"
        },
        loginVerify: {
            url: serverUrl + "/auth/login/verify",
            method: "get"
        },
        register: {
            url: serverUrl + "/user/register",
            method: "post"
        },
        checkDuplicate: {
            url: serverUrl + "/user/check/duplicate",
            method: "get"
        },
        findId: {
            url: serverUrl + "/user/id",
            method: "get"
        },
        changePassword: {
            url: serverUrl + "/user/password",
            method: "put"
        },
        changeInfo: {
            url: serverUrl + "/user/info",
            method: "put"
        },
        unregister: {
            url: serverUrl + "/user",
            method: "delete"
        },
        getNotifications: {
            url: serverUrl + "/user/notification/list",
            method: "get"
        },
        setNotificationChecked: {
            url: serverUrl + "/user/notification/checked",
            method: "put"
        },
        setAllNotificationsChecked: {
            url: serverUrl + "/user/notification/all/checked",
            method: "put"
        }
    },
    Comics: {
        createComics: {
            url: serverUrl + "/comics",
            method: "post"
        },
        findComicsMatchTitle: {
            url: serverUrl + "/comics/match/title",
            method: "get"
        },
        findComicsMatchTitleIndexChar: {
            url: serverUrl + "/comics/match/title/index/char",
            method: "get"
        },
        // Handle comics writer info
        addComicsWriter: {
            url: serverUrl + "/comics/writer",
            method: "post"
        },
        addNewComicsWriter: {
            url: serverUrl + "/comics/writer/new",
            method: "post"
        },
        findWritersMatchText: {
            url: serverUrl + "/comics/writers/match/text",
            method: "get"
        },
        // Handle comics artist info
        addComicsArtist: {
            url: serverUrl + "/comics/artist",
            method: "post"
        },
        addNewComicsArtist: {
            url: serverUrl + "/comics/artist/new",
            method: "post"
        },
        findArtistsMatchText: {
            url: serverUrl + "/comics/artists/match/text",
            method: "get"
        },
        // Handle comics publisher info
        findPublishersMatchText: {
            url: serverUrl + "/comics/publishers/match/text",
            method: "get"
        },
        updateComicsPublisher: {
            url: serverUrl + "/comics/publisher",
            method: "put"
        },
        updateWithNewComicsPublisher: {
            url: serverUrl + "/comics/publisher/new",
            method: "put"
        },
        // Handle comics genre info
        findAllComicsGenres: {
            url: serverUrl + "/comics/genres",
            method: "get"
        },
        addComicsGenre: {
            url: serverUrl + "/comics/genre",
            method: "post"
        },
        findAllComicsContentDescriptors: {
            url: serverUrl + "/comics/content-descriptors",
            method: "get"
        },
        addComicsContentDescriptor: {
            url: serverUrl + "/comics/content-descriptor",
            method: "post"
        },
        updateComicsCoverImage: {
            url: serverUrl + "/comics/cover-image",
            method: "post"
        },
        updateComicsSummary: {
            url: serverUrl + "/comics/summary",
            method: "put"
        },
        recentReadComicsLatestUpdateInfo: {
            url: serverUrl + "/comics/recent/read/list",
            method: "get"
        }
    },
    ComicBook: {
        addPageImage: {
            url: serverUrl + "/comic-book/edit/page",
            method: "post"
        },
        updateSinglePageType: {
            url: serverUrl + "/comic-book/edit/single/page/type",
            method: "put"
        },
        updateDoublePageType: {
            url: serverUrl + "/comic-book/edit/double/page/type",
            method: "put"
        },
        deletePage: {
            url: serverUrl + "/comic-book/edit/page",
            method: "delete"
        },
        updatePageOrder: {
            url: serverUrl + "/comic-book/edit/page/order",
            method: "put"
        },
        updateInfo: {
            url: serverUrl + "/comic-book/edit/info",
            method: "put"
        },
        publishComicBook: {
            url: serverUrl + "/comic-book",
            method: "post"
        },
        updateComicBookData: {
            url: serverUrl + "/comic-book",
            method: "put"
        },
        deleteComicBook: {
            url: serverUrl + "/comic-book",
            method: "delete"
        }
    },
    Post: {
        findTempPost: {
            url: serverUrl + "/post/temp",
            method: "get"
        },
        checkPostOwnership: {
            url: serverUrl + "/post/ownership",
            method: "get"
        },
        createTempPost: {
            url: serverUrl + "/post/temp",
            method: "post"
        },
        updateTempPost: {
            url: serverUrl + "/post/temp",
            method: "put"
        },
        publishPost: {
            url: serverUrl + "/post",
            method: "post"
        },
        updateUpvote: {
            url: serverUrl + "/post/upvote",
            method: "post"
        },
        updatePost: {
            url: serverUrl + "/post",
            method: "put"
        },
        deletePost: {
            url: serverUrl + "/post",
            method: "delete"
        }
    },
    Comment: {
        findCommentsInSpecificPage: {
            url: serverUrl + "/comment/page",
            method: "get"
        },
        uploadComment: {
            url: serverUrl + "/comment",
            method: "post"
        },
        uploadReplyComment: {
            url: serverUrl + "/comment/reply",
            method: "post"
        },
        updateCommentUpvote: {
            url: serverUrl + "/comment/upvote",
            method: "post"
        },
        userDeleteComment: {
            url: serverUrl + "/comment",
            method: "delete"
        }
    },
    Image: {
        uploadPostImage: {
            url: serverUrl + "/image/post",
            method: "post"
        }
    },
    Editor: {
        linkTitle: {
            url: serverUrl + "/post/link/title",
            method: "post"
        }
    },
    PageData: {
        homepage: {
            url: serverUrl + "/service/data/homepage",
            method: "get"
        },
        postPage: {
            url: serverUrl + "/service/data/post",
            method: "get"
        },
        postEditPage: {
            url: serverUrl + "/service/data/post/edit",
            method: "get"
        },
        board: {
            community: {
                notice: {
                    url: serverUrl + "/service/data/board/community/notice",
                    method: "get"
                },
                freeboard: {
                    url: serverUrl + "/service/data/board/community/freeboard",
                    method: "get"
                },
                humor: {
                    url: serverUrl + "/service/data/board/community/humor",
                    method: "get"
                },
                horror: {
                    url: serverUrl + "/service/data/board/community/horror",
                    method: "get"
                },
                food: {
                    url: serverUrl + "/service/data/board/community/food",
                    method: "get"
                }
            },
            comics: {
                freeboard: {
                    url: serverUrl + "/service/data/board/comics/freeboard"
                }
            },
            animation: {
                freeboard: {
                    url: serverUrl + "/service/data/board/animation/freeboard"
                }
            }
        },
        comicsPage: {
            url: serverUrl + "/service/data/comics",
            method: "get"
        },
        comicsTranslatePage: {
            url: serverUrl + "/service/data/comics/translate",
            method: "get"
        },
        comicBookWritePage: {
            url: serverUrl + "/service/data/comic-book/write",
            method: "get"
        },
        comicBookEditPage: {
            url: serverUrl + "/service/data/comic-book/edit",
            method: "get"
        },
        comicBookPage: {
            url: serverUrl + "/service/data/comic-book",
            method: "get"
        }
    },
    Mail: {
        sendUserId: {
            url: serverUrl + "/mail/user/id",
            method: "post"
        },
        sendPasswordResetLink: {
            url: serverUrl + "/mail/user/password/reset/link",
            method: "post"
        },
    }
};