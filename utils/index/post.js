const EDIT_MODE = {
    WRITING: "WRITING",
    EDIT: "EDIT"
}

export const TYPE = {
    NOTICE: "NOTICE",
    HOT: "HOT",
}

export default {
    EDIT_MODE, TYPE
}