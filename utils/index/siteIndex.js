export const SiteIndex = {
    Community: {
        code: "community",
        name: "커뮤니티",
        boards: {
            Notice: {
                code: "notice",
                name: "공지사항",
                fullName: "커뮤니티 공지사항",
                tagName: "공지",
                path: "/board/community/notice",
                showOnSidebar: true
            },
            Freeboard: {
                code: "freeboard",
                name: "자유게시판",
                fullName: "자유게시판",
                tagName: "자유",
                path: "/board/community/freeboard",
                showOnSidebar: true
            },
            Humor: {
                code: "humor",
                name: "유머게시판",
                fullName: "유머게시판",
                tagName: "유머",
                path: "/board/community/humor",
                showOnSidebar: true
            },
            Food: {
                code: "food",
                name: "음식게시판",
                fullName: "음식게시판",
                tagName: "음식",
                path: "/board/community/food",
                showOnSidebar: true
            },
            Horror: {
                code: "horror",
                name: "호러게시판",
                fullName: "호러게시판",
                tagName: "호러",
                path: "/board/community/horror",
                showOnSidebar: true
            },
            // Photo: {
            //     code: "photo",
            //     name: "사진게시판",
            //     path: "/board/community/photo"
            // },
        }
    },
    Comics: {
        code: "comics",
        name: "코믹스",
        boards: {
            // Today: {
            //     code: "today",
            //     name: "모든코믹스 홈",
            //     fullName: "코믹스 모든코믹스",
            //     tagName: "모든코믹",
            //     path: "/board/comics/home",
            //     showOnSidebar: true,
            //     isNotReady: true
            // },
            Freeboard: {
                code: "freeboard",
                name: "잡담게시판",
                fullName: "코믹스 잡담게시판",
                tagName: "코믹잡담",
                path: "/board/comics/freeboard",
                showOnSidebar: true
            },
            NewsAndColumn: {
                code: "news_column",
                name: "코믹스소식/칼럼/리뷰",
                fullName: "코믹스 소식/칼럼/리뷰",
                tagName: "코믹소식",
                path: "/board/comics/news_column",
                showOnSidebar: false,
                isNotReady: true
            },
            Translate: {
                code: "translate",
                name: "번역게시판",
                fullName: "코믹스 번역게시판",
                tagName: "코믹번역",
                path: "/board/comics/translate",
                showOnSidebar: false,
            },
            ComicBook: {
                code: "book"
            }
            // Review: {
            //     code: "review",
            //     name: "코믹스리뷰",
            //     fullName: "COMICS 리뷰",
            //     path: "/board/comics/reviews"
            // },
        }
    },
    Animation: {
        code: "animation",
        name: "미애니",
        boards: {
            Freeboard: {
                code: "freeboard",
                name: "잡담게시판",
                fullName: "미애니 잡담게시판",
                tagName: "매니잡담",
                path: "/board/animation/freeboard",
                showOnSidebar: true
            },
            NewsAndColumn: {
                code: "news_column",
                name: "미애니소식/칼럼",
                fullName: "미애니 소식/칼럼",
                tagName: "먜니소식",
                path: "/board/animation/news_column",
                showOnSidebar: false,
                isNotReady: true
            },
            // ComicsWIKI: {
            //     code: "wiki",
            //     name: "미애니WIKI",
            //     fullName: "미애니 WIKI",
            //     path: "/board/animation/wiki"
            // }
        }
    }
}


export const findCategoryMapItem = (code) => {
    return Object.values(SiteIndex).find(category => category.code===code);
}

export const findBoardMapItem = (categoryCode, code) => {
    const category = findCategoryMapItem(categoryCode);

    if (!category) return null;

    return Object.values(category.boards).find(board => board.code===code);
}
