import React, {useEffect} from "react";
import {useDispatch} from "react-redux";
import {wrapper} from "../../../../features/store";
import { useRouter } from "next/router";
import Error from "next/error";
import Custom404 from "../../../404";
import Custom500 from "../../../500";
import LeftSidebar from "../../../../components/sideBar/left-sidebar";
import RightSidebar from "../../../../components/sideBar/right-sidebar";
import ErrorServerDown from "../../../500.ServerDown";
import {SiteIndex} from "../../../../utils/index/siteIndex";
import {setBoardIndex, setBoardPostListAndPageInfoState} from "../../../../features/pageData/boardPage/actions";


import style from "./post.humor.module.scss"


import {requestPostPageData} from "../../../../features/pageData/actions";
import {setPostState} from "../../../../features/post/actions";
import {setCommentState} from "../../../../features/comment/actions";


import CommentPanel from "../../../../components/post/comment";
import Article from "../../../../components/post/article"
import _ from "underscore";
import {checkNumeric} from "../../../../utils/format";
import PostBoard from "../../../../components/board/board";
import {AD_728X90} from "../../../../components/ad";
import DeletedPostPage from "../../../../components/errorPage/deletedPostPage";



const category = SiteIndex.Community.code;
const board = SiteIndex.Community.boards.Humor.code;



export default function CommunityHumorPost({status, pageData, isServerDown}) {
    if (isServerDown) return <ErrorServerDown/>;

    switch (status) {
        case 404:
            return <DeletedPostPage/>;
        case 403:
        case 500:
            return <Custom500/>;
    }

    const router = useRouter();
    const dispatch = useDispatch();





    return (
        <>
            <LeftSidebar/>

            <div className={style["container"]}>

                <section className={style["post"]}>
                    <Article/>
                </section>


                <section className={style["comment"]}>
                    <CommentPanel/>
                </section>

                <section className={style["ad"]}>
                    <AD_728X90/>
                </section>

                <section className={style["board"]}>
                    <PostBoard category={category} board={board}/>
                </section>

                <section className={style["ad_bottom"]}>
                    <AD_728X90/>
                </section>
            </div>

            <RightSidebar/>
        </>

    )
}




export const getServerSideProps = wrapper.getServerSideProps(async (context) => {
    const {dispatch} = context.store;
    const {query} = context;

    const postNumber = (!_.isEmpty(query) && checkNumeric(query.post))? query.post: 1;
    const commentToScroll = (!_.isEmpty(query) && checkNumeric(query.comment))? query.comment: null;
    const listType = (!_.isEmpty(query) && query.list)? query.list: undefined;



    /**
     * Set board page index state
     * */
    dispatch(setBoardIndex(category, board, listType));


    /**
     * Request page data
     * */
    const {status, postData, commentData, boardData, isServerDown} = await requestPostPageData(postNumber, listType, category, board, commentToScroll, context);


    if (status===200) {

        /**
         * Set post data state
         * */
        dispatch(setPostState(postData, category, board));


        /**
         * Set post comment data state
         * */
        dispatch(setCommentState(commentData, commentToScroll));

        /**
         * Set board data
         * */
        const {postList: boardPostList, noticePostList, searchedPage, totalPageCount, postPerPage} = boardData;

        dispatch(setBoardPostListAndPageInfoState(boardPostList, noticePostList, searchedPage, totalPageCount, postPerPage));
    }


    return {
        props: {
            status: status,
            pageData: postData,
            isServerDown: isServerDown,
        }
    }
})