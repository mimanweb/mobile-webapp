import React, {useEffect} from 'react';
import { wrapper } from '../features/store';
import './global.scss'
import style from './main.module.scss'
import {useDispatch} from "react-redux";


import NavBar from "../components/navBar/navbar";
import Footer from "../components/footer/footer";
import DocHead from "../components/meta/head";
import {updateRecentReadComicsList} from "../utils/index/comics/comics";

const App = ({Component, pageProps}) => {
    const dispatch = useDispatch();



    /**
     * Update recent comic book list state
     * */
    useEffect(async () => {
        await updateRecentReadComicsList(dispatch)
    }, [])

    return (
        <>
            <DocHead/>
            <NavBar/>
            <div className={style['container']}>
                <Component {...pageProps} />
            </div>
            <Footer/>
        </>
    )
};

export default wrapper.withRedux(App);