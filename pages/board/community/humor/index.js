import React from "react";
import {wrapper} from "../../../../features/store";
import {
    requestCommunityFreeboardPageData,
    requestCommunityHumorPageData
} from "../../../../features/pageData/boardPage/communityCategory";
import {setBoardPostListAndPageInfoState, setBoardIndex} from "../../../../features/pageData/boardPage/actions";
import ErrorServerDown from "../../../500.ServerDown";
import _ from "underscore";
import {checkNumeric} from "../../../../utils/format";
import {SiteIndex} from "../../../../utils/index/siteIndex";
import CommonBoardPage from "../../../../components/board/commonBoardPage";


const category = SiteIndex.Community.code;
const board = SiteIndex.Community.boards.Humor.code;
const boardFullname = SiteIndex.Community.boards.Humor.fullName;




export default function CommunityHumorPage({isServerDown, status}) {
    if (isServerDown) return <ErrorServerDown/>;


    return (
        <CommonBoardPage category={category} board={board} boardFullname={boardFullname} status={status} isServerDown={isServerDown}/>
    );
}




export const getServerSideProps = wrapper.getServerSideProps(async (context) => {
    const {dispatch} = context.store;
    const {query} = context;

    const pageToSearch = (!_.isEmpty(query) && checkNumeric(query.page) && Number(query.page)>0)? query.page: 1;
    const listType = (!_.isEmpty(query) && query.list)? query.list: undefined;


    /**
     * Set board page index state
     * */
    dispatch(setBoardIndex(category, board, listType));


    /**
     * Request page data
     * */
    const {status, board: boardData, isServerDown} = await requestCommunityHumorPageData(pageToSearch, listType, context);


    switch (status) {
        case 200:
            const {postList, searchedPage, totalPageCount, postPerPage, noticePostList, hotPostListInfo} = boardData;
            dispatch(setBoardPostListAndPageInfoState(postList, noticePostList, searchedPage, totalPageCount, postPerPage, hotPostListInfo));
            break;
    }



    return {
        props: {
            isServerDown: isServerDown,
            status: status,
        }
    }
});