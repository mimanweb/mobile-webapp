import {useRouter} from "next/router";
import React, {useState} from "react";
import {useSelector} from "react-redux"
import ErrorServerDown from "../../../500.ServerDown";


// Style
import style from "./password.module.scss";

// Actions
import {requestChangePassword} from "../../../../features/account/actions";
import {requestLoginVerify} from "../../../../features/user/actions";
import {wrapper} from "../../../../features/store";
import LeftSidebar from "../../../../components/sideBar/left-sidebar";
import RightSidebar from "../../../../components/sideBar/right-sidebar";


const PasswordResetPanel = (props) => {
    const router = useRouter();
    const [password, setPassword] = useState("");

    const {id, token} = props;


    const submit = async (e) => {
        e.preventDefault();
        if (!password) return;

        const result = await requestChangePassword(id, token, password);

        if (result.success) {
            alert("비밀번호를 변경하였습니다.")
            await router.push("/")
        } else {
            alert(result.msg);
        }
    }


    return (
        <div>
            <blockquote>
                <p>"계정({id})의 비밀번호를 변경합니다."</p>
                <p>"비밀번호가 변경되면 모든 장치에서 로그아웃되며, 새로운 비밀번호로 다시 로그인해야 합니다."</p>
            </blockquote>

            <form onSubmit={submit}>
                <h3>새롭게 변경할 비밀번호</h3>
                <br/>
                <input type="text" hidden={true} value={id} readOnly={true}/>
                <input type="password" autoComplete="new-password" onChange={(e) => setPassword(e.target.value)}/>
                <br/>
                <br/>
                <input type="submit" value="변경하기"/>
            </form>
        </div>
    )
}


export default function ResetPassword({isServerDown}) {
    if (isServerDown) return <ErrorServerDown/>;


    const router = useRouter();
    const params = router.query.params

    let id;
    let token;

    if (params && params.length > 1) {
        id = params[0]
        token = params[1]
    }

    return (
        <>
            <LeftSidebar/>

            <div className={style["wrapper"]}>
                <h2>비밀번호 재설정</h2>
                <br/>

                {
                    !id || !token ? (
                        <blockquote>
                            <p>올바른 링크가 아닙니다.</p>
                        </blockquote>
                    ) : (
                        <PasswordResetPanel id={id} token={token}/>
                    )
                }
            </div>

            <RightSidebar/>
        </>
    )
}


export const getServerSideProps = wrapper.getServerSideProps(async (context) => {
    const {isServerDown} = await requestLoginVerify(context);

    return {
        props: {
            isServerDown: isServerDown
        }
    }
})