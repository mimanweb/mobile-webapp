import {wrapper} from "../../../features/store";
import React, {useState, useEffect} from "react";
import {useSelector} from "react-redux";
import ErrorServerDown from "../../500.ServerDown";
import Meta from "../../../components/meta/head";



// Styles
import style from "./find.module.scss";
import LoadingDots from "../../../components/animation/loading/dots";


// Actions
import {findIdRequest, requestSendIdEmail, requestPasswordResetEmail} from "../../../features/find/actions";
import {requestLoginVerify} from "../../../features/user/actions";
import LeftSidebar from "../../../components/sideBar/left-sidebar";
import RightSidebar from "../../../components/sideBar/right-sidebar";


/**
 * Find id area
 * */
const FindId = () => {

    const [id, setId] = useState("아래 이메일을 입력하여 아이디를 찾습니다.");
    const [email, setEmail] = useState("");
    const [emailStatus, setEmailStatus] = useState("");
    const [emailResultMsg, setEmailResultMsg] = useState("");


    const submit = async (e) => {
        e.preventDefault();
        if (!email) return;

        const id = await findIdRequest(email);
        if (id) setId(id);
        else setId("해당 이메일로 가입된 계정이 없습니다.")
    };


    const sendEmail = async (e) => {
        e.preventDefault();
        if (!email) return;

        if (emailStatus==="PENDING") return;
        setEmailStatus("PENDING");


        const sent = await requestSendIdEmail(email);
        setEmailStatus(sent.result? "SUCCESS": "FAILED");
        setEmailResultMsg(sent.msg);
    };


    const EmailStatus = () => {
        switch (emailStatus) {
            case "PENDING":
                return <LoadingDots/>;
            case "SUCCESS":
                return <label className={style["success"]}>- 전송완료 -</label>;
            case "FAILED":
                return <label className={style["failed"]}>- 전송실패 - {emailResultMsg}</label>;
            default:
                return "";
        }
    }


    useEffect(()=>{
        setEmailStatus("");
    }, [email])

    return (
        <div className={style["id-finder"]}>
            <h3>아이디 찾기</h3>
            <br/>
            <p className={style["id"]}>{id}</p>
            <br/>
            <form onSubmit={submit}>
                <span>이메일(email)</span>
                <input type="email" onChange={e => setEmail(e.target.value)}/>
                <span>으로 가입된 </span><input type="submit" value="아이디 검색하기"/>
                <br/>
                <br/>

                <div className={style["retrieve-id"]}>
                    <span>그래도 모르겠다 싶을 땐</span><input type="submit" value="이메일로 아이디 받아보기" onClick={sendEmail}/>
                    <br/>
                    <EmailStatus/>
                </div>
            </form>
        </div>
    )
};


/**
 * Resetting reset area
 * */
const PasswordReset = () => {
    const [id, setId] = useState("");
    const [email, setEmail] = useState("");
    const [emailStatus, setEmailStatus] = useState("");
    const [emailResultMsg, setEmailResultMsg] = useState("");


    const submit = async (e) => {
        e.preventDefault();
        if (!id || !email) return;

        if (emailStatus==="PENDING") return;
        setEmailStatus("PENDING");


        const sent = await requestPasswordResetEmail(id, email);
        setEmailStatus(sent.result? "SUCCESS": "FAILED");
        setEmailResultMsg(sent.msg);
    };


    const EmailStatus = () => {
        switch (emailStatus) {
            case "PENDING":
                return <LoadingDots/>;
            case "SUCCESS":
                return <label className={style["success"]}>- 전송완료 -</label>;
            case "FAILED":
                return <label className={style["failed"]}>- 전송실패 - {emailResultMsg}</label>;
            default:
                return "";
        }
    }


    useEffect(()=>{
        setEmailStatus("");
    }, [id, email])


    return (
        <div className={style["pw-finder"]}>
            <h3>비밀번호 재설정하기</h3>
            <br/>

            <blockquote>
                <p>"계정 아이디(ID)와 이메일(email)을 입력하여</p>
                <p>&nbsp;비밀번호를 재설정 할 수 있는 링크를 이메일로 전송합니다."</p>
            </blockquote>
            <br/>

            <form onSubmit={submit}>
                <h4>아이디</h4>
                <input type="text" onChange={(e)=>setId(e.target.value)}/>
                <br/>

                <h4>이메일</h4>
                <input type="email" onChange={e => setEmail(e.target.value)}/>
                <br/>
                <br/>

                이메일(email)로 <input type="submit" value="비밀번호 재설정 링크 받아보기"/>
                <EmailStatus/>
                <br/>
                <br/>
            </form>
        </div>
    )
};



export default function FindAccount({isServerDown}) {
    if (isServerDown) return <ErrorServerDown/>;


    const user = useSelector(state => state.user);


    return (
        <>
            <Meta title="미만웹 | 계정찾기"/>
            
            <div className={style["wrapper"]}>
                <h2>계정 찾기</h2>
                <br/>

                {user.loggedIn? (
                    <blockquote>
                        <p>"이미 로그인 중입니다."</p>
                    </blockquote>
                ): (
                    <>
                        <FindId/>
                        <PasswordReset/>
                    </>
                )}
            </div>
        </>
    )
};



export const getServerSideProps = wrapper.getServerSideProps(async (context) => {
    const {isServerDown} = await requestLoginVerify(context);

    return {
        props: {
            isServerDown: isServerDown
        }
    }
});
