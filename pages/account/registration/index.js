import {wrapper} from "../../../features/store";
import React, {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux'
import ErrorServerDown from "../../500.ServerDown";
import style from './registration.module.scss';
import {pattern} from "../../../utils/regex";
import Meta from "../../../components/meta/head";



// Actions
import {registerRequest, checkDuplicateRequest} from "../../../features/registeration/actions";
import {requestLoginVerify} from "../../../features/user/actions";
import LeftSidebar from "../../../components/sideBar/left-sidebar";
import RightSidebar from "../../../components/sideBar/right-sidebar";



let timeout;


const RegistrationForm = () => {
    const dispatch = useDispatch();

    const [id, setId] = useState('');
    const [duplicate, setDuplicate] = useState({id: undefined, nickname: undefined, email: undefined});
    const [nickname, setNickname] = useState('');
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');



    // Check duplicates
    const checkDuplicate = (item, state) => {
        setDuplicate({...duplicate, ...{[item]: undefined}})
        clearTimeout(timeout);
        if (![item]) return;

        timeout = setTimeout(async () => {
            const duplicated = await checkDuplicateRequest([item], state);
            setDuplicate({...duplicate, ...{[item]: duplicated}})
        }, 400);
    };


    // Submit registration info
    const submit = (e) => {
        e.preventDefault();

        if (duplicate.id || duplicate.nickname || duplicate.email) return alert("중복된 항목이 존재합니다.");
        if (password1 !== password2) return alert("비밀번호 확인이 동일하게 입력되지 않았습니다.");
        if (!id || !password1 || !password2 || !nickname || !email) return alert("회원가입 항목을 모두 채워주십시오.");


        dispatch(registerRequest(id, password1, email, nickname));
    };


    const DuplicateMessage = ({item}) => {
        let state;
        switch (item) {
            case "id":
                state = id;
                break;
            case "nickname":
                state = nickname;
                break;
            case "email":
                state = email;
                break;
        }
        return state && (
            (duplicate[item] === undefined ? (
                <small>조회 중...</small>
            ) : (duplicate[item]) && (
                <small><strong>이미 사용 중입니다.</strong></small>
            ))
        )
    }


    useEffect(() => {
        checkDuplicate("id", id);
    }, [id]);


    useEffect(() => {
        checkDuplicate("nickname", nickname);
    }, [nickname]);


    useEffect(() => {
        checkDuplicate("email", email);
    }, [email]);


    return (
        <>
            <blockquote>
                <p>"계정 생성 이후 아이디와 이메일은 변경할 수 없습니다."</p>
                <p>"이메일은 아이디 찾기, 비밀번호 수정 등 계정 관리에 사용되므로, 실제 사용 중인 이메일을 입력해 주십시오."</p>
            </blockquote>
            <form onSubmit={submit}>
                <div>
                    <div>
                        <h3>아이디</h3>
                        <small>띄어쓰기 없이 <mark>4자 이상</mark>
                            <mark>16자 이하</mark>
                            <mark>영문, 숫자</mark>
                            조합</small>
                        <br/>
                        <input type='text'
                               minLength={4}
                               maxLength={16}
                               pattern={pattern.id}
                               onChange={e => setId(e.target.value)}/>
                        <DuplicateMessage item={"id"}/>

                    </div>
                    <div>
                        <h3>비밀번호</h3>
                        <small>띄어쓰기 없이 <mark>8자 이상</mark>
                            <mark>24자 이하</mark>
                            <mark>영문, 숫자, 특수문자</mark>
                            조합</small>
                        <br/>
                        <input type='password'
                               minLength={8}
                               maxLength={24}
                               pattern={pattern.password}
                               title={"영문, 숫자, 특수문자 !@#$%^&*_- 사용 가능"}
                               autoComplete="new-password"
                               onChange={e => setPassword1(e.target.value)}/>
                    </div>
                    <div>
                        <h3>비밀번호 확인</h3>
                        <br/>
                        <input type='password'
                               minLength={8}
                               maxLength={24}
                               pattern={pattern.password}
                               title={"영문, 숫자, 특수문자 !@#$%^&*_- 사용 가능"}
                               onChange={e => setPassword2(e.target.value)}/>
                    </div>
                </div>
                <div>
                    <div>
                        <h3>닉네임</h3>
                        <small>띄어쓰기 없이 <mark>16자 이하</mark>
                            <mark>한글, 영문, 숫자</mark>
                            조합</small>
                        <br/>
                        <small>주 1회 수정 가능</small>
                        <br/>
                        <input type='text'
                               minLength={1}
                               maxLength={16}
                               pattern={pattern.nickname}
                               onChange={e => setNickname(e.target.value)}/>
                        <DuplicateMessage item={"nickname"}/>
                    </div>
                    <div>
                        <h3>이메일</h3>
                        <br/>
                        <input type='email'
                               minLength={1}
                               onChange={e => setEmail(e.target.value)}/>
                        <DuplicateMessage item={"email"}/>
                    </div>
                </div>
                <div>
                    <input type="submit" value="회원가입"/>
                </div>
            </form>
        </>
    )
};


const LoggedInMessage = () => {
    return (
        <blockquote>
            <p>"이미 로그인 중입니다."</p>
        </blockquote>
    )
};


export default function Registration({isServerDown}) {
    if (isServerDown) return <ErrorServerDown/>;


    const user = useSelector(state => state.user);

    return (
        <>
            <Meta title="미만웹 | 회원가입"/>

            <main className={style["wrapper"]}>
                <h2>미만웹 회원가입</h2>
                <br/>
                {user.loggedIn ? (
                    <LoggedInMessage/>
                ) : (
                    <RegistrationForm/>
                )}

            </main>
        </>
    )
};


export const getServerSideProps = wrapper.getServerSideProps(async (context) => {
    const {isServerDown} = await requestLoginVerify(context);

    return {
        props: {
            isServerDown: isServerDown
        }
    }
})