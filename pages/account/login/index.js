import React, {useState, useEffect} from 'react';
import {useDispatch} from 'react-redux'
import style from './login.module.scss';
import Meta from '../../../components/meta/head';
import {loginRequest, requestLoginVerify} from "../../../features/user/actions";
import {wrapper} from "../../../features/store";
import Link from "next/link";


/**
 * Login page
 * */
export default function Login({referer}) {
    const dispatch = useDispatch();
    const [userId, setUserId] = useState("");
    const [userPassword, setUserPassword] = useState("");
    const [stayLoggedIn, setStayLoggedIn] = useState(false);
    const [isStayLoggedInStored, setIsStayLoggedInStored] = useState(false);


    // Set stay logged in checkbox as local stored value
    useEffect(() => {
        const stored = localStorage.getItem("STAY_LOGGED_IN")==="true"

        if (!isStayLoggedInStored) {
            setStayLoggedIn(stored)
            setIsStayLoggedInStored(true);
        }

        localStorage.setItem("STAY_LOGGED_IN", stayLoggedIn.toString())
    }, [stayLoggedIn])


    const onSubmit = (e) => {
        e.preventDefault();

        if (!userId) return;
        if (!userPassword) return;

        dispatch(loginRequest(userId, userPassword, stayLoggedIn, referer));
    }



    return (
        <>
            <Meta title='미만웹 | 로그인'/>


            <main className={style["container"]}>
                <form className={style["login"]} onSubmit={e=>e.preventDefault()}>
                    {/** LOGIN */}
                    <section className={style["info"]}>
                        <h1>
                            <label>미만웹</label>
                            <span> 로그인</span>
                        </h1>
                        <input className={style["id"]} type="text" placeholder="아이디" onChange={(e) => setUserId(e.target.value)} spellCheck={false}/>
                        <input className={style["password"]} type="password" placeholder="비밀번호" onChange={(e) => setUserPassword(e.target.value)} spellCheck={false}/>
                    </section>

                    {/** LOGIN BUTTON, STAY LOGGED IN CHECKBOX */}
                    <section className={style["menu"]}>
                        <button className={style["login"]} onClick={onSubmit}>로그인</button>

                        <div className={style["stay_logged_in"]} onClick={() => setStayLoggedIn(!stayLoggedIn)}>
                            <div className={stayLoggedIn? style["checkbox"]: style["checkbox_blank"]}/>
                            <label>로그인유지</label>
                        </div>
                    </section>
                </form>

                {/** FIND USER INFO, SIGN UP */}
                <section className={style["find_user_register"]}>
                    <Link href="/account/find">
                        <a>아이디/비밀번호 찾기</a>
                    </Link>
                    <Link href="/account/registration">
                        <a className={style["register"]}>
                            <div className={style["icon"]}/>
                            <label>회원가입</label>
                        </a>
                    </Link>
                </section>
            </main>
        </>
    )
};



export const getServerSideProps = wrapper.getServerSideProps(async (context) => {
    const {isServerDown} = await requestLoginVerify(context);


    return {
        props: {
            isServerDown: isServerDown,
            referer: context.req.headers.referer || null
        }
    }
})

