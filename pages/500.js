import React from "react";
import style from "./500.module.scss"
import Link from "next/link";

import SmallButton, {SmallButtonType} from "../components/button/small";


const Error500 = () => {

    return (
        <div className={style["container"]}>
            <section>
                <h1>ERROR<br/>HAS<br/>occurred</h1>

                <p>"에러가 발생하였습니다."</p>

                <section className={style["buttons"]}>
                    <Link href="/">
                        <a className={style["to_home_button"]}>
                            <SmallButton buttonType={SmallButtonType.TO_HOME}/>
                        </a>
                    </Link>
                    <SmallButton buttonType={SmallButtonType.GO_BACK} onClick={()=>window.history.back()}/>
                </section>
            </section>
        </div>
    )
}



export default Error500;