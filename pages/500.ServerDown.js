import React from "react";
import style from "./500.module.scss"
import Link from "next/link";

import SmallButton, {SmallButtonType} from "../components/button/small";


const ErrorServerDown = () => {

    return (
        <div className={style["container"]}>
            <section>
                <h1>SERVER<br/>IS<br/>DOWN</h1>

                <p>"현재 서버가 점검 중입니다.<br/>잠시 뒤 다시 이용해 주십시오."</p>

                <section className={style["buttons"]}>
                    <Link href="/">
                        <a className={style["to_home_button"]}>
                            <SmallButton buttonType={SmallButtonType.TO_HOME}/>
                        </a>
                    </Link>
                    <SmallButton buttonType={SmallButtonType.GO_BACK} onClick={()=>window.history.back()}/>
                </section>
            </section>
        </div>
    )
}



export default ErrorServerDown;