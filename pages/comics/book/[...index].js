import {wrapper} from "../../../features/store";
import ComicsBookPage from "../../../components/comics/comicBookPage";
import ComicsPage from "../../../components/comics/comicsPage";
import React, {useEffect} from "react";
import {requestComicBookPageData, requestComicsPageData} from "../../../features/pageData/actions";
import {setComicsState} from "../../../features/comics/actions";
import {setComicBookState} from "../../../features/comicsBook/actions";
import {setPostState} from "../../../features/post/actions";
import {SiteIndex} from "../../../utils/index/siteIndex";
import {setCommentState} from "../../../features/comment/actions";
import router from "next/router";



const ComicBookIndex = ({status, comicsId, comicsNumber, translator, issue, volume, isComicsBookPageRequested}) => {


    if (isComicsBookPageRequested) {
        switch (status) {
            case 410:
                alert("유효하지 않은 코믹북 데이터입니다.");

                router.push(`/comics/book/${comicsNumber}/${comicsId}`);
                break;
            case 404:
                alert("삭제된 코믹북입니다.");

                router.push(`/comics/book/${comicsNumber}/${comicsId}`);
                break;
        }
    }

    if (issue || volume || translator) return <ComicsBookPage/>;
    else return <ComicsPage comicsId={comicsId} comicsNumber={comicsNumber}/>;
}



export default ComicBookIndex;



const category = SiteIndex.Comics.code;
const board = SiteIndex.Comics.boards.ComicBook.code;




export const getServerSideProps = wrapper.getServerSideProps(async (context) => {
    const {dispatch} = context.store;

    const {issue, vol: volume, translator, index} = context.query;
    const shortStoryNumber = context.query["short-story"];
    const comicsNumber = index[0];
    const comicsId = index[1];


    const isComicsBookPageRequested = (issue || volume || shortStoryNumber);


    /**
     * Comics page requested
     * */
    if (!isComicsBookPageRequested) {
        const {status, comicsData, isServerDown} = await requestComicsPageData(comicsNumber, comicsId, context);

        switch (status) {
            case 200:
                dispatch(setComicsState(comicsData));
                break;
        }

        return {
            props: {
                status: status,
                isServerDown: isServerDown,
                comicsId: comicsId,
                comicsNumber: comicsNumber,
                volume: volume || null,
                issue: issue || null,
                translator: translator || null,
                isComicsBookPageRequested: isComicsBookPageRequested || false
            }
        }
    }
    /**
     * Comics Book page requested
     * */
    else {
        const {status, isServerDown, comicsData, comicBookData, comicBookPostData, comicBookCommentData} = await requestComicBookPageData(comicsNumber, comicsId, translator, issue, volume, shortStoryNumber, context);

        switch (status) {
            case 200:
                dispatch(setComicsState(comicsData));
                dispatch(setComicBookState(comicBookData));
                dispatch(setPostState(comicBookPostData, category, board));
                dispatch(setCommentState(comicBookCommentData))
                break;
        }

        return {
            props: {
                status: status,
                isServerDown: isServerDown,
                comicsId: comicsId,
                comicsNumber: comicsNumber,
                volume: volume || null,
                issue: issue || null,
                translator: translator || null,
                isComicsBookPageRequested: isComicsBookPageRequested || false
            }
        }
    }

})


