const {
    PHASE_DEVELOPMENT_SERVER,
    PHASE_PRODUCTION_BUILD,
} = require('next/constants')

// This uses phases as outlined here: https://nextjs.org/docs/#custom-configuration
module.exports = (phase) => {
    // development env
    const isDev = phase === PHASE_DEVELOPMENT_SERVER
    // local development env
    const isLocalDev = process.env.NEXT_SERVER_ENV === "local"

    // when `next build` or `npm run build` is used
    const isProd = phase === PHASE_PRODUCTION_BUILD && process.env.STAGING !== '1'
    const isStaging = phase === PHASE_PRODUCTION_BUILD && process.env.STAGING === '1'


    console.log("phase", phase)
    console.log(`isDev:${isDev}   isLocalDev:${isLocalDev}   isProd:${isProd}   isStaging:${isStaging}`)

    const env = {
        API_SERVER_URL: (() => {
            if (isDev) return 'http://localhost:5000'
            if (isStaging) return 'https://dev.api.mimanweb.com'
            if (isProd) return 'https://api.mimanweb.com'
        })(),
        IMG_SERVER_URL: (() => {
            if (isDev) return 'http://localhost:5000/file'
            if (isStaging) return 'https://dev.img.mimanweb.com/file'
            if (isProd) return 'https://img.mimanweb.com/file'
        })(),
        WEBSITE_ORIGIN_REFERER: (() => {
            if (isDev) return 'localhost:3001'
            if (isStaging) return 'mimanweb.com'
            if (isProd) return 'mimanweb.com'
        })(),
        DEBUG_REDUX: (() => {
            if (isDev) return '1'
            if (isProd) return '0'
        })()
    }

    // next.config.js object
    return {
        env,
        poweredByHeader: false,
    }
}